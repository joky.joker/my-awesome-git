package com.magit.javafx.ui.controllers;

import com.magit.javafx.ui.MagitFX;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.event.MouseEvent;

public class AboutController {
    @FXML
    Button okay;
    @FXML
    Hyperlink myMail;
    @FXML
    Hyperlink guysMail;
    private Stage myStage;

    public void setStage(Stage stage){
        myStage = stage;
    }

    public void onExit(){
        myStage.close();
    }

    public void onMyMailClicked(){
        MagitFX.getInstance().getHostServices().showDocument("mailto:" + myMail.getText());
    }

    public void onGuysMailClicked(){
        MagitFX.getInstance().getHostServices().showDocument("mailto:" + guysMail.getText());
    }
}
