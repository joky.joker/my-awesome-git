package com.magit.javafx.ui.interfaces;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import javafx.scene.layout.Pane;

import java.util.List;

public interface CommitDrawer {
    Pane draw(Commit currentCommit, List<Branch> orDefault, IInformationProvider provider, CommitGraphDrawerListener listener, boolean isBold);
    double getPaneWidth();
    double getPaneHeight();
}
