package com.magit.javafx.ui.contextmenu;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

import java.util.List;

public class CheckoutMenuItem extends CommitContextMenu {
    @Override
    public MenuItem createMenuItem(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IUserInterfaceListener userInterfaceListener, IInformationProvider provider) {
        Menu checkout = new Menu("Checkout");
        for (Branch branch: branchList){
            if(branch.isRemote()){
                continue;
            }
            MenuItem branchCheckout = new MenuItem(branch.getFullName());
            branchCheckout.setOnAction( event -> {
                try {
                    if (provider.isChangesPending()){
                        UIController.<MainWindow>getController().onError("Cant checkout on pending changes");
                        return;
                    }
                    UIController.getUIListener().onSetHeadBranchCheckout(branch.getName());
                    //listener.onCommitFocused(commit);
                    listener.onRefreshRequested();
                }catch (Exception e){
                    onError(e);
                }});
            checkout.getItems().add(branchCheckout);
        }
        return checkout;
    }

    @Override
    public boolean shouldBeCreated(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IInformationProvider provider){
        return branchList != null && branchList.stream().anyMatch(branch -> !branch.isRemote());
    }
}
