package com.magit.javafx.ui.contextmenu;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;

import java.util.List;
import java.util.Optional;

public class ResetHeadMenuItem  extends CommitContextMenu{
    @Override
    public MenuItem createMenuItem(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IUserInterfaceListener userInterfaceListener, IInformationProvider provider) {
        MenuItem resetHead = new MenuItem("Reset " + getHeadOrNon(provider) + " To This");
        resetHead.setOnAction(event -> {
            try{
                String head = provider.getHead();
                if (provider.isChangesPending()){
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Override");
                    alert.setHeaderText("Override Pending changes?");
                    alert.setContentText("There are pending changes in the MaGit folder, are you sure you want to proceed?");
                    alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
                    Optional<ButtonType> result = alert.showAndWait();
                    if (!result.isPresent()){
                        return;
                    }
                    if (result.get().equals(ButtonType.NO)){
                        return;
                    }
                }
                UIController.getUIListener().overrideBranchCommit(head, commit.getSha1Identifier());
                UIController.getUIListener().onSetHeadBranchCheckout(head); //check out after the reset
                listener.onRefreshRequested();
            }catch (Exception e){
                UIController.getLoader().<MainWindow>getController().onError(e);
            }
        });
        return resetHead;
    }

    @Override
    public boolean shouldBeCreated(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IInformationProvider provider){
        try{
            return !provider.getHeadBranch().getCommitPointed().equals(commit.getSha1Identifier());
        } catch (Exception e){}
        return false;
    }
}
