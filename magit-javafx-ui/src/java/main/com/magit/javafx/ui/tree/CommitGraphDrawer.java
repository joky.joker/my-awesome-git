package com.magit.javafx.ui.tree;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.interfaces.CommitDrawer;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import com.magit.javafx.ui.interfaces.ErrorListener;
import javafx.beans.InvalidationListener;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class CommitGraphDrawer {
    private Group graphicalView;
    private Commit root;
    private CommitDrawer drawer;
    private Map<Commit, Pane> commitToUpperPaneAndPoint;
    private Graph<Commit, DefaultEdge> commitGraph;
    private CommitGraphDrawerListener listener;
    private ErrorListener errorListener;
    private IInformationProvider provider;
    private double totalWidth;
    private boolean isValid = false;
    private List<Commit> sorted_commits;
    private Set<Commit> boldCommits;
    private boolean sortByDate;
    private static Logger log = Logger.getLogger(CommitGraphDrawer.class.getTypeName());

    public void setErrorListener(ErrorListener errorListener) {
        this.errorListener = errorListener;
    }

    private void sendError(Exception e){
        if (errorListener != null){
            errorListener.onError(e);
        }
    }
    private static double YSPACING = 20;

    public void setCommitChosenListener(CommitGraphDrawerListener listener) {
        this.listener = listener;
    }

    public CommitGraphDrawer(IInformationProvider provider, Graph<Commit, DefaultEdge> graph,
                             Group grid, CommitDrawer drawer, double totalWidth,
                             Set<Commit> boldCommits, boolean sortByDate) throws Exception{
        this.graphicalView = grid;
        this.totalWidth = Math.max(totalWidth, 100);
        this.graphicalView.getChildren().clear();
        this.provider = provider;
        this.sortByDate = sortByDate;
        List<Commit> roots = graph.vertexSet().stream().filter(commit -> graph.inDegreeOf(commit) == 0).collect(Collectors.toList());
        if (roots.size() != 1){
            log.info(String.valueOf(roots.stream().map(commit -> commit.getSha1Identifier()).collect(Collectors.toList())));
            log.warning("invalid commit graph received!");
            return;
        }
        isValid = true;
        this.boldCommits = boldCommits;
        this.root = roots.get(0);
        this.drawer = drawer;
        this.commitGraph = graph;
        this.sorted_commits = graph.vertexSet().stream().sorted(Comparator.comparing(Commit::getDateOfCreation)).collect(Collectors.toList());
    }

    private double getYForPane(){
        return getContainerHeight() - drawer.getPaneHeight() * 1.5;
    }
    private Point2D getUpperMiddlePanePoint(Pane somePane){
        double x = somePane.getLayoutX() + somePane.getBoundsInParent().getWidth() / 2;
        double y = somePane.getLayoutY();
        return new Point2D(x,y);
    }

    private Point2D getLowerMiddlePanePoint(Pane somePane){
        double x = somePane.getLayoutX() + somePane.getBoundsInLocal().getWidth() / 2;
        double y = somePane.getLayoutY() + somePane.getBoundsInLocal().getHeight();
        return new Point2D(x,y);
    }

    //gets a pane, and generates a list of x bounds (as points) for a subtree space of each
    private List<Point2D> getPointsForChildren(Pane somePane, int children, double startx, double endx){
        double blockSize = Math.abs(startx - endx)/ (children);
        List<Point2D> locations = new ArrayList<>();
        for (int i = 0; i < children; i++){
            double start = i * blockSize + startx;
            double end = (i + 1) * blockSize + startx;
            locations.add(new Point2D(start, end));
        }
        return locations;
    }

    private double getXMiddleLocationForPane(Point2D xbounds, Pane somePane){
        return xbounds.getX() + (xbounds.getY() - xbounds.getX()) / 2  - somePane.getBoundsInLocal().getWidth() / 2;
    }

    private class CommitGraphDetails {
        public Commit commit;
        public double yValue;
        public double lowerXBound;
        public double upperXBound;

        public CommitGraphDetails(Commit commit, double yValue, double lowerXBound, double upperXBound) {
            this.commit = commit;
            this.yValue = yValue;
            this.lowerXBound = lowerXBound;
            this.upperXBound = upperXBound;
        }

        public Point2D toXBoundsPoint() {return new Point2D(lowerXBound, upperXBound);}
        @Override
        public String toString() {
            return "Commit " + commit + "(" + lowerXBound + "," + upperXBound + ")";
        }
    }
    private double generateNextYValueForPane(Pane somePane, double currentY){
        return currentY - somePane.getBoundsInLocal().getHeight() - YSPACING;
    }
    private double getContainerHeight(){
        return graphicalView.getBoundsInLocal().getHeight();
    }
    private double getContainerWidth(){
        return graphicalView.getBoundsInLocal().getWidth();
    }

    private double getYValueForCommit(Pane somePane, CommitGraphDetails details) throws Exception{
        //a hack to get java to let us change the value inside the lambda
        if (this.sortByDate){
            int index = sorted_commits.indexOf(details.commit);
            return  -(drawer.getPaneHeight() * index + (index - 1) * YSPACING);
        }

        double[] loc = { 0.0};
        loc[0] = Math.min(getContainerHeight(), details.yValue);
        details.commit.applyOnParents(parent ->{
            Commit parentCommit = provider.getCommitBySha1(parent);
            loc[0] = Math.min(commitToUpperPaneAndPoint.get(parentCommit).getLayoutY(), loc[0]);
        });

        return generateNextYValueForPane(somePane, loc[0]);
    }


    public void updateGrid() throws Exception{
        if (!isValid){
            return;
        }
        Map<String, List<Branch>> map = provider.getCommitBranchMap();
        graphicalView.getChildren().clear();
        log.info("--------------updating commit graph--------------");
        Queue<CommitGraphDetails> commitPaneBounds = new ArrayDeque<>();
        commitToUpperPaneAndPoint = new HashMap<>();
        commitPaneBounds.add(new CommitGraphDetails(root, getYForPane(), 0, totalWidth));
        double minSize = Double.POSITIVE_INFINITY;
        //TopologicalOrderIterator iterator = new TopologicalOrderIterator(commitGraph);
        while (!commitPaneBounds.isEmpty()){
            CommitGraphDetails currentCommitData = commitPaneBounds.remove();
            log.info("Drawing " + currentCommitData);
            if (!commitParentsHandled(currentCommitData)){
                commitPaneBounds.add(currentCommitData);
                continue;
            }

            if (commitToUpperPaneAndPoint.containsKey(currentCommitData.commit)){
                continue;
            }
            Pane commitPane = drawer.draw(currentCommitData.commit,
                    map.getOrDefault(currentCommitData.commit.getSha1Identifier(), null),
                    provider,
                    listener,
                    boldCommits.contains(currentCommitData.commit));
            graphicalView.getChildren().add(commitPane);
            commitToUpperPaneAndPoint.put(currentCommitData.commit, commitPane);

            //force the layout to render and compute true values of the positions and sizes
            //look at: https://stackoverflow.com/questions/26152642/get-the-height-of-a-node-in-javafx-generate-a-layout-pass
            graphicalView.applyCss();
            graphicalView.layout();

            double currentY = getYValueForCommit(commitPane, currentCommitData);
//            minSize = currentY < minSize ? currentY : minSize;
            commitPane.setLayoutY(getYValueForCommit(commitPane, currentCommitData));
            commitPane.setLayoutX(getXMiddleLocationForPane(currentCommitData.toXBoundsPoint(), commitPane));
            drawLineToParents(commitPane, currentCommitData);

            List<Commit> successors = Graphs.successorListOf(commitGraph, currentCommitData.commit);
            List<Point2D> locations = getPointsForChildren(commitPane, successors.size(), currentCommitData.lowerXBound, currentCommitData.upperXBound);
            double nextY = generateNextYValueForPane(commitPane, currentCommitData.yValue);
            for (Commit successor: successors){
                log.info("adding child "+ currentCommitData.commit + "-> " + successor);
                Point2D xBounds = locations.remove(0);
                if (!commitToUpperPaneAndPoint.containsKey(successor)){
                    commitPaneBounds.add(new CommitGraphDetails(successor, nextY, xBounds.getX(), xBounds.getY()));
                }
            }
        }
//        if (minSize < 0){
//            graphicalView.setPrefHeight(graphicalView.getBoundsInLocal().getHeight() + -minSize);
//        }
        graphicalView.layout();
    }

    private boolean commitParentsHandled(CommitGraphDetails details) throws Exception {
        Commit currentCommit = details.commit;
        return currentCommit.allParents(parent ->{
            Commit parentCommit = provider.getCommitBySha1(parent);
            return commitToUpperPaneAndPoint.containsKey(parentCommit);
        });
    }
    private void drawLineToParents(Pane commitPane, CommitGraphDetails currentCommitData, Commit parent) {
        if (parent == null){
            return;
        }
        Pane parentPane = commitToUpperPaneAndPoint.get(parent);
        Point2D parentPoint = getUpperMiddlePanePoint(parentPane);
        Point2D commitPoint = getLowerMiddlePanePoint(commitPane);
        Line connector = new Line(commitPoint.getX(), commitPoint.getY(),
                parentPoint.getX(), parentPoint.getY());
        if (boldCommits.contains(currentCommitData.commit)){
            connector.setStrokeWidth(5);
        }
        graphicalView.getChildren().add(connector);
    }
    
    private void drawLineToParents(Pane commitPane, CommitGraphDetails currentCommitData) throws Exception{
        Commit currentCommit = currentCommitData.commit;
        currentCommit.applyOnParents(parent -> {
            Commit parentCommit = provider.getCommitBySha1(parent);
            drawLineToParents(commitPane, currentCommitData, parentCommit);
        });
    }
}
