package com.magit.javafx.ui.tasks;

import com.magit.core.controllers.CommitGraphGenerator;
import com.magit.core.exceptions.RepositoryUninitializedException;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.tree.CommitGraphDrawer;
import com.magit.javafx.ui.tree.DummyCommitDrawer;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Group;
import javafx.scene.layout.Pane;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CommitTreeDrawerTask extends Task<Void> {
    private IInformationProvider provider;
    private Group group;
    private Pane layoutPane;
    private List<Commit> bold;
    private boolean allCommits;
    private boolean sortByDate;
    public CommitTreeDrawerTask(IInformationProvider provider, Group group,
                                Pane layoutPane, List<Commit> bold, boolean allCommits, boolean sortByDate) {
        this.provider = provider;
        this.group = group;
        this.layoutPane = layoutPane;
        this.bold = bold;
        this.allCommits = allCommits;
        this.sortByDate = sortByDate;
    }

    private CommitGraphGenerator getGraphGenerator() throws Exception{
        if (allCommits){
            return new CommitGraphGenerator(provider, provider.getLeafsCommits());
        }
        Set<Commit> commits = new HashSet<>();
        Set<String> commitIds = provider.getCommitBranchMap().keySet();
        for (String commitId : commitIds){
            commits.add(provider.getCommitBySha1(commitId));
        }
        return new CommitGraphGenerator(provider, commits);
    }
    @Override
    protected Void call() throws Exception {
        if (!provider.isRepositoryLoaded()){
            return null;
        }
        try {
            CommitGraphGenerator generator = getGraphGenerator();
            Graph<Commit, DefaultEdge> commitGraph = generator.produceTreeGraph();
            Platform.runLater(() -> {
                try {
                    CommitGraphDrawer drawer = new CommitGraphDrawer(provider, commitGraph, group, new DummyCommitDrawer(),
                            layoutPane.getBoundsInLocal().getWidth(), new HashSet<>(this.bold), this.sortByDate);
                    drawer.setCommitChosenListener(UIController.<MainWindow>getController());
                    drawer.setErrorListener(UIController.<MainWindow>getController());
                    drawer.updateGrid();
                    layoutPane.layout();
                    group.layout();
                } catch (Exception e) {
                    UIController.<MainWindow>getController().onError(e);
                }
            });
        }catch (RepositoryUninitializedException e){
            //should do nothing...
        } catch (Exception e){
            Platform.runLater(() -> {
                UIController.<MainWindow>getController().onError(e);
            });
        }
        return null;
    }
}
