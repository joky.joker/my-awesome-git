package com.magit.javafx.ui.controllers;

import com.magit.core.factories.StagingCommitFactory;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.merge.FileStateProvider;
import com.magit.core.objects.Blob;
import com.magit.core.objects.Commit;
import com.magit.core.objects.MagiFileInfo;
import com.magit.core.objects.MagiObjectType;
import com.magit.javafx.ui.MainWindow;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.util.Pair;
import org.apache.commons.io.FileUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class CommitDetailsController extends UIController {
    private static Logger log = Logger.getLogger(CommitDetailsController.class.getTypeName());
    private Map<String, MagiFileInfo> commitDetails;
    private Commit focusedCommit;

    private boolean isFirstParent;

    @FXML
    ListView addedFilesListView;
    @FXML
    ListView removedFilesListView;
    @FXML
    ListView modifiedFilesListView;
    @FXML
    HBox hboxParentChooser;
    @FXML
    SplitMenuButton splitMenuButton;

    //commit content
    @FXML
    ListView conflictFilesListView;
    @FXML
    TextArea fileContentTextArea;


    @Override
    public void onRefreshRequested() {
        try{
            render();
        }catch (Exception e){
            this.<MainWindow>getController().onError(e);
        }
    }

    @Override
    public void render() throws Exception {
        addedFilesListView.getItems().clear();
        modifiedFilesListView.getItems().clear();
        removedFilesListView.getItems().clear();
        if (focusedCommit == null){
            return;
        }

        if (focusedCommit.getParent() == null){
            //root commit
            addedFilesListView.getItems().setAll(provider.getCommitItemDetails(focusedCommit)
                    .keySet());
            this.splitMenuButton.setText("                                        ");
            return;
        }

        Commit parentCommit = focusedCommit.getParentCommit();
        if (focusedCommit.isMultiParent() && !isFirstParent){
            //merge child commit
            parentCommit = focusedCommit.getSecondParentCommit();
        }
        IFolderComparer comparer = StagingCommitFactory.createFolderComparer();
        Path currentCommitCheckout = Files.createTempDirectory("magit");
        Path parentCommitCheckout = Files.createTempDirectory("magit");

        focusedCommit.checkout(currentCommitCheckout);
        parentCommit.checkout(parentCommitCheckout);

        comparer.compare(currentCommitCheckout, parentCommitCheckout);

        addedFilesListView.getItems().setAll(comparer.getNewFiles());
        modifiedFilesListView.getItems().setAll(comparer.getModified());
        removedFilesListView.getItems().setAll(comparer.getRemoved());

        FileUtils.forceDelete(currentCommitCheckout.toFile());
        FileUtils.forceDelete(parentCommitCheckout.toFile());
    }

    public Commit getFocusedCommit() {
        return focusedCommit;
    }

    private void updateSplitMenuButton(){
        this.splitMenuButton.getItems().clear();
        if (focusedCommit == null){
            return;
        }
        this.splitMenuButton.setText(focusedCommit.getParent());
        try{
            this.focusedCommit.applyOnParents(parent ->{
                MenuItem menuItem = new MenuItem(parent);
                menuItem.setOnAction(action ->{
                    this.isFirstParent = (parent.equals(this.focusedCommit.getParent()));
                    this.splitMenuButton.setText(parent);
                    onRefreshRequested();
                });
                this.splitMenuButton.getItems().add(menuItem);
            });
        }catch (Exception e){}
    }

    public void updateCommitFiles() {
        conflictFilesListView.getItems().clear();
        if (focusedCommit == null){
            return;
        }
        try{
            commitDetails = getProvider().getCommitItemDetails(focusedCommit);
            conflictFilesListView.getItems().setAll(commitDetails.keySet());
        } catch (Exception e){
            log.log(Level.SEVERE, "failed to get commit items! fuck!", e);
        }
    }

    public void updateFileContent() {
        if (focusedCommit == null){
            return;
        }
        String file = (String) this.conflictFilesListView.getSelectionModel().getSelectedItem();
        if (commitDetails.get(file).getType() != MagiObjectType.BLOB){
            return;
        }
        try{
            Blob selectedFile =  (Blob) commitDetails.get(file).loadMagiObj();
            fileContentTextArea.setText(selectedFile.getContent());
            if (selectedFile.getContent().isEmpty()){
                fileContentTextArea.setText("File is empty");
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "failed to obtain blob object!");
        }
    }

    public void setFocusedCommit(Commit focusedCommit) {
        if (this.focusedCommit == focusedCommit){
            return;
        }
        this.focusedCommit = focusedCommit;
        this.isFirstParent = true;
        this.hboxParentChooser.setDisable(focusedCommit == null ? true : !focusedCommit.isMultiParent());
        updateSplitMenuButton();
        updateCommitFiles();
        Platform.runLater(()->{
            onRefreshRequested();
        });
    }
}
