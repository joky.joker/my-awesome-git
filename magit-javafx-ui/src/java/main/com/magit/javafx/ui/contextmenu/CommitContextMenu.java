package com.magit.javafx.ui.contextmenu;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.MenuItem;

import java.util.List;


public abstract class CommitContextMenu {
    public abstract MenuItem createMenuItem(Commit commit, List<Branch> branchList,
                                            CommitGraphDrawerListener listener,
                                            IUserInterfaceListener userInterfaceListener,
                                            IInformationProvider provider);
    public boolean shouldBeCreated(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IInformationProvider provider){
        return true;
    }

    protected void onError(Exception e){
        UIController.<MainWindow>getController().onError(e);
    }

    protected static String getHeadOrNon(IInformationProvider provider){
        try{
            return "'" + provider.getHead() + "'";
        } catch (Exception e){
            return "Head";
        }
    }

}
