package com.magit.javafx.ui.controllers;

import com.magit.core.merge.ConflictDetails;
import com.magit.core.merge.FileState;
import com.magit.javafx.ui.MainWindow;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class ConflictWindowSolver extends UIController {
    private List<ConflictDetails> conflictDetails;
    private Consumer<Boolean> onFinished;
    private Stage currentStage;

    private int currentConflict = 0;

    @FXML
    AnchorPane mainLayout;

    @FXML
    TextArea headVersion;

    @FXML
    TextArea baseVersion;

    @FXML
    TextArea otherVersion;

    @FXML
    Label conflictsRemain;

    @FXML
    Label filePath;

    @FXML
    TextArea finalVersion;

    private void onCloseRequested(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Canceling merge operation");
        alert.setTitle("Are you sure you want to cancel the merge operation?");
        alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.CANCEL);
        Optional<ButtonType> option = alert.showAndWait();
        option.ifPresent(buttonType -> {
            if (buttonType.equals(ButtonType.YES)){
                UIController.<MainWindow>getController().getWindow().getScene().getRoot().setEffect(null);
                this.onFinished.accept(false);
                currentStage.close();
            }
        });
    }

    public void init(Parent root, FXMLLoader loader) throws IOException{
        Stage stage = new Stage();
        stage.setTitle("Merge Conflict solver");
        stage.setScene(new Scene(root, 450, 450));
        stage.initModality(Modality.APPLICATION_MODAL);
        GaussianBlur blur = new GaussianBlur();
        UIController.<MainWindow>getController().getWindow().getScene().getRoot().setEffect(blur);
        stage.show();
        this.currentStage = stage;
        stage.setOnCloseRequest(evt ->{
            evt.consume();
            onCloseRequested();
        });
        updateConflictWindows();
    }

    private void updateRemainingConflicts(){
        int remain = this.conflictDetails.size() - this.currentConflict;
        this.conflictsRemain.setText(remain + "/" + this.conflictDetails.size() + " Conflicts Remain");
    }

    public static void solveConflicts(List<ConflictDetails> conflictDetails, Consumer<Boolean> onFinished) throws IOException {
        if (conflictDetails.isEmpty()){
            onFinished.accept(true);
            return;
        }
        FXMLLoader loader = new FXMLLoader(UIController.getUIListener().getClass().getResource("/ConflictSolverWindow.fxml"));
        Parent root = loader.load();
        ConflictWindowSolver solver = loader.getController();
        solver.conflictDetails = conflictDetails;
        solver.onFinished = onFinished;
        solver.init(root, loader);
    }
    @Override
    public void onRefreshRequested() {

    }

    private void applyStateToTextArea(TextArea area, FileState state){
        if (state.getState().equals(FileState.State.NON_EXISTING)){
            area.setDisable(true);
            area.setText("File was deleted");
        } else {
            area.setText(state.getContent());
            area.setDisable(false);
        }
    }
    public void updateConflictWindows(){
        ConflictDetails conflict = conflictDetails.get(currentConflict);
        applyStateToTextArea(headVersion, conflict.getHeadVersion());
        applyStateToTextArea(baseVersion, conflict.getBaseVersion());
        applyStateToTextArea(otherVersion, conflict.getOtherVersion());
        finalVersion.setText("");
        filePath.setText(conflict.getFinalVersionFile().normalize().toString());
    }
    public void onMarkResolved(){
        FileState state = new FileState(FileState.State.EXISTING, finalVersion.getText());
        if (finalVersion.getText().isEmpty()){
            String content = "You have left the final version box empty, do you want to delete the file or keep it empty?" +
                    "\nPress Yes to delete, No to keep it empty";
            Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION,
                    content, ButtonType.YES, ButtonType.NO);
            confirmation.setTitle("Deletion of file");
            confirmation.setHeaderText("Deletion of file");
            Optional<ButtonType> buttonType =  confirmation.showAndWait();
            if (!buttonType.isPresent()){
                return;
            }

            if (buttonType.get().equals(ButtonType.YES)){
                state.setState(FileState.State.NON_EXISTING);
            }
        }
        conflictDetails.get(currentConflict).setFinalVersion(state);
        currentConflict += 1;
        if (currentConflict == conflictDetails.size()){
            this.onFinished.accept(true);
            UIController.<MainWindow>getController().getWindow().getScene().getRoot().setEffect(null);
            UIController.<MainWindow>getController().onRefreshRequested();
            this.currentStage.close();
            return;
        }
        updateRemainingConflicts();
        updateConflictWindows();
    }

    @Override
    public void render() throws Exception {

    }
}
