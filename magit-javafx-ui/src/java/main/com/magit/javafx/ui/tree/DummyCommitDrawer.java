package com.magit.javafx.ui.tree;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.interfaces.CommitDrawer;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.layout.Pane;

import java.util.List;


public class DummyCommitDrawer implements CommitDrawer {

    static CommitPane somePane = new CommitPane(null, null, null, null, true);

    @Override
    public Pane draw(Commit currentCommit, List<Branch> orDefault, IInformationProvider provider, CommitGraphDrawerListener listener, boolean isBold) {
        return new CommitPane(currentCommit, orDefault, provider, listener, isBold);
    }

    @Override
    public double getPaneWidth() {
        somePane.applyCss();
        somePane.layout();
        return somePane.getBoundsInLocal().getWidth();
    }

    @Override
    public double getPaneHeight() {
        somePane.applyCss();
        somePane.layout();
        return somePane.getBoundsInLocal().getHeight();
    }

}
