package com.magit.javafx.ui.tasks;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.UIController;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Consumer;

public class XmlLoaderTask extends Task<Void> {
    private Path xml;
    private IUserInterfaceListener listener;
    private IInformationProvider provider;

    public void setOnFinished(Consumer<Void> onFinished) {
        this.onFinished = onFinished;
    }

    private Consumer<Void> onFinished;

    public XmlLoaderTask(Path xmlPath, IUserInterfaceListener listener, IInformationProvider provider){
        this.xml = xmlPath;
        this.listener = listener;
        this.provider = provider;
    }

    @Override
    protected Void call() throws Exception {
        try{
            Path path = listener.onRepoXmlLoaded(xml.toString());
            Platform.runLater(() -> {
                try{
                    if (path.toFile().exists()){
                        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
                        confirmation.setTitle("Path exists");
                        confirmation.setHeaderText(path.toString() + " already exist, override?");
                        Optional<ButtonType> result = confirmation.showAndWait();
                        if (!result.isPresent() || result.get().getButtonData().isCancelButton()){
                            return;
                        }
                    }
                    listener.onXmlDump();
                    listener.onRepoOpened(path.toString());
                    if (provider.isInitilized()){
                        listener.onSetHeadBranchCheckout(provider.getHead());
                    }

                    if (onFinished != null){
                        onFinished.accept(null);
                    }
                } catch (Exception e){
                    UIController.<MainWindow>getController().onError(e);
                }
            });
            return null;
        } catch (Exception e){
            Platform.runLater(() -> {
                UIController.<MainWindow>getController().onError(e);
            });
        }
        return null;
    }
}
