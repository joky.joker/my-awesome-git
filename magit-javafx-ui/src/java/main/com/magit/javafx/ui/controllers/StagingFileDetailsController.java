package com.magit.javafx.ui.controllers;

import com.magit.javafx.ui.MainWindow;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.util.Duration;

import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

public class StagingFileDetailsController extends UIController {
    @FXML
    Button refreshButton;

    @FXML
    Button commitButton;

    @FXML
    ListView addedFilesListView;
    @FXML
    ListView removedFilesListView;
    @FXML
    ListView modifiedFilesListView;

    Timeline timeline;
    public StagingFileDetailsController(){
        timeline = new Timeline(new KeyFrame(Duration.seconds(5), ev -> {
            onRefreshRequested();
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    @Override
    public void onRefreshRequested() {
        try{
            render();
        }catch (Exception e){}
    }

    @Override
    public void render() throws Exception {
        refreshButton.setDisable(!provider.isRepositoryLoaded());
        if (provider.isRepositoryLoaded()){
            addedFilesListView.getItems().setAll(provider.getNewUnstagedFiles());
            modifiedFilesListView.getItems().setAll(provider.getModifiedUnstagedFiles());
            removedFilesListView.getItems().setAll(provider.getRemovedUnstagedFiles());
            commitButton.setDisable(!areChangesPending());
        }
    }

    public boolean areChangesPending(){
        return !addedFilesListView.getItems().isEmpty() ||
                !modifiedFilesListView.getItems().isEmpty() ||
                !removedFilesListView.getItems().isEmpty();
    }

    public void onCommit() throws Exception{
        if (!provider.isChangesPending()){
            this.<MainWindow>getController().onError("No changes pending");
        }
        TextInputDialog dialog = new TextInputDialog();
        dialog.setHeaderText("New Commit");
        dialog.setContentText("Please provide a commit message");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(str ->{
            try{
                getUIListener().onCommit(str.trim());
                this.<MainWindow>getController().onRefreshRequested();
            }catch (Exception e){
                this.<MainWindow>getController().onError(e);
            }
        });
    }

    public void onRefresh() throws Exception {
        render();
    }
}
