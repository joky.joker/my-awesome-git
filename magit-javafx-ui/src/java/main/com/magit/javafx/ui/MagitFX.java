package com.magit.javafx.ui;

import com.magit.javafx.ui.controllers.UIController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class MagitFX extends Application {
    public static MagitFX instance;

    public static MagitFX getInstance() {
        return instance;
    }
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        instance = this;
        try{
            primaryStage.setTitle("Magit folder");
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/MainWindowPane.fxml"));
            Parent root = loader.load();
            UIController.setLoader(loader);
            MainWindow main = loader.getController();
            Scene scene = new Scene(root, 500, 600);
            primaryStage.setTitle("Magit GUI JavaFX");
            primaryStage.getIcons().add(new Image(MagitFX.class.getResourceAsStream("/icon.png")));
            primaryStage.setMaximized(true);
            primaryStage.setScene(scene);
            primaryStage.show();
            root.layout();
            main.init();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        Platform.exit();
        System.exit(0);
    }
}
