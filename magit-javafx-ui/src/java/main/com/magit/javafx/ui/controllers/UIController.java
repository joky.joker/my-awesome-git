package com.magit.javafx.ui.controllers;

import com.magit.core.controllers.Controller;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import javafx.fxml.FXMLLoader;

public abstract class UIController {
    static IInformationProvider provider;
    static IUserInterfaceListener listener;

    static FXMLLoader loader;

    public UIController(){
        if (provider == null || listener == null){
            Controller controller = new Controller();
            provider = controller;
            listener = controller;
        }
    }
    public static IInformationProvider getProvider() {
        return provider;
    }

    public static IUserInterfaceListener getUIListener() {
        return listener;
    }

    public static FXMLLoader getLoader() {
        return loader;
    }

    public static <T> T getController(){
        return loader.<T>getController();
    }

    public static void setLoader(FXMLLoader loader) {
        UIController.loader = loader;
    }

    public abstract void onRefreshRequested();
    public abstract void render() throws Exception;
}
