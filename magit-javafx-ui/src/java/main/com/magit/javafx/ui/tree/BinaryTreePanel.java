package com.magit.javafx.ui.tree;

import com.magit.core.objects.Commit;
import com.magit.core.utils.MagitFS;
import com.magit.javafx.ui.interfaces.CommitDrawer;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Pair;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;
import java.util.stream.Collectors;

public class BinaryTreePanel<T> {
    private VBox graphicalView;
    private MagitFS fs;
    private Commit root;
    private CommitDrawer drawer;
    private Map<String, Pair<Pane, Bounds>> commitToUpperPaneAndPoint;
    private Graph<Commit, DefaultEdge> commitGraph;

    public BinaryTreePanel(MagitFS fs, Commit root, Graph<Commit, DefaultEdge> graph, VBox grid, CommitDrawer drawer) throws Exception{
        this.graphicalView = grid;
        this.fs = fs;
        List<Commit> roots = graph.vertexSet().stream().filter(commit -> graph.inDegreeOf(commit) == 0).collect(Collectors.toList());
        if (roots.size() != 1){
            System.out.println(roots.stream().map(commit -> commit.getSha1Identifier()).collect(Collectors.toList()));
            throw new Exception("invalid commit graph received!");
        }
        this.root = roots.get(0);
        this.drawer = drawer;
        this.commitGraph = graph;
    }

    void drawParentCommitLine(Commit childCommit, Pane currnetCommitPane){
        if (childCommit == null){
            return;
        }
        //Pane drawingLinePane = commitToUpperPaneAndPoint.get(childCommit).getKey();
        //Bounds bounds = commitToUpperPaneAndPoint.get(childCommit).getValue();
        //Line line = new Line();
        //TODO: make it work
    }

    void addParentToStack(Queue<Pair<Commit, Integer>> levels, Commit commit, int level) throws Exception{
        if (commit.getParent() != null){
            Commit parent = fs.readCommitObject(commit.getParent());
            levels.add(new Pair(parent, level + 1));
        }
        if (commit.getSecondParent() != null){
            Commit parent = fs.readCommitObject(commit.getSecondParent());
            levels.add(new Pair(parent, level + 1));
        }
    }

    public void updateGrid(){
        Queue<Pair<Commit, Integer>> levels = new ArrayDeque<>();
        commitToUpperPaneAndPoint = new HashMap<>();
        levels.add(new Pair(root, 0));
        int lastLevel = -1;
        System.out.println("root is " + root.getSha1Identifier());
        HBox currentHbox = new HBox();
        Pane upperPane = new Pane();

        while (!levels.isEmpty()){
            Pair<Commit, Integer> pair = levels.remove();
            int level = pair.getValue();
            Commit currentCommit = pair.getKey();

            if (level != lastLevel){
                System.out.println("level upgraded!");
                currentHbox = new HBox();
                currentHbox.setSpacing(40);
                currentHbox.setAlignment(Pos.BOTTOM_CENTER);
                graphicalView.getChildren().add(0, currentHbox);
                upperPane = new Pane();
                graphicalView.getChildren().add(0, upperPane);
                lastLevel = level;
            }
            Pane pane = new Pane();//drawer.draw(currentCommit, fs);
            currentHbox.prefHeight(pane.getHeight());
            currentHbox.getChildren().add(0, pane);
            Bounds bounds = pane.localToScene(currentHbox.getChildren().get(0).getBoundsInLocal());
            commitToUpperPaneAndPoint.put(currentCommit.getSha1Identifier(), new Pair(upperPane, bounds));
            System.out.println("looking for commit " + currentCommit.getSha1Identifier());
            System.out.println(commitGraph.edgeSet());
            List<Commit> neighbors = Graphs.successorListOf(commitGraph, currentCommit);
            for (Commit neighbor: neighbors){
                System.out.println("drawing child" + neighbor.getSha1Identifier());
                drawParentCommitLine(neighbor, pane);
                levels.add(new Pair<>(neighbor, level + 1));
            }
        }
    }
}
