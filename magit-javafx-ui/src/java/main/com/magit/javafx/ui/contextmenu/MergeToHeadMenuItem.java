package com.magit.javafx.ui.contextmenu;

import com.magit.common.utils.FileSystemFactory;
import com.magit.core.factories.StagingCommitFactory;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IMerger;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.merge.ConflictDetails;
import com.magit.core.merge.MergeExecutor;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.ConflictWindowSolver;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.Alert;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

public class MergeToHeadMenuItem extends CommitContextMenu {
    public MenuItem createMenuItemForBranch(Branch branch, IInformationProvider provider,
                                            IUserInterfaceListener userInterfaceListener, CommitGraphDrawerListener listener){
        MenuItem menuItem = new MenuItem(branch.getName());
        menuItem.setOnAction(event -> {
            try{
                onMenuChosen(branch, provider, userInterfaceListener, listener);
            } catch (Exception e){
                onError(e);
            }
        });
        return menuItem;
    }

    @Override
    public MenuItem createMenuItem(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IUserInterfaceListener userInterfaceListener, IInformationProvider provider) {
        String head = getHeadOrNon(provider);
        Menu mergeMenu = new Menu("Merge into " + head);
        for (Branch branch: branchList){
            if (branch.isRemote()){
                continue;
            }
            MenuItem menuItem = createMenuItemForBranch(branch, provider, userInterfaceListener ,listener);
            mergeMenu.getItems().add(menuItem);
        }
        return mergeMenu;
    }

    public boolean shouldBeCreated(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IInformationProvider provider){
        try{
            if (branchList == null){
                return false;
            }
            if (provider.getHeadBranch().getCommitPointed().equals(commit.getSha1Identifier())){
                return false;
            }
            if (branchList.stream().anyMatch(branch -> !branch.isRemote())){
                return true;
            }
            return false;
        }catch (Exception e){}
        return false;
    }

    public String getCommitMessage(){
        TextInputDialog inputDialog = new TextInputDialog();
        inputDialog.setHeaderText("Commit message");
        inputDialog.setContentText("Please enter a commit message:");
        Optional<String> input = inputDialog.showAndWait();
        if (!input.isPresent()){
            return null;
        }
        return input.get();
    }

    public void onMenuChosen(Branch branch, IInformationProvider provider, IUserInterfaceListener userInterfaceListener,
                             CommitGraphDrawerListener listener) throws Exception {
        if (provider.isChangesPending()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Merge Error");
            alert.setHeaderText("Head branch contains uncommitted changes");
            alert.show();
            return;
        }

        MergeExecutor executor = userInterfaceListener.onMerge(branch.getName());
        if (executor.isFFMerge()){
            userInterfaceListener.overrideBranchCommit(provider.getHead(),
                    branch.getCommitPointed());
            UIController.<MainWindow>getController().onRefreshRequested();
            Alert dialog = new Alert(Alert.AlertType.INFORMATION);
            dialog.setHeaderText("Fast forward merge");
            dialog.setContentText("Fast forward merge was performed");
            dialog.show();
            return;
        }

        List<ConflictDetails> details = executor.updateConflictList();
        ConflictWindowSolver.solveConflicts(details, hasSucceeded -> {
            if (!hasSucceeded){
                return;
            }
            try{
                String msg = getCommitMessage();
                //TODO: if canceling supported - we will have to move the head dir to the wc.
                if (msg == null){
                    //TODO: handle merge cancellation
                    return;
                }
                executor.execute(msg, provider.getCurrentUsername());
                //UIController.getUIListener().onCommit(message, branch.getCommitPointed());
                listener.onRefreshRequested();
            } catch (Exception e){
                onError(e);
            }
        });

    }
}
