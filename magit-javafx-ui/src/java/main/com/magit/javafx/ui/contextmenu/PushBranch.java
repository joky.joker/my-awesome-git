package com.magit.javafx.ui.contextmenu;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

import java.util.List;
import java.util.stream.Collectors;

public class PushBranch extends CommitContextMenu {
    @Override
    public MenuItem createMenuItem(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IUserInterfaceListener userInterfaceListener, IInformationProvider provider) {
        Menu menu = new Menu("Push branch to Remote repo");
        List<Branch> localBranches = branchList.stream()
                .filter(branch -> !branch.isRemote() && !branch.isTracking())
                .collect(Collectors.toList());
        for (Branch localBranch : localBranches){
            MenuItem item = new MenuItem(localBranch.getName());
            item.setOnAction(event -> {
                try{
                    userInterfaceListener.onPushLocalBranch(localBranch);
                    UIController.<MainWindow>getController().onRefreshRequested();
                } catch (Exception e){
                    UIController.<MainWindow>getController().onError(e);
                }
            });
            menu.getItems().add(item);
        }
        return menu;
    }

    @Override
    public boolean shouldBeCreated(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IInformationProvider provider){
        if (!provider.isRemoteRepo()){
            return false;
        }
        if (branchList == null){
            return false;
        }
        //whether there is a local branch in this commit
        return branchList.stream().anyMatch(branch -> !branch.isRemote() && !branch.isTracking());
    }
}
