package com.magit.javafx.ui.factories;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.contextmenu.*;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.MenuItem;

import java.util.List;

public class ContextMenuFactory {
    private static ContextMenuCreator instance;
    public static ContextMenuCreator getContextMenuCreator(){
        if (instance == null){
            ContextMenuCreator contextMenuCreator = new ContextMenuCreator();
            contextMenuCreator.addContextMenu(new NewLocalBranchMenuItem());
            contextMenuCreator.addContextMenu(new CheckoutMenuItem());
            contextMenuCreator.addContextMenu(new RemoveBranchMenuItem());
            contextMenuCreator.addContextMenu(new ResetHeadMenuItem());
            contextMenuCreator.addContextMenu(new MergeToHeadMenuItem());
            contextMenuCreator.addContextMenu(new NewRTBMenuItem());
            contextMenuCreator.addContextMenu(new PushBranch());
            contextMenuCreator.addContextMenu(new CommitContextMenu() {
                @Override
                public MenuItem createMenuItem(Commit commit, List<Branch> branchList,
                                               CommitGraphDrawerListener listener,
                                               IUserInterfaceListener userInterfaceListener,
                                               IInformationProvider provider) {
                    return new MenuItem("Close");
                }
            });
            instance = contextMenuCreator;
        }
        return instance;
    }
}
