package com.magit.javafx.ui;

import com.magit.common.CurrentUser;
import com.magit.common.utils.IsDebug;
import com.magit.core.factories.StringCompressorFactory;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.merge.FileState;
import com.magit.core.merge.FileStateProvider;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.controllers.CommitDetailsController;
import com.magit.javafx.ui.controllers.StagingFileDetailsController;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.controllers.UpperToolbarController;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import com.magit.javafx.ui.interfaces.ErrorListener;
import com.magit.javafx.ui.tasks.CommitTreeDrawerTask;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Window;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainWindow extends UIController implements CommitGraphDrawerListener, ErrorListener {
    static Logger log = Logger.getLogger(MainWindow.class.getTypeName());

    @FXML
    UpperToolbarController upperToolbarController;

    @FXML
    CommitDetailsController commitDetailsController;

    @FXML
    StagingFileDetailsController stageDetailsController;

    @FXML
    ScrollPane scrollPane;

    @FXML
    Pane layoutPane;

    @FXML
    VBox background;

    @FXML
    Group group;

    @FXML
    Label statusBar;

    @FXML
    Label repoPath;

    @FXML
    Label repoName;

    @FXML
    Label currBranch;

    @FXML
    ToggleButton allCommitsBtn;

    @FXML
    ToggleButton dateSortBtn;

    //DEBUG
    @FXML
    Button commit;
    @FXML
    Button openRepo1;
    @FXML
    Button openLocal;


    public void init(){
        if (IsDebug.isDebugEnvironment()){
            StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
            commit.setVisible(true);
            commit.setOnMouseClicked( event ->{
                try{
                    IInformationProvider provider = UIController.getProvider();
                    FileStateProvider prov = new FileStateProvider(provider.getHeadBranch().getPointerCommit());
                    FileState state = prov.getStateForPath(Paths.get("./PeaceAndLove.txt"));
                    File file = new File(provider.getCurrentRepositoryPath().toFile(), "PeaceAndLove.txt");
                    if (state.getState().equals(FileState.State.NON_EXISTING)){
                        FileUtils.writeStringToFile(file, "PEACE!", StandardCharsets.UTF_8);
                        UIController.getUIListener().onCommit("TEST Added P&L");
                    } else {
                        FileUtils.forceDelete(file);
                        UIController.getUIListener().onCommit("TEST Removed P&L");
                    }
                    this.onRefreshRequested();
                }catch (Exception e){
                    e.printStackTrace();
                }
            });
            openRepo1.setVisible(true);
            openRepo1.setOnMouseClicked(event -> {
                try{
                    UIController.getUIListener().onRepoOpened("C:\\repo1");
                    onRefreshRequested();
                    upperToolbarController.onRefreshRequested();
                }catch (Exception e){
                    e.printStackTrace();
                }
            });
            openLocal.setVisible(true);
            openLocal.setOnMouseClicked(event -> {
                try{
                    UIController.getUIListener().onRepoOpened("C:\\new_repo");
                    onRefreshRequested();
                    upperToolbarController.onRefreshRequested();
                }catch (Exception e){
                    e.printStackTrace();
                }
            });
        }
        layoutPane.setOnMouseClicked(clicked -> {
            if (clicked.getTarget().equals(background)){
                onCommitFocused(null);
            }
        });
        IInformationProvider provider = UIController.getProvider();
        repoPath.setText("Repository Path: " + getStringOrDefault(provider.getCurrentRepositoryLocation(), "N/A"));
        repoName.setText("Repository Name: " + getStringOrDefault(provider.getRepoName(), "N/A"));
        currBranch.setText("Current Branch: " + getStringOrDefault(provider.getHead(), "N/A"));
        setUsername(CurrentUser.getCurrentUser());
    }

    private static String getStringOrDefault(String str, String def){
        if (str != null && !str.isEmpty()){
            return str;
        }
        return def;
    }


    @Override
    public void render() throws Exception {
        setUsername(CurrentUser.getCurrentUser());
        IInformationProvider provider = UIController.getProvider();
        repoPath.setText("Repository Path: " + getStringOrDefault(provider.getCurrentRepositoryLocation(), "N/A"));
        repoName.setText("Repository Name: " + getStringOrDefault(provider.getRepoName(), "N/A"));
        currBranch.setText("Current Branch: " + getStringOrDefault(provider.getHead(), "N/A"));
        stageDetailsController.onRefreshRequested();
        CommitTreeDrawerTask drawerTask = new CommitTreeDrawerTask(getProvider(),
                group, layoutPane, new ArrayList<>(), allCommitsBtn.isSelected(), dateSortBtn.isSelected());
        new Thread(drawerTask).start();
    }

    public void setUsername(String username){
        CurrentUser.setCurrentUser(username);
        statusBar.setText("Current User: " + CurrentUser.getCurrentUser());
    }

    @Override
    public void onCommitFocused(Commit commit) {
        commitDetailsController.setFocusedCommit(commit);
        try{
            List<Commit> history = new ArrayList<>();
            if (commit != null){
                 history = getProvider().getCommitHistory(commit.getSha1Identifier());
            }
            CommitTreeDrawerTask drawerTask = new CommitTreeDrawerTask(getProvider(),
                    group, layoutPane, history, allCommitsBtn.isSelected(), dateSortBtn.isSelected());
            new Thread(drawerTask).start();
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    public void onRefreshRequested() {
        try{
            render();
        } catch (Exception e){
            onError(e);
        }
    }

    @Override
    public void onError(Exception error) {
        log.log(Level.SEVERE, "crap... ", error);
        onError(error.getMessage());
    }

    @Override
    public void onError(String error) {
        log.info("showing error....");
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Occured");
        alert.setHeaderText("An Error occured");
        alert.setContentText(error);
        alert.showAndWait();
    }


    public Window getWindow(){
        return this.layoutPane.getScene().getWindow();
    }

}
