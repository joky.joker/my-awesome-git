package com.magit.javafx.ui.contextmenu;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

import java.util.List;

public class NewRTBMenuItem extends CommitContextMenu {
    @Override
    public MenuItem createMenuItem(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IUserInterfaceListener userInterfaceListener, IInformationProvider provider) {
        Menu createRTB = new Menu("Create RTB for");
        branchList.stream().filter(Branch::isRemote).forEach(branch -> {
            MenuItem menuItem = new MenuItem(branch.getName());
            //TODO: see if there can be only one RTB for RB
            menuItem.setOnAction(event -> {
                try{
                    UIController.getUIListener().onNewRTBranchCreated(branch);
                    listener.onRefreshRequested();
                }catch (Exception e){
                    UIController.<MainWindow>getController().onError(e);
                }
            });
            createRTB.getItems().add(menuItem);
        });
        return createRTB;
    }

    @Override
    public boolean shouldBeCreated(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IInformationProvider provider) {
        if (branchList == null){
            return false;
        }
        return branchList.stream().anyMatch(Branch::isRemote);
    }
}
