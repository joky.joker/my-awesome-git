package com.magit.javafx.ui.contextmenu;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.ContextMenu;

import java.util.ArrayList;
import java.util.List;

public class ContextMenuCreator {
    List<CommitContextMenu> contextMenuList;

    public ContextMenuCreator(){
        contextMenuList = new ArrayList<>();
    }

    public void addContextMenu(CommitContextMenu contextMenu){
        contextMenuList.add(contextMenu);
    }

    public ContextMenu createContextMenu(Commit commit,
                                                List<Branch> branches,
                                                IInformationProvider provider,
                                                IUserInterfaceListener uiListener,
                                                CommitGraphDrawerListener listener){
        ContextMenu contextMenu = new ContextMenu();
        for (CommitContextMenu commitContextMenu : contextMenuList){
            if (!commitContextMenu.shouldBeCreated(commit, branches, listener, provider)){
                continue;
            }
            contextMenu.getItems().add(commitContextMenu.createMenuItem(commit, branches, listener, uiListener, provider));
        }
        return contextMenu;
    }
}
