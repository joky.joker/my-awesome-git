package com.magit.javafx.ui.contextmenu;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

import java.util.List;

public class RemoveBranchMenuItem extends CommitContextMenu {
    @Override
    public MenuItem createMenuItem(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IUserInterfaceListener userInterfaceListener, IInformationProvider provider) {
        Menu removeMenu = new Menu("Remove Branch");
        for (Branch branch: branchList){
            if (branch.isRemote()){
                continue;
            }
            MenuItem branchCheckout = new MenuItem(branch.getName());
            branchCheckout.setOnAction( event -> {
                try {
                    UIController.getUIListener().onBranchDeleted(branch.getName());
                    listener.onRefreshRequested();
                }catch (Exception e){
                    onError(e);
                }});
            removeMenu.getItems().add(branchCheckout);
        }
        return removeMenu;
    }

    @Override
    public boolean shouldBeCreated(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IInformationProvider provider){
        return branchList != null && branchList.stream().anyMatch(branch -> !branch.isRemote());
    }
}
