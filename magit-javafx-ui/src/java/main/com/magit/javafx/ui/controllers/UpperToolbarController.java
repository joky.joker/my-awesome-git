package com.magit.javafx.ui.controllers;

import com.magit.common.utils.IsDebug;
import com.magit.core.Main;
import com.magit.core.objects.Repository;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.tasks.XmlLoaderTask;
import javafx.application.Platform;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.TextInputDialog;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class UpperToolbarController extends UIController {

    @FXML
    Menu colabMenu;

    @Override
    public void onRefreshRequested() {
        colabMenu.setDisable(!UIController.getProvider().isRemoteRepo());
        this.<MainWindow>getController().onCommitFocused(null);
        this.<MainWindow>getController().onRefreshRequested();
    }

    @Override
    public void render() throws Exception {}

    public void onRepoOpenMenu(){
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Please choose the repo folder");
        directoryChooser.setInitialDirectory(new File("C:\\"));
        File repoDir = directoryChooser.showDialog(this.<MainWindow>getController().getWindow());
        if (repoDir == null){
            return;
        }

        Platform.runLater(() -> {
            try{
                getUIListener().onRepoOpened(repoDir.toPath().toString());
                onRefreshRequested();
            } catch (Exception e){
                e.printStackTrace();
                UIController.<MainWindow>getController().onError(e);
            }
        });
    }
    public void onRepoXmlOpened(){
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Please choose xml file to load");
        chooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml"));
        if (IsDebug.isDebugEnvironment()){
            File file = new File("E:\\Amit\\workspace\\Magit\\magit-core\\test-resources");
            if (file.isDirectory()){
                chooser.setInitialDirectory(file);
            }
        }
        File chosenXml = chooser.showOpenDialog(this.<MainWindow>getController().getWindow());
        if (chosenXml == null){
            return;
        }

        XmlLoaderTask loader = new XmlLoaderTask(chosenXml.toPath(), getUIListener(), getProvider());
        loader.setOnFinished(value ->{
            onRefreshRequested();
        });

        new Thread(loader).start();
    }

    public void onUserNameChanged(){
        TextInputDialog dialog = new TextInputDialog();
        dialog.setHeaderText("Change username");
        dialog.setContentText("Please enter new username:");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(str ->{
            this.<MainWindow>getController().setUsername(str.trim());
        });
    }

    public void onClone() {
        Platform.runLater(() ->{
            try{
                CloneDialogController.show();
                onRefreshRequested();
            } catch (Exception e){
                UIController.<MainWindow>getController().onError(e);
            }
        });
    }
    public void onFetch(){
        Platform.runLater(() ->{
            try{
                UIController.getUIListener().onFetch();
                onRefreshRequested();
            } catch (Exception e){
                UIController.<MainWindow>getController().onError(e);
            }
        });
    }

    public void onPull(){
        Platform.runLater(() ->{
            try{
                UIController.getUIListener().onPull();
                onRefreshRequested();
            } catch (Exception e){
                UIController.<MainWindow>getController().onError(e);
            }
        });
    }

    public void onPush(){
        Platform.runLater(() -> {
            try{
                UIController.getUIListener().onPush();
                onRefreshRequested();
            } catch (Exception e){
                UIController.<MainWindow>getController().onError(e);
            }
        });
    }

    public void onCreateNewRepo(ActionEvent actionEvent) {
        try{
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("choose directory to initialize repo");
            File dir = chooser.showDialog(UIController.<MainWindow>getController().getWindow());
            if (dir.listFiles().length != 0){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Directory isnt empty");
                alert.showAndWait();
                return;
            }
            TextInputDialog dialog = new TextInputDialog();
            dialog.setContentText("Please choose a name for the repository: ");
            Optional<String> input = dialog.showAndWait();
            if (!input.isPresent() || input.get().isEmpty()){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Opertaion canceled");
                alert.showAndWait();
                return;
            }

            Repository.createRepository(dir.toPath(), input.get());
            UIController.getUIListener().onRepoOpened(dir.getAbsolutePath());
            onRefreshRequested();
        } catch (Exception e){
            UIController.<MainWindow>getController().onError(e);
        }
    }

    public void onAbout(ActionEvent actionEvent) throws Exception{
        FXMLLoader loader = new FXMLLoader(UIController.getUIListener().getClass().getResource("/AboutDialog.fxml"));
        Parent root = loader.load();
        AboutController controller = loader.getController();
        Stage currentStage = new Stage();
        currentStage.setScene(new Scene(root));
        currentStage.initModality(Modality.APPLICATION_MODAL);
        controller.setStage(currentStage);
        currentStage.showAndWait();
    }
}
