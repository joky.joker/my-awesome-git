package com.magit.javafx.ui.tree;

import com.magit.common.utils.DefaultDateFormatterFactory;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.factories.ContextMenuFactory;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class CommitPane extends AnchorPane {
    private Commit commit;
    private IInformationProvider provider;
    private List<Branch> commitBranches;
    private Tooltip tip;
    private Circle circle;
    private ContextMenu contextMenu;
    private CommitGraphDrawerListener listener;
    private boolean isBold = false;

    public CommitPane(Commit commit, List<Branch> branch, IInformationProvider provider, CommitGraphDrawerListener listener, boolean isBold){
        this.commit = commit;
        this.provider = provider;
        this.commitBranches = branch;
        this.listener = listener;
        this.isBold = isBold;
        circle = new Circle(30);
        setCircleDefaults();

        contextMenu = ContextMenuFactory.getContextMenuCreator().createContextMenu(commit,
                branch,
                UIController.getProvider(),
                UIController.getUIListener(),
                listener);

        String innerText = "N/A";
        if (commit != null){
            innerText = commit.getSha1Identifier().substring(0, 4) + "...";
        }

        Text text = new Text(innerText);
        text.setBoundsType(TextBoundsType.VISUAL);
        StackPane stack = new StackPane();
        stack.getChildren().add(circle);
        stack.getChildren().add(text);

        HBox box = new HBox(stack);
        box.setAlignment(Pos.CENTER);
        this.getChildren().add(box);
        AnchorPane.setBottomAnchor(box, 0.0);
        AnchorPane.setTopAnchor(box, 0.0);
        AnchorPane.setRightAnchor(box, 0.0);
        AnchorPane.setLeftAnchor(box, 0.0);
        addEventFilter(MouseEvent.MOUSE_MOVED, event -> onMouseEntered(event));
        addEventFilter(MouseEvent.MOUSE_EXITED, event -> onMouseExited());
        addEventFilter(MouseEvent.MOUSE_PRESSED, event -> onMousePressed(event));
    }

    private void onMousePressed(MouseEvent event) {
        event.consume();
        if (!event.isSecondaryButtonDown()){
            if (listener != null){
                listener.onCommitFocused(commit);
            }
            return;
        }
        contextMenu.show(circle, event.getScreenX(), event.getScreenY());
    }

    private void setCircleDefaults(){
        if (circle == null){
            return;
        }
        circle.setFill(Color.WHITE);
        circle.setStroke(Color.GREEN);
        circle.setStrokeWidth(1);
        if (commitBranches != null){
            circle.setStrokeWidth(3);
            try{
                if (commit.getSha1Identifier().equals(provider.getHeadBranch().getCommitPointed())){
                    circle.setStrokeWidth(5);
                    circle.setFill(Color.CORAL);
                }
            } catch (Exception e){
            }
        }
        if (isBold){
            circle.setStrokeWidth(5);
            circle.setStroke(Color.BLACK);
        }

    }
    private void onMouseEntered(MouseEvent event){
        if (tip == null){
            tip = new Tooltip(getTooltipString());
        }
        tip.show(this.getScene().getWindow(), event.getScreenX() + 10, event.getScreenY() + 10);
        circle.setStroke(Color.BLUE);
        circle.setStrokeWidth(3);
    }

    private void onMouseExited(){
        setCircleDefaults();
        if (tip != null){
            tip.hide();
        }
    }

    private String createBranchesTooltip(){
        if (commitBranches == null){
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (Branch branch : commitBranches){
            String type = branch.isRemote() ? "Remote" : (branch.isTracking() ? "RTB" : "Local");
            builder.append(String.format("%s [%s]\n", type, branch.getFullName()));
        }
        builder.append("\n\n");
        return builder.toString();
    }

    private String getTooltipString(){
        StringBuilder builder = new StringBuilder();
        builder.append(createBranchesTooltip());
        builder.append(commit);
        builder.append("\n");

        if (commit == null){
            return builder.toString();
        }
        builder.append("Message: "+ commit.getMessage() + "\n");
        builder.append("Author: "+ commit.getAuthor() + "\n");
        SimpleDateFormat formatter = DefaultDateFormatterFactory.createDateFormatter();
        builder.append("Date: "+ formatter.format(commit.getDateOfCreation()) + "\n");
        if (commit.getParent() != null){
            builder.append("Parent1: " + commit.getParent() + "\n");
        }
        if (commit.getSecondParent() != null){
            builder.append("Parent2: " + commit.getSecondParent() + "\n");
        }
        return builder.toString();
    }
    @Override
    public String toString() {
        return "Commit pane at: "+ getBoundsInParent().getMinX() + ", " + getBoundsInParent().getMinY();
    }
}
