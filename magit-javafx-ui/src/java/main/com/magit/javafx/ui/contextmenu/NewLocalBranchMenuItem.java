package com.magit.javafx.ui.contextmenu;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.UIController;
import com.magit.javafx.ui.interfaces.CommitGraphDrawerListener;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;

import java.util.List;
import java.util.Optional;

public class NewLocalBranchMenuItem extends CommitContextMenu {
    @Override
    public MenuItem createMenuItem(Commit commit, List<Branch> branchList, CommitGraphDrawerListener listener, IUserInterfaceListener userInterfaceListener, IInformationProvider provider) {
        MenuItem createBranch = new MenuItem("Create new local branch here");
        createBranch.setOnAction(event -> {
            TextInputDialog dialog = new TextInputDialog();
            dialog.setHeaderText("New Branch");
            dialog.setContentText("Please enter new Branch name: ");
            Optional<String> result =dialog.showAndWait();
            result.ifPresent(str -> {
                try{
                    UIController.getUIListener().onNewLocalBranchCreated(str.trim(), commit.getSha1Identifier());
                    listener.onRefreshRequested();
                }catch (Exception e){
                    UIController.<MainWindow>getController().onError(e);
                }
            });
        });
        return createBranch;
    }
}
