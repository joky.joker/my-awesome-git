package com.magit.javafx.ui.interfaces;

public interface ErrorListener {
    void onError(Exception error);
    void onError(String error);
}
