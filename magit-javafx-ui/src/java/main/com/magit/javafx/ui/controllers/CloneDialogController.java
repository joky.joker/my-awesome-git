package com.magit.javafx.ui.controllers;

import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.common.utils.IsDebug;
import com.magit.core.objects.Repository;
import com.magit.javafx.ui.MainWindow;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.io.FileSystemUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CloneDialogController {

    private Stage currentStage;

    @FXML
    public TextField repositoryName;

    @FXML
    public TextField destinationPath;

    @FXML
    public TextField cloneRepoPath;


    public static void show() throws Exception {
        FXMLLoader loader = new FXMLLoader(UIController.getUIListener().getClass().getResource("/CloneDialog.fxml"));
        Parent root = loader.load();
        CloneDialogController dialogController = loader.getController();
        dialogController.currentStage = new Stage();
        dialogController.currentStage.setScene(new Scene(root));
        dialogController.currentStage.initModality(Modality.APPLICATION_MODAL);
        if (IsDebug.isDebugEnvironment()){
            dialogController.repositoryName.setText("Hayo");
            dialogController.destinationPath.setText("C:\\new_repo");
            Path currDir = FileSystemFactory.getFs().getPath("C:\\new_repo");
            FileUtilsHelper.deleteFileRec(currDir);
            Files.createDirectories(currDir);
            dialogController.cloneRepoPath.setText("C:\\repo1");
        }
        dialogController.currentStage.showAndWait();
    }
    public void onRepoChoose(){
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Choose Repository to clone");
        File repoDir = chooser.showDialog(currentStage.getScene().getWindow());
        if (repoDir == null){
            return;
        }
        if (!new File(repoDir, Repository.MAGIT_DIR_NAME).isDirectory()){
            onError("Invalid Magit repo chosen");
            return;
        }
        cloneRepoPath.setText(repoDir.toString());
    }

    public void onDestinationChoose(){
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Choose a destination for the cloned version");
        File repoDir = chooser.showDialog(currentStage.getScene().getWindow());
        if (repoDir == null){
            return;
        }
        destinationPath.setText(repoDir.toString());
    }

    public void onClone() {
        if (repositoryName.getText().isEmpty()){
            onError("Invalid repo name");
            return;
        }
        try{
            UIController.getUIListener().onClone(repositoryName.getText(),
                    Paths.get(destinationPath.getText()),
                    Paths.get(cloneRepoPath.getText()));
            UIController.getUIListener().onSetHeadBranchCheckout(UIController.getProvider().getHead());
            UIController.<MainWindow>getController().onRefreshRequested();
            currentStage.close();
            return;
        }catch ( Exception e){
            onError(e.getMessage());
        }
    }
    public void onError(String msg){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error occured");
        alert.setHeaderText("Error occured");
        alert.setContentText(msg);
        alert.showAndWait();
    }
}
