package com.magit.javafx.ui.interfaces;

import com.magit.core.objects.Commit;

public interface CommitGraphDrawerListener {
    void onCommitFocused(Commit commit);
    void onRefreshRequested();
}
