import com.magit.javafx.ui.MagitFX;
import com.magit.javafx.ui.MainWindow;
import com.magit.javafx.ui.controllers.UIController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

@ExtendWith({ApplicationExtension.class})
public class SimpleUITest {

    @Start
    public void start(Stage stage) throws Exception {
        stage.setTitle("Magit folder");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/MainWindowPane.fxml"));
        Parent root = loader.load();
        UIController.setLoader(loader);
        MainWindow main = loader.getController();
//        LogManager.getLogManager().reset();
//        Logger.getGlobal().setLevel(Level.OFF);
        Scene scene = new Scene(root, 500, 600);
        stage.setTitle("Magit GUI JavaFX");
        stage.setMaximized(true);
        stage.setScene(scene);
        stage.show();
        root.layout();
        main.init();
    }


    @Test
    public void loadfromXML(FxRobot robot){
    }
}
