import com.magit.core.objects.Blob;
import com.magit.core.objects.Folder;
import com.magit.core.objects.MagiFileInfo;
import com.magit.core.objects.MagiObjectType;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class FolderTest {

    public MagiFileInfo generateItem1(){
        Blob blob1 = new Blob("test1234");
        MagiFileInfo fileInfo1 = new MagiFileInfo("java", MagiObjectType.BLOB, "Amit", new Date());
        fileInfo1.setObjIdentifier(blob1.getSha1Identifier());
        return fileInfo1;
    }

    public MagiFileInfo generateItem2(){
        Blob blob1 = new Blob("test12345");
        MagiFileInfo fileInfo1 = new MagiFileInfo("java", MagiObjectType.BLOB, "Amit", new Date());
        fileInfo1.setObjIdentifier(blob1.getSha1Identifier());
        return fileInfo1;
    }
    @Test
    public void testMagiFileInfo(){
        MagiFileInfo file1 = generateItem1();
        MagiFileInfo file2 = generateItem1();
        Assert.assertEquals(file1.getSha1Identifier(), file2.getSha1Identifier());
    }

    @Test
    public void testItemOrder(){
        Folder folder1 = new Folder();
        Folder folder2 = new Folder();
        folder1.addFileOrFolder("fucka you1", generateItem1());
        folder1.addFileOrFolder("fucka you2", generateItem2());
        folder2.addFileOrFolder("fucka you1", generateItem1());
        folder2.addFileOrFolder("fucka you2", generateItem2());
        Assert.assertEquals(folder1.getSha1Identifier(), folder2.getSha1Identifier());
    }
}
