import com.magit.core.objects.Blob;
import com.magit.core.objects.MagitObject;
import org.junit.Assert;
import org.junit.Test;

public class BlobTest {

    @Test
    public void digestSha1() {
        Blob blob = new Blob("test123");
        Blob blob1 = new Blob("test123");
        Assert.assertEquals(blob.getSha1Identifier(), blob1.getSha1Identifier());
        MagitObject obj = blob;
        Assert.assertEquals(obj.getSha1Identifier(), blob1.getSha1Identifier());
        blob1.setContent("test1234");
        Assert.assertNotEquals(blob.getSha1Identifier(), blob1.getSha1Identifier());
    }

}