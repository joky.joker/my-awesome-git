import com.magit.core.factories.MagitSerializerFactory;
import com.magit.core.interfaces.IMagiObjectSerializer;
import com.magit.core.objects.Blob;
import com.magit.core.objects.Folder;
import com.magit.core.objects.MagiFileInfo;
import com.magit.core.objects.MagiObjectType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

public class SerializationTest {
    IMagiObjectSerializer<String> serializer;
    Date someDate;
    @Before
    public void before(){
        serializer = MagitSerializerFactory.createMagiSerilizer();
        someDate = new Date();
    }

    @Test
    public void serializeBlob() throws Exception{
        Blob blob = new Blob("test1234");
        Blob newBlob = (Blob)serializer.deserializeBlob(serializer.serializeBlob(blob)).getMagiObj();
        Assert.assertEquals(newBlob.getSha1Identifier(), blob.getSha1Identifier());
    }
    @Test
    public void serializeFolderWithMagiObj() throws Exception{
        Folder folder = new Folder();
        Blob blob1 = new Blob("test1234");
        MagiFileInfo fileInfo = new MagiFileInfo("java", MagiObjectType.BLOB, "Amit", someDate);
        fileInfo.setMagiObj(blob1);
        folder.addFileOrFolder("fucka you", fileInfo);
        String elem = serializer.serializeFolder(folder);
        Folder newFolder = (Folder)serializer.deserializeFolder(elem).getMagiObj();
        Assert.assertEquals(folder.getSha1Identifier(), newFolder.getSha1Identifier());
    }

    @Test
    public void serializeFolderWithoutMagiObj() throws Exception{
        Folder folder = new Folder();
        Blob blob1 = new Blob("test1234");
        MagiFileInfo fileInfo = new MagiFileInfo("java", MagiObjectType.BLOB, "Amit", someDate);
        fileInfo.setObjIdentifier(blob1.getSha1Identifier());
        folder.addFileOrFolder("fucka you", fileInfo);
        Folder newFolder = (Folder)serializer.deserializeFolder(serializer.serializeFolder(folder)).getMagiObj();
        Assert.assertEquals(folder.getSha1Identifier(), newFolder.getSha1Identifier());
    }
}
