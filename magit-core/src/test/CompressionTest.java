import com.magit.core.factories.StringCompressorFactory;
import com.magit.core.interfaces.IStringCompressor;
import org.junit.Assert;
import org.junit.Test;

public class CompressionTest {
    @Test
    public void testCompressionDecompression(){
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.REAL);
        IStringCompressor compressor = StringCompressorFactory.createStringCompressor();
        String data = "blabla1235412!<>\"";
        String newData = compressor.decompress(compressor.compress(data));
        Assert.assertEquals(data, newData);
    }
}
