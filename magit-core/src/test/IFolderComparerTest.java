import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.factories.StagingCommitFactory;
import com.magit.core.factories.StringCompressorFactory;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.objects.Repository;
import com.magit.core.serializers.RepoXmlLoader;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class IFolderComparerTest {
    private RepoLoaderTest tester;
    @Before
    public void before(){
        tester = new RepoLoaderTest();
    }
    public Repository loadAndCheckoutRepo(String xml) throws Exception{
        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(tester.loadFile(xml));
        Repository repo = repoXmlLoader.dumpRepo();
        repo.checkout(repo.getActiveBranch());
        return repo;
    }
    public Path checkoutBranchToTemp(Repository repo) throws Exception {
        Path tempDir = FileSystemFactory.createTempDirectory("magit");
        repo.checkout(repo.getActiveBranch(), tempDir);
        return tempDir;
    }

    @Test
    public void testNewRepo() throws Exception{
        loadAndCheckoutRepo("small/ex1-small.xml");
    }

    @Test
    public void testModified() throws Exception{
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        Repository repo = loadAndCheckoutRepo("small/ex1-small.xml");
        Files.write(FileSystemFactory.getFs().getPath("C:\\repo1\\fol1\\Foo.java"),
                "Foo new content".getBytes(StandardCharsets.UTF_8));
        Path tempDir = checkoutBranchToTemp(repo);
        IFolderComparer comp = StagingCommitFactory.createFolderComparer();
        comp.compare(repo.getRepoDir(), tempDir);

        Assert.assertArrayEquals(comp.getModified().toArray(), new Path[]{FileSystemFactory.getFs().getPath("fol1\\Foo.java")});
        Assert.assertEquals(comp.getNewFiles().size(), 0);
        Assert.assertEquals(comp.getRemoved().size(), 0);
        Assert.assertTrue(comp.isModified(FileSystemFactory.getFs().getPath("fol1\\Foo.java")));

        repo.commitStagedChanges("Aloha!", "TestUser");

        tempDir = checkoutBranchToTemp(repo);
        comp.compare(repo.getRepoDir(), tempDir);
        Assert.assertEquals(comp.getModified().size(), 0);
        Assert.assertEquals(comp.getNewFiles().size(), 0);
        Assert.assertEquals(comp.getRemoved().size(), 0);
    }

    @Test
    public void testNew() throws Exception{
        Repository repo = loadAndCheckoutRepo("small/ex1-small.xml");
        Files.write(FileSystemFactory.toPath("C:\\repo1\\fol1\\Foo2.java"), "asdasd".getBytes(StandardCharsets.UTF_8));

        Path tempDir = checkoutBranchToTemp(repo);
        IFolderComparer comp = StagingCommitFactory.createFolderComparer();
        comp.compare(repo.getRepoDir(), tempDir);

        Assert.assertArrayEquals(comp.getNewFiles().toArray(), new Path[]{FileSystemFactory.toPath("fol1\\Foo2.java")});
        Assert.assertEquals(comp.getModified().size(), 0);
        Assert.assertEquals(comp.getRemoved().size(), 0);
        Assert.assertTrue(comp.isNew(FileSystemFactory.toPath("fol1\\Foo2.java")));
    }

    @Test
    public void testRemoved() throws Exception{
        Repository repo = loadAndCheckoutRepo("small/ex1-small.xml");
        FileUtilsHelper.deleteFileRec(FileSystemFactory.toPath("C:\\repo1\\fol1"));
        Path tempDir = checkoutBranchToTemp(repo);
        IFolderComparer comp = StagingCommitFactory.createFolderComparer();
        comp.compare(repo.getRepoDir(), tempDir);

        Assert.assertArrayEquals(comp.getRemoved().toArray(), new Path[]{FileSystemFactory.toPath("fol1\\Foo.java")});
        Assert.assertEquals(comp.getModified().size(), 0);
        Assert.assertEquals(comp.getNewFiles().size(), 0);
        Assert.assertTrue(comp.isDeleted(FileSystemFactory.toPath("fol1\\Foo.java")));
    }

    @Test
    public void testLargeRepoRemoveThenAddAllCommit() throws Exception{
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        testLargeRepoRemoveThenAddAllCommit("large/ex1-large.xml", "deep");
        testLargeRepoRemoveThenAddAllCommit("large/ex1-large.xml", "master");
        testLargeRepoRemoveThenAddAllCommit("large/ex1-large.xml", "test");
    }

    @Test
    public void testMediumRepoRemoveThenAddAllCommit() throws Exception{
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        testLargeRepoRemoveThenAddAllCommit("medium/ex1-medium.xml", "deep");
        testLargeRepoRemoveThenAddAllCommit("medium/ex1-medium.xml", "master");
    }

    @Test
    public void testSmallRepoRemoveThenAddAllCommit() throws Exception{
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        testLargeRepoRemoveThenAddAllCommit("small/ex1-small.xml", "test");
        testLargeRepoRemoveThenAddAllCommit("small/ex1-small.xml", "master");
    }

    public void testLargeRepoRemoveThenAddAllCommit(String xml, String branch) throws Exception{
        Repository repo = loadAndCheckoutRepo(xml);
        repo.checkout(branch);
        try(Stream<Path> files = Files.list(repo.getRepoDir())){
            files.forEachOrdered(file ->{
                if (file.getFileName().toString().equals(Repository.MAGIT_DIR_NAME)){
                    return;
                }
                FileUtilsHelper.deleteFileRec(file);
            });
        }
        Path tempDir = checkoutBranchToTemp(repo);
        IFolderComparer beforeComparer = repo.getChanges();
        Assert.assertTrue("Comparer reported no changes", beforeComparer.hasChanges());

        //add some dummy file just so the folder wont be empty
        Files.write(repo.getRepoDir().resolve("empty.txt"), "some data".getBytes(StandardCharsets.UTF_8));

        repo.commitStagedChanges("some commit", "Testuser");

        IFolderComparer midComparer = repo.getChanges();
        Assert.assertTrue("Comparer reported changes exists", !midComparer.hasChanges());
        for (Path file: FileUtilsHelper.listFilesRelative(tempDir)){
            if (file.getFileName().toString().equals(Repository.MAGIT_DIR_NAME)){
                continue;
            }
            Path currentFile = tempDir.resolve(file);
            if (Files.isRegularFile(currentFile)){
                Files.copy(tempDir.resolve(file), repo.getRepoDir().resolve(file));
            }
            if (Files.isDirectory(currentFile)){
                FileUtilsHelper.copyDirRecursively(currentFile, repo.getRepoDir().resolve(file));
            }
        }

        IFolderComparer finalComparer = repo.getChanges();
        Assert.assertTrue(beforeComparer.getModified().equals(finalComparer.getModified()));
        //Assert.assertTrue(beforeComparer.getNewFiles().equals(finalComparer.getNewFiles()));
        Assert.assertEquals(beforeComparer.getRemoved(),finalComparer.getNewFiles());

        repo.commitStagedChanges("some msg", "test user");
        IFolderComparer emptyComparer = repo.getChanges();
        Assert.assertTrue(!emptyComparer.hasChanges());

    }
}
