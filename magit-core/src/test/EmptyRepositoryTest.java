import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.objects.Repository;
import com.magit.core.serializers.RepoXmlLoader;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class EmptyRepositoryTest {
    private RepoLoaderTest tester;
    @Before
    public void before(){
        tester = new RepoLoaderTest();
    }
    @Test
    public void emptySanityRepoXmlLoad() throws Exception {
        RepoXmlLoader loader = new RepoXmlLoader();
        loader.loadRepoFromFile(tester.loadFile("/empty/empty-repository.xml"));
        Repository repo = loader.dumpRepo();
        Assert.assertTrue(repo.isCleanState());
        Assert.assertTrue(!repo.isInitialized());
        Assert.assertEquals(1, FileUtilsHelper.listFilesRelative(FileSystemFactory.getFs().getPath("C:\\repo2")).size());
        Path newFile = FileSystemFactory.getFs().getPath("C:\\repo2\\fol1\\file.txt");
        Files.createDirectories(newFile.getParent());
        Files.write(newFile, "new content".getBytes(StandardCharsets.UTF_8));
        Assert.assertTrue(!repo.isCleanState());
        Assert.assertTrue(!repo.isInitialized());
        repo.commitStagedChanges("new commit", "user");
        Assert.assertTrue(repo.isCleanState());
        Assert.assertTrue(repo.isInitialized());
    }
}
