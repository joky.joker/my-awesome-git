import com.magit.core.factories.StagingCommitFactory;
import com.magit.core.factories.StringCompressorFactory;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.objects.Repository;
import com.magit.core.serializers.RepoXmlLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.security.InvalidKeyException;

public class RepoLoaderTest {

    public static String LARGE = "large/ex1-large.xml";
    public static String MEDIUM = "medium/ex1-medium.xml";
    public static String SMALL = "small/ex1-small.xml";
    public static String MERGE = "merge/merge-repo.xml";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    public Path loadFile(String name){
        URL url = getClass().getResource(name);
        return new File(url.getFile()).toPath();
    }

    @Before
    public void setup(){
    }


    @Test
    public void testErrorRepoLoader2() throws Exception {
        expectedException.expect(InvalidKeyException.class);
        expectedException.expectMessage("id 2 appears twice in the xml");

        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("errors/ex1-error1-3.2.xml"));
    }

    @Test
    public void testErrorRepoLoader3() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("folder id 4 points file id 33 that does not exist");

        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("errors/ex1-error2-3.3.xml"));
    }

    @Test
    public void testErrorRepoLoader6() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("commit 1 contains non root folder id");

        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("errors/ex1-error2-3.6.xml"));
    }

    @Test
    public void testErrorRepoLoader7() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("commit 1 contains non root folder id");

        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("errors/ex1-error3-3.7.xml"));
    }

    @Test
    public void testErrorRepoLoader9() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("head 'Faruk' pointing to non existent branch name");

        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("errors/ex1-error4-3.9.xml"));
    }

    @Test
    public void testErrorRepoLoader4b() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("tracking branch has invalid remote branch");

        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("remote/error/ex2-medium-error1-4b.xml"));
    }

    public void compareBranch(Repository repo, String branchName, Path compareFolder) throws Exception{
        repo.checkout(branchName);
        IFolderComparer folderComparer = StagingCommitFactory.createFolderComparer();
        folderComparer.compare(repo.getRepoDir(), compareFolder);
        Assert.assertFalse(folderComparer.hasChanges());
    }

    @Test
    public void testInvalidRepoSmallSanity() throws Exception{
        expectedException.expect(AssertionError.class);
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("small/ex1-small.xml"));
        Repository repo = repoXmlLoader.dumpRepo();

        Path path = MagitMergerTest.resourceToPath("small/ex1-smal-master-branch");
        compareBranch(repo, "test", path);
    }

    @Test
    public void testValidRepoSmall() throws Exception{
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("small/ex1-small.xml"));
        Repository repo = repoXmlLoader.dumpRepo();

        Path path = MagitMergerTest.resourceToPath("small/ex1-smal-master-branch");
        compareBranch(repo, "master", path);

        path = MagitMergerTest.resourceToPath("small/ex1-smal-test-branch");
        compareBranch(repo, "test", path);
    }


    @Test
    public void testValidRepoMedium() throws Exception{
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("medium/ex1-medium.xml"));
        Repository repo = repoXmlLoader.dumpRepo();

        Path path = MagitMergerTest.resourceToPath("medium/ex1-medium-deep-branch");
        compareBranch(repo, "deep", path);

        path = MagitMergerTest.resourceToPath("medium/ex1-medium-master-branch");
        compareBranch(repo, "master", path);
    }

    @Test
    public void testValidRepoLargeSanity() throws Exception {
        expectedException.expect(AssertionError.class);
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("large/ex1-large.xml"));
        Repository repo = repoXmlLoader.dumpRepo();

        Path path = MagitMergerTest.resourceToPath("large/large-deep-branch");
        compareBranch(repo, "master", path);
    }
    @Test
    public void testValidRepoLarge() throws Exception{
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile("large/ex1-large.xml"));
        Repository repo = repoXmlLoader.dumpRepo();

        Path path = MagitMergerTest.resourceToPath("large/large-deep-branch");
        compareBranch(repo, "deep", path);

        path = MagitMergerTest.resourceToPath("large/large-test-branch");
        compareBranch(repo, "test", path);

        path = MagitMergerTest.resourceToPath("large/large-master-branch");
        compareBranch(repo, "master", path);
    }

    @Test
    public void testValidRepoMerge() throws Exception{
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(loadFile(MERGE));
        Repository repo = repoXmlLoader.dumpRepo();

        Path path = MagitMergerTest.resourceToPath("merge/base");
        compareBranch(repo, "base", path);

        path = MagitMergerTest.resourceToPath("merge/containment/concealed");
        compareBranch(repo, "concealed", path);

        path = MagitMergerTest.resourceToPath("merge/containment/contained");
        compareBranch(repo, "contained", path);

        path = MagitMergerTest.resourceToPath("merge/cross-updates/cross-updates");
        compareBranch(repo, "cross-updates", path);

        path = MagitMergerTest.resourceToPath("merge/deleted-file/deleted-file");
        compareBranch(repo, "deleted-file", path);

        path = MagitMergerTest.resourceToPath("merge/deleted-with-conflict/delete-with-conflict");
        compareBranch(repo, "delete-with-conflict", path);

        path = MagitMergerTest.resourceToPath("merge/master");
        compareBranch(repo, "master", path);

        path = MagitMergerTest.resourceToPath("merge/new-file/new-file-created");
        compareBranch(repo, "new-file-created", path);

        path = MagitMergerTest.resourceToPath("merge/updated-with-conflict/updated-with-conflict");
        compareBranch(repo, "updated-with-conflict", path);
    }
}
