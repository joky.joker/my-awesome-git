import com.magit.common.utils.FileSystemFactory;
import com.magit.core.serializers.RepoXmlLoader;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class VirtualFileSystemTest {

    RepoLoaderTest test;
    @Before
    public void setup(){
        test = new RepoLoaderTest();
    }

    @Test
    public void testVirtualFS() throws Exception {
        FileSystemFactory.setCurrentType(FileSystemFactory.Type.VIRTUAL);
        RepoXmlLoader loader = new RepoXmlLoader();
        File repo1 = new File("C:\\repo1");
        FileUtils.deleteDirectory(repo1);
        Assert.assertFalse(repo1.exists());
        loader.loadRepoFromFile(test.loadFile(RepoLoaderTest.LARGE));
        loader.dumpRepo();
        Assert.assertFalse(repo1.exists());
    }

    @Test
    public void testVirtualFSSanity() throws Exception {
        FileSystemFactory.setCurrentType(FileSystemFactory.Type.REAL);
        RepoXmlLoader loader = new RepoXmlLoader();
        File repo1 = new File("C:\\repo1");
        FileUtils.deleteDirectory(repo1);
        Assert.assertFalse(repo1.exists());
        loader.loadRepoFromFile(test.loadFile(RepoLoaderTest.LARGE));
        loader.dumpRepo();
        Assert.assertTrue(repo1.exists());
    }

}
