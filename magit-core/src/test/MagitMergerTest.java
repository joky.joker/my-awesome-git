import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.factories.StagingCommitFactory;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.interfaces.IMerger;
import com.magit.core.merge.ConflictDetails;
import com.magit.core.merge.MergeExecutor;
import com.magit.core.objects.Commit;
import com.magit.core.objects.Repository;
import com.magit.core.serializers.RepoXmlLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MagitMergerTest {

    private String largeXmlPath  = "/large/ex1-large.xml";
    private String largeXmlMergeResult  = "/large/large-deep-test-merge";
    private Repository repo;

    @Before
    public void setUp() throws Exception {
        RepoXmlLoader repoXmlLoader = new RepoXmlLoader();
        repoXmlLoader.loadRepoFromFile(resourceToPath(largeXmlPath));
        this.repo = repoXmlLoader.dumpRepo();
        this.repo.checkout(this.repo.getActiveBranch());
        Assert.assertTrue(!this.repo.getChanges().hasChanges());
    }

    public static Path resourceToPath(String resource) throws Exception {
        URL url = MagitMergerTest.class.getResource(resource);
        if (url == null){
            throw new Exception("File not found exception: " + resource);
        }
        Path reourcePath = new File(url.toURI()).toPath();
        Path temp = FileSystemFactory.createTempDirectory("magit");
        return FileUtilsHelper.copyDirRecursively(reourcePath, temp);
    }

    @Test
    public void mergeSanity() throws Exception {
        Commit headCommit = repo.getCurrentBranch().getPointerCommit();
        Commit otherCommit = repo.getFileSystem().readLocalBranchObject("test").getPointerCommit();
        Commit baseCommit = repo.getFileSystem().readCommitObject("d0534daa27ae1bc8d814c805cd654bd7661f9174");
        IMerger merger = StagingCommitFactory.createMerger();
        merger.fromCommits(repo, headCommit, otherCommit, baseCommit);

        MergeExecutor executor = merger.merge();
        IFolderComparer comparer = StagingCommitFactory.createFolderComparer();
        comparer.compare(repo.getRepoDir(), resourceToPath(largeXmlMergeResult));
        Assert.assertTrue(!comparer.hasChanges());
        Assert.assertEquals(1, executor.updateConflictList().size());
        Assert.assertEquals(executor.updateConflictList().get(0).getFinalVersionFile().toString(), "fol1\\Foo.java");
    }

    public void mergeTest(String head, String otherBranch, String resourceFolder) throws Exception {
        mergeTest(head, otherBranch, resourceFolder, false);
    }
    public void mergeTest(String head, String otherBranch, String resourceFolder, boolean conflicts) throws Exception{
        RepoLoaderTest tester = new RepoLoaderTest();
        RepoXmlLoader loader = new RepoXmlLoader();
        loader.loadRepoFromFile(tester.loadFile(RepoLoaderTest.MERGE));
        Repository repo = loader.dumpRepo();

        IMerger merger = StagingCommitFactory.createMerger();
        repo.setActiveBranch(head);
        repo.checkout(head);
        Commit headCommit = repo.getCurrentBranch().getPointerCommit();
        Commit otherCommit = repo.getFileSystem().readBranchObject(otherBranch).getPointerCommit();
        Commit baseCommit = repo.getCommonAncestor(headCommit, otherCommit);
        merger.fromCommits(repo, headCommit, otherCommit, baseCommit);

        MergeExecutor executor = merger.merge();
        if (conflicts == false){
            Assert.assertEquals(new ArrayList<>(), executor.updateConflictList());
        } else {
            for (ConflictDetails conflictDetail: executor.updateConflictList()){
                conflictDetail.setFinalVersion(conflictDetail.getHeadVersion());
                conflictDetail.markResolved(repo.getRepoDir());
            }
        }
        executor.execute("merge", "user");
        System.out.println("repo path: " + repo.getRepoDir().toAbsolutePath().toString());
        Path path = resourceToPath(resourceFolder);
        IFolderComparer comparer = StagingCommitFactory.createFolderComparer();
        comparer.compare(path, repo.getRepoDir());
        Assert.assertEquals(new HashSet<>(), comparer.getNewFiles());
        Assert.assertEquals(new HashSet<>(), comparer.getModified());
        Assert.assertEquals(new HashSet<>(), comparer.getRemoved());
    }
    @Test
    public void mergerCrossUpdatesWithMasterMerge() throws Exception {
        mergeTest("cross-updates",
                "master",
                "merge/cross-updates/cross-updates_with_master");
    }

    @Test
    public void mergerDeletedFileWithMasterMerge() throws Exception {
        mergeTest("deleted-file",
                "master",
                "merge/deleted-file/deleted-file_with_master");
    }

    @Test
    public void mergerNewFileCreatedWithMasterMerge() throws Exception {
        mergeTest("new-file-created",
                "master",
                "merge/new-file/new-file-created_with_master");
    }
    @Test
    public void deletedFileWithMaster() throws Exception {
        mergeTest("delete-with-conflict",
                "master",
                "merge/deleted-with-conflict/delete-with-conflict_with_master_conflicts_by_ours",
                true);
    }

    @Test
    public void masterwithDeletedFile() throws Exception {
        mergeTest("master",
                "delete-with-conflict",
                "merge/deleted-with-conflict/master_with_delete-with-conflict_conflicts-by-ours",
                true);
    }

    @Test
    public void updatedWithMaster() throws Exception {
        mergeTest("updated-with-conflict",
                "master",
                "merge/updated-with-conflict/updated-with-conflict_with_master_conlicts-by-ours",
                true);
    }

    @Test
    public void masterWithUpdated() throws Exception {
        mergeTest("master",
                "updated-with-conflict",
                "merge/updated-with-conflict/master_with_updated-with-conflict_conlicts_by_ours",
                true);
    }
}