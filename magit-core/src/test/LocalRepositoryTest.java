import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.factories.StringCompressorFactory;
import com.magit.core.objects.Branch;
import com.magit.core.objects.LocalRepository;
import com.magit.core.objects.Repository;
import com.magit.core.serializers.RepoXmlLoader;
import com.magit.core.utils.MagitFS;
import javafx.util.Pair;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class LocalRepositoryTest {
    RepoLoaderTest loader;
    final String clonedRepoName = "HAHA";
    final String clonedRepoPath = "C:\\repo_local";

    @Before
    public void setUp() throws Exception {
        loader = new RepoLoaderTest();
        StringCompressorFactory.setType(StringCompressorFactory.CompressionType.DUMMY);
    }

    public Pair<LocalRepository, Repository> createRepoAndClone(String xmlPath, String repoName, String newRepoLocation) throws Exception{
        FileUtilsHelper.deleteFileRec(FileSystemFactory.toPath(newRepoLocation));

        RepoXmlLoader xmlLoader = new RepoXmlLoader();
        xmlLoader.loadRepoFromFile(loader.loadFile(xmlPath));
        Repository repo = xmlLoader.dumpRepo();
        LocalRepository local = LocalRepository.clone(repoName, FileSystemFactory.toPath(newRepoLocation), repo.getRepoDir());
        LocalRepository other = new LocalRepository(local);
        return new Pair<>(other, repo);
    }

    public void cloneTestSanity(String xmlPath) throws Exception{

        Pair<LocalRepository, Repository> cloned = createRepoAndClone(xmlPath, clonedRepoName, clonedRepoPath);
        LocalRepository localRepo = cloned.getKey();
        Repository remoteRepo = cloned.getValue();

        Assert.assertEquals(clonedRepoName, localRepo.getName());
        Assert.assertEquals(remoteRepo.getName(), localRepo.getRemoteRepoName());
        Assert.assertEquals(remoteRepo.getRepoDir(), localRepo.getRemoteRepoLocation());
        Set<String> remoteBranchesNames = remoteRepo.getFileSystem().listLocalBranches()
                .stream()
                .map(branch -> remoteRepo.getName() + "\\" + branch.getName())
                .collect(Collectors.toSet());

        Assert.assertEquals(remoteBranchesNames, localRepo.getFileSystem()
                .listRemoteBranches()
                .stream()
                .map(branch -> branch.getFullName()).collect(Collectors.toSet()));

        Assert.assertEquals(remoteRepo.getCurrentBranch().getName(), localRepo.getCurrentBranch().getName());
    }

    @Test
    public void cloneLargeTestSanity() throws Exception {
        cloneTestSanity(RepoLoaderTest.LARGE);
    }

    @Test
    public void cloneMediumTestSanity() throws Exception {
        cloneTestSanity(RepoLoaderTest.MEDIUM);
    }

    @Test
    public void cloneSmallTestSanity() throws Exception {
        cloneTestSanity(RepoLoaderTest.SMALL);
    }


    public void fetchTestSanity(String xml) throws Exception{
        Pair<LocalRepository, Repository> cloned = createRepoAndClone(xml, clonedRepoName, clonedRepoPath);
        LocalRepository localRepo = cloned.getKey();
        Repository remoteRepo = cloned.getValue();
        Path outFile = remoteRepo.getRepoDir().resolve("peaceAndLovee123.txt");
        Files.write(outFile, "NEW DATA!@!#$".getBytes(StandardCharsets.UTF_8));

        Assert.assertFalse(remoteRepo.isCleanState());
        remoteRepo.commitStagedChanges("new message!", "Some user");
        Branch head = remoteRepo.getCurrentBranch();
        String commitPointed = head.getCommitPointed();

        localRepo.fetch();
        Set<Branch> remoteBranches = localRepo.getFileSystem().listRemoteBranches();
        Optional<Branch> remoteHead = remoteBranches.stream().filter(branch -> branch.getName().equals(head.getName())).findAny();
        Assert.assertTrue(remoteHead.get().getCommitPointed().equals(commitPointed));
    }

    @Test
    public void fetchTestLargeSanity() throws Exception {
        fetchTestSanity(RepoLoaderTest.LARGE);
    }
    @Test
    public void fetchTestMediumSanity() throws Exception {
        fetchTestSanity(RepoLoaderTest.MEDIUM);
    }
    @Test
    public void fetchTestSmallSanity() throws Exception {
        fetchTestSanity(RepoLoaderTest.SMALL);
    }

    @Test
    public void pullTestLargeSanity() throws Exception {
        //deep is the head
        Pair<LocalRepository, Repository> repos = createRepoAndClone(RepoLoaderTest.LARGE, clonedRepoName, clonedRepoPath);
        LocalRepository localRepo = repos.getKey();
        Repository remoteRepo = repos.getValue();
        MagitFS localFs = localRepo.getFileSystem();
        MagitFS remoteFs = remoteRepo.getFileSystem();
        //we will make RTB for test
        remoteRepo.setActiveBranch("test");
        Branch testRTB = new Branch("test", remoteRepo.getCurrentBranch().getCommitPointed());
        testRTB.setTrackingAfter("rep 1\\test");
        testRTB.setFs(localRepo.getFileSystem());
        testRTB.save();


        String testPreCommitSha1 = remoteRepo.getCurrentBranch().getCommitPointed();
        //make commit to test
        Path outFile = remoteRepo.getRepoDir().resolve("peaceAndLovee123.txt");
        Files.write(outFile, "NEW DATA!@!#$".getBytes(StandardCharsets.UTF_8));
        Assert.assertFalse(remoteRepo.isCleanState());
        remoteRepo.commitStagedChanges("new message!", "Some user");

        String testPreCommitSha1Local = localFs.readRemoteBranchObject("test").getCommitPointed();
        Assert.assertEquals(testPreCommitSha1, testPreCommitSha1Local);

        String deepOriginalSha1 = localRepo.getCurrentBranch().getCommitPointed();
        localRepo.pull();
        //head in local is deep, we make sure it hasn't changed after pull
        Assert.assertEquals(localRepo.getCurrentBranch().getCommitPointed(), deepOriginalSha1);
        //make sure test hasnt changed either
        localRepo.setActiveBranch("test");
        localRepo.checkout();

        Assert.assertEquals(testPreCommitSha1, localRepo.getCurrentBranch().getCommitPointed());
        //pull again with test as head and make sure its changing
        localRepo.pull();
        Assert.assertEquals(localRepo.getCurrentBranch().getCommitPointed(), remoteRepo.getCurrentBranch().getCommitPointed());
    }
}