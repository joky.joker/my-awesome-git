package com.magit.core.serializers;

import com.magit.common.utils.DefaultDateFormatterFactory;
import com.magit.core.interfaces.IMagiObjectSerializer;
import com.magit.core.interfaces.IMagiXmlObjectLoader;
import com.magit.core.objects.*;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class IMagiXmlSerializer implements IMagiXmlObjectLoader, IMagiObjectSerializer<String> {
    private static final Logger log = Logger.getLogger(IMagiXmlSerializer.class.getSimpleName());
    private static final XMLOutputter xmlOutputter;

    static {
        xmlOutputter = new XMLOutputter();
        xmlOutputter.setFormat(Format.getPrettyFormat());
    }

    public String serializeBlob(Blob blob) {
        Element MagiBlob = new Element("MagitBlob");
        MagiBlob.addContent(new Element("content").setText(blob.getContent()));
        return xmlOutputter.outputString(MagiBlob);
    }

    @Override
    public MagiFileInfo deserializeBlob(String input) throws Exception {
        SAXBuilder sax = new SAXBuilder();
        Document doc = sax.build(new StringReader(input));
        return deserializeBlob(doc.getRootElement());
    }

    public MagiFileInfo deserializeBlob(Element blobElement) throws Exception{
        if (!blobElement.getName().equals("MagitBlob")){
            throw new AssertionError("element is not Blob");
        }
        String content = blobElement.getChildText("content");
        Blob blob = new Blob(content);
        MagiFileInfo fileInfo = this.createMagiFile(blobElement, MagiObjectType.BLOB);
        fileInfo.setMagiObj(blob);
        return fileInfo;
    }

    private MagiFileInfo createMagiFile(Element magiElem, MagiObjectType type){
        String name = magiElem.getChildText("name");
        String lastUpdater = magiElem.getChildText("last-updater");
        String lastUpdate = magiElem.getChildText("last-update-date");
        Date lastUpdateDate = null;
        try{
            if (lastUpdate != null){
                lastUpdateDate = DefaultDateFormatterFactory.createDateFormatter().parse(lastUpdate);
            }
        } catch (ParseException e){
            log.log(Level.WARNING, "invalid date format :" + lastUpdate, e);
        }
        MagiFileInfo fileInfo = new MagiFileInfo(name, type, lastUpdater, lastUpdateDate);
        fileInfo.setObjIdentifier(magiElem.getChildText("id"));
        return fileInfo;
    }

    public String serializeFolder(Folder folder) {
        Element magiFolder = new Element("MagitSingleFolder");
        Element items = new Element("items");
        for (Map.Entry<String, MagiFileInfo> folderItem : folder.getFiles().entrySet()){
            String id = folderItem.getKey();
            MagiFileInfo fileInfo = folderItem.getValue();
            Element item = new Element("item");
            item.setAttribute("type", fileInfo.getType().toString());
            item.setAttribute("id", fileInfo.getObjIdentifier());
            item.addContent(new Element("last-updater").setText(fileInfo.getLastChanger()));
            String date = DefaultDateFormatterFactory.createDateFormatter().format(fileInfo.getLastChanged());
            item.addContent(new Element("last-update-date").setText(date));
            item.addContent(new Element("name").setText(fileInfo.getName()));
            items.addContent(item);
        }
        magiFolder.addContent(items);
        return xmlOutputter.outputString(magiFolder);
    }

    @Override
    public MagiFileInfo deserializeFolder(String bytes) throws Exception {
        SAXBuilder sax = new SAXBuilder();
        Document doc = sax.build(new StringReader(bytes));
        return deserializeFolder(doc.getRootElement());
    }

    @Override
    public MagiFileInfo deserializeFolder(Element folderElement) throws Exception{
        if (!folderElement.getName().equals("MagitSingleFolder")){
            throw new AssertionError("element in the xml is not Folder");
        }
        List<Element> items = folderElement.getChild("items").getChildren();
        Folder folder = new Folder();
        for (Element item : items){
            String type = item.getAttributeValue("type");
            String itemId = item.getAttributeValue("id");
            MagiFileInfo fileInfo = createMagiFile(item, MagiObjectType.fromString(type));
            fileInfo.setObjIdentifier(itemId);
            folder.addFileOrFolder(itemId, fileInfo);
        }
        MagiFileInfo fileInfo = createMagiFile(folderElement, MagiObjectType.FOLDER);
        fileInfo.setMagiObj(folder);
        return fileInfo;
    }


    public String serializeCommit(Commit commit) {
        Element magitCommit = new Element("MagitSingleCommit");
        magitCommit.addContent(new Element("root-folder").setAttribute("id", commit.getRootFolderIdentifier()));
        magitCommit.addContent(new Element("message").setText(commit.getMessage()));
        magitCommit.addContent(new Element("author").setText(commit.getAuthor()));
        String date = DefaultDateFormatterFactory.createDateFormatter().format(commit.getDateOfCreation());
        magitCommit.addContent(new Element("date-of-creation").setText(date));
        Element precedingCommits = new Element("preceding-commits");
        if (commit.getParent() != null){
            Element precedingCommit = new Element("preceding-commit");
            precedingCommit.setAttribute("id", commit.getParent());
            precedingCommits.addContent(precedingCommit);
        }
        if (commit.getSecondParent() != null){
            Element precedingCommit = new Element("preceding-commit");
            precedingCommit.setAttribute("id", commit.getSecondParent());
            precedingCommits.addContent(precedingCommit);
        }
        magitCommit.addContent(precedingCommits);
        return xmlOutputter.outputString(magitCommit);
    }

    @Override
    public Commit deserializeCommit(String input) throws Exception {
        SAXBuilder sax = new SAXBuilder();
        Document doc = sax.build(new StringReader(input));
        return deserializeCommit(doc.getRootElement());
    }

    @Override
    public Commit deserializeCommit(Element commitElement) throws Exception {
        if (!commitElement.getName().equals("MagitSingleCommit")){
            throw new AssertionError("element in the xml is not a Commmit");
        }
        Commit commit = new Commit();
        commit.setRootFolderIdentifier(commitElement.getChild("root-folder").getAttributeValue("id"));
        commit.setMessage(commitElement.getChildText("message"));
        commit.setAuthor(commitElement.getChildText("author"));
        try{
            commit.setDateOfCreation(DefaultDateFormatterFactory.createDateFormatter().parse(commitElement.getChildText("date-of-creation")));
        } catch (NumberFormatException e){
            e.printStackTrace();
        }
        Element precedingCommitsNode = commitElement.getChild("preceding-commits");
        if (precedingCommitsNode == null){
            return commit;
        }
        List<Element> precedingCommits = precedingCommitsNode.getChildren();
        if (precedingCommits.size() > 2 ){
            throw new Exception("commit has more then 2 preceding commits");
        }
        if (precedingCommits.size() == 2){
            commit.setParent(precedingCommits.remove(0).getAttributeValue("id"));
            commit.setSecondParent(precedingCommits.remove(0).getAttributeValue("id"));
        } else if (precedingCommits.size() == 1){
            commit.setParent(precedingCommits.remove(0).getAttributeValue("id"));
        }
        return commit;
    }

    public String serializeBranch(Branch branch) {
        Element magitBranch = new Element("MagitSingleBranch");
        magitBranch.addContent(new Element("name").setText(branch.getName()));
        String commitPointed = branch.getCommitPointed() == null ? "" : branch.getCommitPointed();
        magitBranch.addContent(new Element("pointed-commit").setAttribute("id", commitPointed));
        if (branch.isRemote()){
            magitBranch.setAttribute("is-remote", "true");
        }
        if (branch.isTracking()){
            magitBranch.setAttribute("tracking", "true");
            magitBranch.addContent(new Element("tracking-after").setText(branch.getTrackingAfter()));
        }
        return xmlOutputter.outputString(magitBranch);
    }

    @Override
    public String serializeCommitList(Set<String> commitList) {
        if (commitList == null){
            return "";
        }
        return commitList.stream().collect(Collectors.joining(","));
    }

    @Override
    public Set<String> commitList(String input) throws Exception {
        if (input == null){
            return new HashSet<>();
        }
        return new HashSet<>(Arrays.asList(input.split(",")));
    }

    @Override
    public Branch deserializeBranch(String xmlData) throws Exception {
        SAXBuilder sax = new SAXBuilder();
        Document doc = sax.build(new StringReader(xmlData));
        return deserializeBranch(doc.getRootElement());
    }

    @Override
    public Branch deserializeBranch(Element branchElement) {
        String name = branchElement.getChildText("name");
        String commit = branchElement.getChild("pointed-commit").getAttributeValue("id");
        if (commit.isEmpty()){
            commit = null;
        }
        Branch branch = new Branch(name, commit);
        branch.setRemote(Boolean.parseBoolean(branchElement.getAttributeValue("is-remote", "false")));
        boolean isTracking = Boolean.parseBoolean(branchElement.getAttributeValue("tracking", "false"));
        if (isTracking){
            branch.setTrackingAfter(branchElement.getChildText("tracking-after"));
        }
        return branch;
    }
}
