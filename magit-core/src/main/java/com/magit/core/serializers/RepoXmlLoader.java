package com.magit.core.serializers;

import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.factories.MagitSerializerFactory;
import com.magit.core.interfaces.IMagiXmlObjectLoader;
import com.magit.core.objects.*;
import com.magit.core.utils.MagitFS;
import org.apache.commons.io.FileUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidKeyException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class RepoXmlLoader {
    private static final Logger log = Logger.getLogger(RepoXmlLoader.class.getSimpleName());

    private Map<String, MagiFileInfo> blobs;
    private Map<String, MagiFileInfo> rootFolders;
    private Map<String, MagiFileInfo> nonRootFolders;



    private Map<String, Branch> trackingBranches;
    private Map<String, Branch> remoteBranches;
    private Map<String, Branch> otherBranches;

    private Map<String, Commit> commits;
    private IMagiXmlObjectLoader serializer;
    private String repoName;
    private Path location;
    private String head;

    private boolean isRemoteConnected = false;
    private String remoteName;
    private String remoteLocation;

    public RepoXmlLoader(){
        serializer = MagitSerializerFactory.createXmlLoader();
    }

    public String getRepoName(){
        return repoName;
    }
    public void handleRemoteRepo(Element elem){
        if (elem == null){
            return;
        }
        remoteName = elem.getChildText("name");
        remoteLocation = elem.getChildText("location");
        isRemoteConnected = remoteName != null && remoteLocation != null;
    }

    public void loadRepoFromString(String content) throws Exception{
        SAXBuilder sax = new SAXBuilder();
        Document doc = null;

        doc = sax.build(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)));

        Element root = doc.getRootElement();
        repoName = root.getAttributeValue("name");
        location = FileSystemFactory.getFs().getPath(root.getChildText("location"));
        parseBlobs(root.getChild("MagitBlobs"));
        parseFolders(root.getChild("MagitFolders"));

        normalizeRootFolders();

        parseCommits(root.getChild("MagitCommits"));
        head = parseBranches(root.getChild("MagitBranches"));
        handleRemoteRepo(root.getChild("MagitRemoteReference"));

        //from this on the data is valid and we should unpack it to disc
    }

    public void loadRepoFromFile(Path xmlFile) throws Exception {
        loadRepoFromString(new String(Files.readAllBytes(xmlFile), StandardCharsets.UTF_8));
    }

    private void normalizeRootFolders(){
        Map<String, MagiFileInfo> oldRootFolders = new HashMap<>(rootFolders);
        for (Map.Entry<String, MagiFileInfo> rootFolderEntry : oldRootFolders.entrySet()){
            MagiFileInfo originalRootFolderInfo = rootFolderEntry.getValue();
            Folder newRootFolder = new Folder();
            //we will just back up the Magifile info of the root folder in a new folder.
            newRootFolder.addFileOrFolder(originalRootFolderInfo.getObjIdentifier(), originalRootFolderInfo);
            MagiFileInfo newRootFolderInfo = new MagiFileInfo(null,MagiObjectType.FOLDER, null, null);
            newRootFolderInfo.setMagiObj(newRootFolder);
            rootFolders.replace(rootFolderEntry.getKey(), newRootFolderInfo);
            nonRootFolders.put(originalRootFolderInfo.getObjIdentifier(), originalRootFolderInfo);
        }
    }
    public Path getLocation(){
        return location;
    }

    private static void writeObjects(MagitFS fs, Collection<? extends MagitObject> infos) throws Exception{
        for (MagitObject object : infos){
            object.setFs(fs);
            object.save();
        }
    }
    public Repository dumpRepo() throws Exception {
        return  dumpRepo(this.location);
    }

    public Repository dumpRepoByName(Path location) throws Exception{
        return dumpRepo(location.resolve(repoName));
    }

    public Repository dumpRepo(Path location) throws Exception {
        FileUtilsHelper.deleteFileRec(location);
        Repository repo = Repository.createRepository(location, repoName);
        MagitFS fileSystem = repo.getFileSystem();
        if (isRemoteConnected){
            LocalRepository localRepository = new LocalRepository(location);
            localRepository.setRemoteRepoName(remoteName);
            localRepository.setRemoteRepoLocation(FileSystemFactory.toPath(remoteLocation));
            repo = localRepository;
        }
        writeObjects(fileSystem, blobs.values());
        writeObjects(fileSystem, nonRootFolders.values());
        writeObjects(fileSystem, rootFolders.values());
        writeObjects(fileSystem, commits.values());
        writeObjects(fileSystem, trackingBranches.values());
        writeObjects(fileSystem, otherBranches.values());
        writeObjects(fileSystem, remoteBranches.values());
        repo.setActiveBranch(head);
        fileSystem.writeHead(head);

        return repo;
    }

    private String parseBranches(Element magitBranches) throws Exception{
        trackingBranches = new HashMap<>();
        otherBranches = new HashMap<>();
        remoteBranches = new HashMap<>();

        String head = magitBranches.getChildText("head");
        boolean wasHeadFound = false;
        for (Element branchElement : magitBranches.getChildren("MagitSingleBranch")){
            Branch branch = serializer.deserializeBranch(branchElement);
            if (!commits.containsKey(branch.getCommitPointed()) && branch.getCommitPointed() != null){
                throw new Exception("branch " + branch.getName() + " points to non existent commit " + branch.getCommitPointed());
            }
            if (branch.getName().equals(head)){
                wasHeadFound = true;
            }
            if (branch.getCommitPointed() != null){
                Commit pointedCommit = commits.get(branch.getCommitPointed());
                branch.setCommitPointed(pointedCommit.getSha1Identifier());
            }
            if (branch.isTracking()){
                trackingBranches.put(branch.getName(), branch);
                continue;
            }
            if (branch.isRemote()){
                String fullname = branch.getName();
                char delimiter = '/';
                if (-1 == fullname.indexOf(delimiter)){
                    delimiter = '\\';
                }
                int index = fullname.indexOf(delimiter);
                branch.setName(fullname.substring(index + 1));
                remoteBranches.put(fullname, branch);
                continue;
            }
            otherBranches.put(branch.getName(), branch);
        }
        if (!wasHeadFound){
            throw new Exception("head '" + head +"' pointing to non existent branch name");
        }

        //validate all the tracking branches are well set.
        for (Branch branch : trackingBranches.values()){
            if (branch.getTrackingAfter() == null){
                throw new Exception("tacking branch has no tracking branch name");
            }
            if (!remoteBranches.containsKey(branch.getTrackingAfter())){
                throw new Exception("tracking branch has invalid remote branch");
            }
        }

        return head;
    }

    private String validateDuplicates(Element rootElement){
        List<String> ids = rootElement.getChildren().stream()
                .map(element -> element.getAttributeValue("id"))
                .collect(Collectors.toList());
        Set<String> ids2 = rootElement.getChildren().stream()
                .map(element -> element.getAttributeValue("id"))
                .collect(Collectors.toSet());
        if (ids.size() == ids2.size()){
            return null;
        }
        for (String id : ids){
            if (Collections.frequency(ids, id) > 1){
                return id;
            }
        }
        return null;
    }

    private Commit getCommitById(Element magitCommits, String commitId) throws Exception {
        if (commits.containsKey(commitId)){
            return commits.get(commitId);
        }

        for (Element commitElement : magitCommits.getChildren()){
            String id = commitElement.getAttributeValue("id");
            if (!commitId.equals(id)){
                continue;
            }
            Commit commit = serializer.deserializeCommit(commitElement);
            if (!rootFolders.containsKey(commit.getRootFolderIdentifier())){
                throw new Exception("commit " + id + " contains non root folder id");
            }
            MagiFileInfo folderInfo = rootFolders.get(commit.getRootFolderIdentifier());
            commit.setRootFolderIdentifier(folderInfo.getObjIdentifier());
            if (commit.getParent() != null){
                commit.setParent(getCommitById(magitCommits, commit.getParent()).getSha1Identifier());
            }
            if (commit.getSecondParent() != null){
                commit.setSecondParent(getCommitById(magitCommits, commit.getParent()).getSha1Identifier());
            }
            commits.put(id, commit);
            return commit;
        }
        throw new Exception("commit id "+ commitId + " referenced but couldnt be found");
    }
    private void parseCommits(Element magitCommits) throws Exception{
        commits = new HashMap<>();
        String duplicateId = validateDuplicates(magitCommits);
        if (duplicateId != null){
            throw new Exception("Commit id " + duplicateId + " appears more then once in the xml");
        }

        for (Element commitElement : magitCommits.getChildren()){
            String id = commitElement.getAttributeValue("id");
            if (commits.containsKey(id)){
                continue;
            }
            Commit commit = getCommitById(magitCommits, id);
            commits.put(id, commit);
        }
    }

    private void parseFolders(Element magitFolders) throws Exception{
        rootFolders = new HashMap<>();
        nonRootFolders = new HashMap<>();
        String duplicateId = validateDuplicates(magitFolders);
        if (duplicateId != null){
            throw new Exception("Folder id " + duplicateId + " appears more then once");
        }
        for (Element folderNode : magitFolders.getChildren()){
            String id = folderNode.getAttributeValue("id");
            parseFolderById(magitFolders, id);
        }
    }

    private MagiFileInfo parseFolderById(Element magiFolders, String id) throws Exception{
        if (rootFolders.containsKey(id)){
            return rootFolders.get(id);
        }
        if (nonRootFolders.containsKey(id)){
            return nonRootFolders.get(id);
        }
        MagiFileInfo fileInfo = null;
        for (Element child : magiFolders.getChildren()){
            if (!child.getAttributeValue("id").equals(id)){
                continue;
            }
            if (fileInfo != null){
                throw new Exception("folder id "+ id + " appreas twice in the xml");
            }
            MagiFileInfo folderInfo = serializer.deserializeFolder(child);
            Folder folder = (Folder)folderInfo.getMagiObj();
            for (String blobId : folder.getBlobIds()){
                if (!blobs.containsKey(blobId)){
                    throw new Exception("folder id " + id +" points file id "+ blobId + " that does not exist");
                }
                folder.replaceContent(blobId, blobs.get(blobId));
            }
            for (String folderIds : folder.getFoldeIds()){
                MagiFileInfo subFolderInfo = parseFolderById(magiFolders, folderIds);
                if (subFolderInfo == null) { //pointing to folder that doesnt exists
                    throw new Exception("folder id " + id + " points to non existent folder id "+ folderIds);
                }
                folder.replaceContent(folderIds, subFolderInfo);
            }
            folderInfo.setMagiObj(folder);
            boolean isRoot = Boolean.parseBoolean(child.getAttributeValue("is-root", "false"));
            if (isRoot){
                rootFolders.put(id, folderInfo);
            } else {
                nonRootFolders.put(id, folderInfo);
            }
            fileInfo = folderInfo;
        }
        return fileInfo;
    }

    private void parseBlobs(Element magitBlobs) throws Exception{
        blobs = new HashMap();
        for (Element blobElement : magitBlobs.getChildren()){
            String id = blobElement.getAttributeValue("id");
            if (blobs.get(id) != null){
                log.severe("invalid repo xml, id " + id + " appears twice in the xml");
                throw new InvalidKeyException("id " + id + " appears twice in the xml");
            }
            MagiFileInfo blob = serializer.deserializeBlob(blobElement);
            blobs.put(id, blob);
        }
    }

}
