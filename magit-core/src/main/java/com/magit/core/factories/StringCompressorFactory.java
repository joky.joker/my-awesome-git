package com.magit.core.factories;

import com.magit.common.utils.IsDebug;
import com.magit.core.DummyIStringCompressorImpl;
import com.magit.core.interfaces.IStringCompressor;
import com.magit.core.utils.IStringCompressorImpl;

public class StringCompressorFactory {
    public enum CompressionType {REAL, DUMMY};
    private static CompressionType type = CompressionType.REAL;

    static {
        if (IsDebug.isDebugEnvironment()){
            type = CompressionType.DUMMY;
        }
    }

    public static CompressionType getType() {
        return type;
    }

    public static void setType(CompressionType type) {
        StringCompressorFactory.type = type;
    }

    public static IStringCompressor createStringCompressor(){
        switch (type){
            case REAL:
                return IStringCompressorImpl.getInstance();
            case DUMMY:
                return new DummyIStringCompressorImpl();
        }
        return null;
    }

}
