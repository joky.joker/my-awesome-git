package com.magit.core.factories;

import com.magit.core.interfaces.IMagiObjectSerializer;
import com.magit.core.interfaces.IMagiXmlObjectLoader;
import com.magit.core.serializers.IMagiXmlSerializer;

public class MagitSerializerFactory {
    private static IMagiObjectSerializer<String> instanceSerializer = null;
    private static IMagiXmlObjectLoader instanceXmlLoader = null;

    public static IMagiObjectSerializer<String> createMagiSerilizer(){
        if (instanceSerializer == null){
            instanceSerializer = new IMagiXmlSerializer();
        }
        return instanceSerializer;
    }

    public static IMagiXmlObjectLoader createXmlLoader(){
        if (instanceXmlLoader == null){
            instanceXmlLoader = new IMagiXmlSerializer();
        }
        return instanceXmlLoader;
    }
}
