package com.magit.core.factories;

import com.magit.core.interfaces.IBaseCommitFinder;
import com.magit.core.interfaces.ICommitter;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.interfaces.IMerger;
import com.magit.core.merge.MagitMerger;
import com.magit.core.staging.CommitStagerImpl;

import com.magit.core.staging.SimpleFolderComparer;

public class StagingCommitFactory {
    public static IFolderComparer createFolderComparer(){
        return new SimpleFolderComparer();
    }
    public static ICommitter createStagingCommitter() {return new CommitStagerImpl();}
    public static IMerger createMerger() {return new MagitMerger();}
}
