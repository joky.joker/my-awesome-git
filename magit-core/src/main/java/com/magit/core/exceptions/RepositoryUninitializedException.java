package com.magit.core.exceptions;

public class RepositoryUninitializedException extends RepositoryException
{
    public RepositoryUninitializedException() {}
    public RepositoryUninitializedException(String message) {super(message);}
}
