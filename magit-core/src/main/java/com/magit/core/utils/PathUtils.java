package com.magit.core.utils;

import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import org.apache.commons.io.FileUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashSet;

public class PathUtils {

    public static void cleanFolder(Path checkout, Collection<Path> ignoreList) throws IOException {
        for (Path file: FileUtilsHelper.listFilesRelative(checkout)){
            if (ignoreList.contains(file)){
                continue;
            }
            FileUtilsHelper.deleteFileRec(checkout.resolve(file));
        }
    }
    public static void cleanFolder(Path checkout, String ignoreFile) throws IOException{
        HashSet<Path> ignored = new HashSet<>();
        ignored.add(FileSystemFactory.getFs().getPath(ignoreFile));
        cleanFolder(checkout, ignored);
    }
}
