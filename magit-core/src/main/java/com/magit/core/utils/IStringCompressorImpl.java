package com.magit.core.utils;

import com.magit.core.interfaces.IStringCompressor;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class IStringCompressorImpl implements IStringCompressor {
    private static IStringCompressorImpl instance;

    private IStringCompressorImpl() {}

    public static IStringCompressor getInstance() {
        if (instance == null) {
            instance = new IStringCompressorImpl();
        }
        return instance;
    }

    public byte[] compress(String text) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (OutputStream out = new DeflaterOutputStream(baos)) {
            out.write(text.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        return baos.toByteArray();
    }


    public String decompress(byte[] bytes){
        InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[8192];
            int len;
            while((len = in.read(buffer))>0)
                baos.write(buffer, 0, len);
            return new String(baos.toByteArray(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            //should never happen
            throw new AssertionError("Corrupted data, cant decompress files");
        }
    }
}
