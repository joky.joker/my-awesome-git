package com.magit.core.utils;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.magit.common.utils.FileSystemFactory;
import com.magit.core.factories.MagitSerializerFactory;
import com.magit.core.factories.StringCompressorFactory;
import com.magit.core.interfaces.IMagiObjectSerializer;
import com.magit.core.interfaces.IStringCompressor;
import com.magit.core.objects.*;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class MagitFS {
    private static Logger log = Logger.getLogger(MagitFS.class.getTypeName());
    private Repository repo;
    private Path objectsDir;
    private Path branchesDir;
    private IMagiObjectSerializer<String> serializer;
    private IStringCompressor compressor;
    private XMLOutputter xmlOutputter;
    private Path repoGitDir;

    public static final String COMMIT_LIST_FILE = "cmt.lst";
    public static final String REMOTE_REPO_NAME_FILE = "rem_name";
    public static final String REMOTE_REPO_LOCATION_FILE = "rem_loc";
    public static final String PRS_FILE = "prs";

    public MagitFS(Repository repo){
        objectsDir = repo.getObjectDir();
        branchesDir = repo.getBranchDir();
        repoGitDir = repo.getRepoGitDir();
        serializer = MagitSerializerFactory.createMagiSerilizer();
        compressor = StringCompressorFactory.createStringCompressor();
        xmlOutputter = new XMLOutputter();
        xmlOutputter.setFormat(Format.getPrettyFormat());
        this.repo = repo;
    }

    public Path getObjectsDir(){return objectsDir;}
    public Path getBranchesDir(){return branchesDir;}
    public Repository getRepository() {return this.repo;}

    public Commit readCommitBySha1Silent(String sha1){
        try{
            return readCommitObject(sha1);
        } catch (Exception e){
            log.log(Level.SEVERE, "fuck!", e);
            return null;
        }
    }

    public void writeHead(String identifier) throws IOException{
        writeCompressedString(branchesDir, "HEAD", identifier);
    }

    public boolean isObjectExist(String identifier){
        return Files.exists(objectsDir.resolve(identifier));
    }

    public void writeRepoName(String name) throws IOException {
        writeCompressedString(repoGitDir, "name", name);
    }

    public String readRepoName(){
        try{
            return readCompressedString(repoGitDir, "name");
        } catch (IOException e){
            return null;
        }
    }

    public boolean isBranchExist(String identifier){
        return Files.exists(branchesDir.resolve(identifier));
    }

    public void removeLocalBranch(String branchName, boolean force) throws Exception {
        if (!force && branchName.equals(readHead())){
            throw new Exception("cannot remove head branch");
        }
        Files.deleteIfExists(branchesDir.resolve(branchName));
    }
    public void removeLocalBranch(String branchName) throws Exception{
        removeLocalBranch(branchName, false);
    }

    public void removeRemoteBranch(String branchName) throws Exception {
        Files.deleteIfExists(getRemoteBranchDir().resolve(branchName));
    }

    public List<Branch> listAllBranches() throws Exception {
        List<Branch> localBranches = new ArrayList<>(listLocalBranches());
        localBranches.addAll(listRemoteBranches());
        return localBranches;
    }
    public Set<Branch> listLocalBranches(){
        HashSet<Branch> branches = new HashSet<>();
        try(Stream<Path> files = Files.list(branchesDir)){
            files.forEachOrdered(file -> {
                if (file.getFileName().toString().equals("HEAD") || !Files.isRegularFile(file)){
                    return;
                }
                try{
                    branches.add(readLocalBranchObject(file.getFileName().toString()));
                } catch (Exception e){
                    throw new RuntimeException(e);
                }
            });
        } catch (IOException e) {
            log.log(Level.SEVERE, "failed to read branch dir list", e);
        }
        return branches;
    }

    public boolean isInitizlied() throws IOException {
        try{
            Branch branch = readLocalBranchObject(readHead());
            return branch.getCommitPointed() != null;
        } catch (Exception e){
            throw new IOException("failed to find head file");
        }
    }
    public String readHead() throws IOException{
        return readCompressedString(branchesDir, "HEAD");
    }

    private String readCompressedString(Path dir, String sha1)throws  IOException{
        byte[] data = Files.readAllBytes(dir.resolve(sha1));
        return compressor.decompress(data);
    }

    private void writeCompressedString(Path dir, String sha1, String data) throws IOException {
        byte[] compressedData = compressor.compress(data);
        Path outFile = dir.resolve(sha1);
        Files.createDirectories(outFile.getParent());
        Files.write(dir.resolve(sha1), compressedData);
    }

    public Blob readBlobObject(String sha1) throws Exception {
        return (Blob) serializer.deserializeBlob(readCompressedString(objectsDir, sha1)).getMagiObj().setFs(this);
    }
    public Folder readFolderObject(String sha1) throws Exception {
        Folder folder = (Folder) serializer.deserializeFolder(readCompressedString(objectsDir, sha1)).getMagiObj().setFs(this);
        return folder;
    }

    public Commit readCommitObject(String sha1) throws Exception {
        return (Commit) serializer.deserializeCommit(readCompressedString(objectsDir, sha1)).setFs(this);
    }

    private Branch readLocalBranchObject(String branchName, Path dir) throws Exception {
        String branchData = readCompressedString(dir, branchName);
        Branch branch = serializer.deserializeBranch(branchData);
        branch.setName(branchName);
        branch.setFs(this);
        return branch;
    }
    public Branch readLocalBranchObject(String branchName) throws Exception {
        return readLocalBranchObject(branchName, branchesDir);
    }

    public Branch readBranchObject(String branchName) throws Exception {
        if (branchName.contains("\\") || branchName.contains("/")){
            return readRemoteBranchObject(FileSystemFactory.toPath(branchName).getFileName().toString());
        }
        return readLocalBranchObject(branchName);
    }
    public Branch readRemoteBranchObject(String branchName) throws Exception{
        Path remoteDir = getRemoteBranchDir();
        return readLocalBranchObject(branchName, remoteDir);
    }

    public void writeObject(MagitObject obj) throws Exception{
        if (obj instanceof Blob){
            writeObject((Blob)obj);
        } else if (obj instanceof Folder){
            writeObject((Folder) obj);
        }else if (obj instanceof Commit){
            writeObject((Commit) obj);
        }else if (obj instanceof Branch){
            writeObject((Branch) obj);
        } else {
            throw new Exception("failed to write object type " + obj.getClass().getSimpleName());
        }
    }
    private Path getRemoteBranchDir() throws IOException {
        return branchesDir.resolve(readRemoteRepoName());
    }

    public void writeObject(Blob obj) throws IOException{
        String data = serializer.serializeBlob(obj);
        writeCompressedString(objectsDir, obj.getSha1Identifier(), data);
    }
    public void writeObject(Folder obj) throws IOException{
        String data = serializer.serializeFolder(obj);
        writeCompressedString(objectsDir, obj.getSha1Identifier(), data);
    }
    private void addToCommitList(String sha1){
        Set<String> commitList = readCommitList();
        commitList.add(sha1);
        writeCommitList(commitList);
    }

    public Set<String> readCommitList(){
        Set<String> commitList = new HashSet<>();
        try{
            commitList = serializer.commitList(readCompressedString(repoGitDir, COMMIT_LIST_FILE));
        } catch (Exception e){}
        return commitList;
    }

    public void writeCommitList(Set<String> commitList){
        try{
            writeCompressedString(repoGitDir, COMMIT_LIST_FILE, serializer.serializeCommitList(commitList));
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void writeObject(Commit obj) throws IOException{
        String data = serializer.serializeCommit(obj);
        addToCommitList(obj.getSha1Identifier());
        writeCompressedString(objectsDir, obj.getSha1Identifier(), data);
    }
    public void writeObject(Branch obj) throws IOException{
        String data = serializer.serializeBranch(obj);
        writeCompressedString(branchesDir, obj.getFullName(), data);
    }

    public boolean isRemote(){
        return Files.isRegularFile(repoGitDir.resolve(REMOTE_REPO_LOCATION_FILE)) &&
                Files.isRegularFile(repoGitDir.resolve(REMOTE_REPO_NAME_FILE));
    }

    public Set<Branch> listRemoteBranches() throws IOException {
        if (!isRemote()){
            return new HashSet<>();
        }
        HashSet<Branch> branches = new HashSet<>();
        if (!Files.isDirectory(getRemoteBranchDir())){
            return branches;
        }
        try(Stream<Path> files = Files.list(getRemoteBranchDir())){
            files.forEachOrdered(file -> {
                try{
                    branches.add(readRemoteBranchObject(file.getFileName().toString()));
                } catch (Exception e){
                    log.log(Level.SEVERE, "should never happen", e);
                }
            });
        }
        return branches;
    }

    public void writeRemoteRepoPath(Path remoteRepoLocation) throws IOException {
        writeCompressedString(repoGitDir, REMOTE_REPO_NAME_FILE, remoteRepoLocation.toString());
    }

    public Path readRemoteRepoPath() throws IOException{
        return FileSystemFactory.toPath(readCompressedString(repoGitDir, REMOTE_REPO_NAME_FILE));
    }

    public void writeRemoteRepoName(String remoteRepoName)  throws IOException {
        writeCompressedString(repoGitDir, REMOTE_REPO_LOCATION_FILE, remoteRepoName);
    }

    public String readRemoteRepoName() throws IOException {
        return readCompressedString(repoGitDir, REMOTE_REPO_LOCATION_FILE);
    }

    public List<PullRequest> readPullRequests() throws IOException {
        Gson gson = getGson();
        if (!Files.isRegularFile(repoGitDir.resolve(PRS_FILE))){
            return new ArrayList<>();
        }
        Type listType = new TypeToken<List<PullRequest>>(){}.getType();
        return gson.fromJson(readCompressedString(repoGitDir, PRS_FILE), listType);
    }

    public void addNewPullRequest(PullRequest pullRequest) throws IOException {
        List<PullRequest> pullRequests = readPullRequests();
        pullRequest.setId(pullRequests.size());
        pullRequests.add(pullRequest);
        writePullRequests(pullRequests);
    }

    public void updatePullRequest(PullRequest pullRequest) throws IOException {
        List<PullRequest> pullRequests = readPullRequests();
        pullRequests.set(pullRequest.getId(), pullRequest);
        writePullRequests(pullRequests);
    }

    public PullRequest getPullRequest(int id) throws IOException {
        return readPullRequests().get(id);
    }

    public void writePullRequests(List<PullRequest> pullRequests) throws IOException{
        Gson gson = getGson();
        Type listType = new TypeToken<List<PullRequest>>(){}.getType();
        String data = gson.toJson(pullRequests, listType);
        writeCompressedString(repoGitDir, PRS_FILE, data);
    }
    private Gson getGson(){
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (json, typeOfT, context) -> new Date(json.getAsJsonPrimitive().getAsLong()))
                .registerTypeAdapter(Date.class, (JsonSerializer<Date>) (date, type, jsonSerializationContext) -> new JsonPrimitive(date.getTime()))
                .create();
    }

}
