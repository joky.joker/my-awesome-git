package com.magit.core.merge;

import com.magit.common.utils.FileUtilsHelper;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileState {
    private String content;
    private State state;
    public enum State {EXISTING, NON_EXISTING};

    public State getState() {
        return state;
    }

    public String getContent() {
        return content;
    }

    public void setState(State state) {
        this.state = state;

    }

    public FileState(State state, String content){
        this.state = state;
        this.content = content;
    }

    public FileState(State state){
        this.state = state;
    }

    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof FileState)){
            return false;
        }
        FileState state = (FileState) obj;
        if (!state.state.equals(this.state)){
            return false;
        }
        if (state.state.equals(State.NON_EXISTING)){
            return true;
        }

        return state.content.equals(this.content);
    }

    public void apply(Path fullPath) throws IOException {
        if (this.state.equals(FileState.State.EXISTING)){
            Files.createDirectories(fullPath.getParent());
            Files.write(fullPath, this.content.getBytes(StandardCharsets.UTF_8));
            return;
        }
        Files.deleteIfExists(fullPath);

        //delete empty dirs along the tree
        Path directory = fullPath.getParent();
        while (directory != null && FileUtilsHelper.isDirEmpty(directory)){
            Files.delete(directory);
            directory = directory.getParent();
        }
    }
}
