package com.magit.core.merge;

import com.magit.common.utils.FileSystemFactory;
import com.magit.core.factories.StagingCommitFactory;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.objects.Commit;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileStateProvider {
    private Path commitDir;

    public FileStateProvider(Commit commit) throws Exception {
        this.commitDir = FileSystemFactory.createTempDirectory("magit-commit-state-");
        commit.checkout(this.commitDir);
    }

    public FileStateProvider(Path commitDirPath) {
        this.commitDir = commitDirPath;
    }

    public FileState getStateForPath(Path path) throws IOException {
        Path fullPath = this.commitDir.resolve(path);
        if (!Files.isRegularFile(fullPath)){
            return new FileState(FileState.State.NON_EXISTING);
        }
        String fileContent = new String(Files.readAllBytes(fullPath), StandardCharsets.UTF_8);
        return new FileState(FileState.State.EXISTING, fileContent);
    }

    public String getContent(Path relativePath) throws Exception {
        Path fullPath = this.commitDir.resolve(relativePath);
        if (!Files.isRegularFile(fullPath)){
            return "";
        }
        return new String(Files.readAllBytes(fullPath), StandardCharsets.UTF_8);
    }

    public IFolderComparer compareTo(FileStateProvider commitStateProvider) throws Exception {
        IFolderComparer comparer = StagingCommitFactory.createFolderComparer();
        comparer.compare(this.commitDir, commitStateProvider.commitDir);
        return comparer;
    }

    public void applyFileState(FileState state, Path path) throws IOException {
        state.apply(commitDir.resolve(path));
    }
}
