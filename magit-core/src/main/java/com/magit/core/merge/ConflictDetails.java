package com.magit.core.merge;

import com.magit.common.utils.FileUtilsHelper;
import org.apache.commons.io.FileUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class ConflictDetails {
    private FileState headVersion;
    private FileState otherVersion;
    private FileState baseVersion;
    private FileState finalVersion;
    private Path finalVersionFile;

    public ConflictDetails(FileState headVersion, FileState otherVersion, FileState baseVersion, Path finalVersionFile) {
        this.headVersion = headVersion;
        this.otherVersion = otherVersion;
        this.baseVersion = baseVersion;
        this.finalVersion = headVersion;
        this.finalVersionFile = finalVersionFile;
    }

    public FileState getFinalVersion() {
        return finalVersion;
    }

    public void setFinalVersion(FileState finalVersion) {
        this.finalVersion = finalVersion;
    }

    public Path getFinalVersionFile() {
        return finalVersionFile;
    }

    public FileState getHeadVersion() {
        return headVersion;
    }

    public FileState getOtherVersion() {
        return otherVersion;
    }

    public FileState getBaseVersion() {
        return baseVersion;
    }

    public void markResolved(Path workingCopyDirectory) throws IOException {
        this.finalVersion.apply(workingCopyDirectory.resolve(finalVersionFile));
    }
}
