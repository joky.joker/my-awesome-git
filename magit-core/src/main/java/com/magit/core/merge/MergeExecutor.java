package com.magit.core.merge;

import com.magit.core.interfaces.IMerger;
import com.magit.core.objects.Commit;
import com.magit.core.objects.Repository;

import java.io.IOException;
import java.util.List;

public class MergeExecutor {
    List<ConflictDetails> conflictDetails;
    Repository repo;
    IMerger merger;
    Commit otherCommit;
    public MergeExecutor(List<ConflictDetails> conflictDetails, IMerger merger, Repository repo, Commit otherCommit) {
        this.conflictDetails = conflictDetails;
        this.merger = merger;
        this.repo = repo;
        this.otherCommit = otherCommit;
    }

    public List<ConflictDetails> updateConflictList(){ return this.conflictDetails;}
    public boolean isFFMerge(){return merger.isFFMerge();}

    public void execute(String message, String user) throws Exception {
        for (ConflictDetails conflictDetails : this.conflictDetails){
            conflictDetails.markResolved(this.repo.getRepoDir());
        }
        this.repo.forceCommitStagedChanges(message, user, otherCommit.getSha1Identifier());
    }
}
