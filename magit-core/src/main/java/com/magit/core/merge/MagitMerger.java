package com.magit.core.merge;

import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.interfaces.IMerger;
import com.magit.core.objects.Commit;
import com.magit.core.objects.Repository;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class MagitMerger implements IMerger {
    private static Logger log = Logger.getLogger(MagitMerger.class.getTypeName());

    private FileStateProvider headStateProvider;
    private FileStateProvider otherStateProvider;
    private FileStateProvider baseStateProvider;
    private FileStateProvider wcStateProvider;
    private Repository repo;
    private Commit otherCommit;
    private boolean isFFMerge = false;

    @Override
    public void fromCommits(Repository repo, Commit headCommit, Commit otherCommit, Commit baseCommit) throws Exception{
        this.headStateProvider = new FileStateProvider(headCommit);
        this.otherStateProvider = new FileStateProvider(otherCommit);
        this.baseStateProvider = new FileStateProvider(baseCommit);
        this.wcStateProvider = new FileStateProvider(repo.getRepoDir());
        this.otherCommit = otherCommit;
        this.repo = repo;
        this.isFFMerge = headCommit.equals(baseCommit);
    }

    private Set<Path> getComparerFiles(IFolderComparer comparer){
        Set filesToCheck = new HashSet<Path>();
        filesToCheck.addAll(comparer.getModified());
        filesToCheck.addAll(comparer.getNewFiles());
        filesToCheck.addAll(comparer.getRemoved());
        return filesToCheck;
    }
    @Override
    public boolean isFFMerge(){return this.isFFMerge;}

    public MergeExecutor merge() throws Exception {
        List<ConflictDetails> conflictDetails = new ArrayList<>();
        IFolderComparer comparer = headStateProvider.compareTo(baseStateProvider);

        Set<Path> filesToCheck = getComparerFiles(comparer);
        comparer = otherStateProvider.compareTo(baseStateProvider);
        filesToCheck.addAll(getComparerFiles(comparer));
        return new MergeExecutor(mergeFiles(filesToCheck), this, this.repo, otherCommit);
    }

    private List<ConflictDetails> mergeFiles(Set<Path> files) throws Exception{
        List<ConflictDetails> conflictDetails = new ArrayList<>();
        for (Path filePath : files){
            //head and other do not agree
            FileState headFileState = headStateProvider.getStateForPath(filePath);
            FileState baseFileState = baseStateProvider.getStateForPath(filePath);
            FileState othersFileState = otherStateProvider.getStateForPath(filePath);
            if (headFileState.equals(baseFileState)){
                // take other's version of the file
                wcStateProvider.applyFileState(othersFileState, filePath);
                continue;
            }
            if (othersFileState.equals(baseFileState)){
                wcStateProvider.applyFileState(headFileState, filePath);
                continue;
            }
            //nobody agrees
            conflictDetails.add(new ConflictDetails(headFileState,
                    othersFileState,
                    baseFileState,
                    filePath));
        }
        return conflictDetails;
    }

}
