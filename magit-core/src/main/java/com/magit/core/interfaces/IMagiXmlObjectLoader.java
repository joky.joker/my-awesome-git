package com.magit.core.interfaces;

import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.core.objects.MagiFileInfo;
import org.jdom2.Element;

public interface IMagiXmlObjectLoader {
    MagiFileInfo deserializeBlob(Element blobElement) throws Exception;
    MagiFileInfo deserializeFolder(Element folderElement) throws Exception;
    Commit deserializeCommit(Element commitElement) throws Exception;
    Branch deserializeBranch(Element branchElement) throws Exception;
}

