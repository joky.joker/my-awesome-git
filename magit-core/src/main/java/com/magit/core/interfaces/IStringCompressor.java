package com.magit.core.interfaces;

import java.security.InvalidParameterException;

public interface IStringCompressor {
    byte[] compress(String text);
    String decompress(byte[] bytes) throws InvalidParameterException;
}
