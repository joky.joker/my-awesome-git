package com.magit.core.interfaces;

public interface ISha1Hashable {
    String getSha1Identifier();
}
