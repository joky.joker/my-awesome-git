package com.magit.core.interfaces;

public interface ExceptionableConsumer<T> {
    void apply(T t) throws Exception;
}
