package com.magit.core.interfaces;

import com.magit.core.merge.MergeExecutor;
import com.magit.core.objects.Commit;
import com.magit.core.objects.Repository;

import java.nio.file.Path;

public interface IMerger {
    void fromCommits(Repository repo, Commit headCommit, Commit otherCommit, Commit baseCommit) throws Exception;
    MergeExecutor merge() throws Exception;
    boolean isFFMerge();
}
