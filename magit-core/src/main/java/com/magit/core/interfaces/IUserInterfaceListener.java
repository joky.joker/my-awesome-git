package com.magit.core.interfaces;

import com.magit.core.merge.MergeExecutor;
import com.magit.core.objects.Branch;

import java.nio.file.Path;

public interface IUserInterfaceListener {
    void onCommit(String message) throws Exception;
    void onCommit(String message, String otherParent) throws Exception;
    void onRepoOpened(String path) throws Exception;
    void setUserName(String username);

    MergeExecutor onMerge(String otherBranch) throws Exception;

    Path onRepoXmlLoaded(String path) throws Exception;

    void onNewLocalBranchCreated(String name, String commitId) throws Exception;
    void onNewRTBranchCreated(Branch remote) throws Exception;

    void onBranchDeleted(String branchName) throws Exception;

    void onSetHeadBranchCheckout(String branchName) throws Exception;

    void overrideBranchCommit(String head, String sha1) throws Exception;
    void onXmlDump() throws Exception;
    void onClone(String repoName, Path destination, Path remoteRepository) throws Exception;
    void onFetch() throws Exception;
    void onPull() throws Exception;
    void onPush() throws Exception;

    void onPushLocalBranch(Branch localBranch) throws Exception;
}
