package com.magit.core.interfaces;

import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.core.objects.MagiFileInfo;
import com.magit.core.utils.MagitFS;
import javafx.util.Pair;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IInformationProvider {
    Set<Path> getNewUnstagedFiles() throws Exception;
    Set<Path> getModifiedUnstagedFiles() throws Exception;
    Set<Path> getRemovedUnstagedFiles() throws Exception;

    String getRepoName();
    String getCurrentRepositoryLocation();
    Path getCurrentRepositoryPath();
    String getCurrentUsername();

    Map<String, MagiFileInfo> getCommitItemDetails() throws Exception;
    Map<String, MagiFileInfo> getCommitItemDetails(Commit commit) throws Exception;

    List<Pair<Branch, String>> getBranchsInformation() throws Exception;

    String getHead();

    Branch getHeadBranch() throws Exception;

    boolean doesBranchExist(String name) throws Exception;

    boolean isChangesPending() throws Exception;

    boolean isInitilized() throws Exception;
    boolean isRepositoryLoaded();
    boolean isRemoteRepo();

    List<Commit> getCommitHistory(String commitSha1) throws Exception;

    MagitFS getRepoFS() throws Exception;

    Commit getCommitBySha1(String sha1) throws Exception;
    Set<Commit> getLeafsCommits() throws Exception;
    Map<String, List<Branch>> getCommitBranchMap() throws Exception;

}
