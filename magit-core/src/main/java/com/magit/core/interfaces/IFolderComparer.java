package com.magit.core.interfaces;

import java.nio.file.Path;
import java.util.Set;

public interface IFolderComparer {
    void compare(Path master, Path slave) throws Exception;
    Set<Path> getNewFiles();
    Set<Path> getRemoved();
    Set<Path> getModified();
    boolean isNew(Path path);
    boolean isDeleted(Path path);
    boolean isModified(Path path);
    boolean hasChanges();
}
