package com.magit.core.interfaces;

import com.magit.core.objects.*;

import java.util.List;
import java.util.Set;

public interface IMagiObjectSerializer<T> {
    T serializeBlob(Blob blob);
    MagiFileInfo deserializeBlob(T input) throws Exception;

    T serializeFolder(Folder folder);
    MagiFileInfo deserializeFolder(T bytes) throws Exception;

    T serializeCommit(Commit commit);
    Commit deserializeCommit(T input) throws Exception;

    T serializeBranch(Branch branch);
    Branch deserializeBranch(T input) throws Exception;

    T serializeCommitList(Set<String> commitList);
    Set<String> commitList(T input) throws Exception;
}
