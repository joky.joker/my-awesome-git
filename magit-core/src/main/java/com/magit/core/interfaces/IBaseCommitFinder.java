package com.magit.core.interfaces;

import com.magit.core.objects.Commit;

public interface IBaseCommitFinder {
    Commit findCommonBaseCommit(Commit first, Commit second) throws Exception;
}
