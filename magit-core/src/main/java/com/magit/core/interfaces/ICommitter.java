package com.magit.core.interfaces;

import com.magit.core.objects.Commit;
import com.magit.core.objects.Repository;

public interface ICommitter {
    Commit commitChanges(IFolderComparer comparer, String secondParent, Repository repo, String message, String user, boolean forceCommit) throws Exception;
    Commit commitChanges(IFolderComparer comparer, String secondParent, Repository repo, String message, String user) throws Exception;
}
