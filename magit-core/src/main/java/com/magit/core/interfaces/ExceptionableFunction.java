package com.magit.core.interfaces;

public interface ExceptionableFunction<T, R> {
    R apply(T t) throws Exception;
}
