package com.magit.core.controllers;

import com.magit.common.CurrentUser;
import com.magit.common.utils.FileSystemFactory;
import com.magit.core.exceptions.RepositoryUninitializedException;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.merge.MergeExecutor;
import com.magit.core.objects.*;
import com.magit.core.serializers.RepoXmlLoader;
import com.magit.core.utils.MagitFS;
import javafx.util.Pair;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class Controller implements IUserInterfaceListener, IInformationProvider {
    private Repository repo;
    private RepoXmlLoader loader;

    private void validateRepoLoaded() throws Exception{
        if (repo == null){
            throw new Exception("No Repository was chosen");
        }
    }

    private void validateRepoInitilized() throws Exception{
        validateRepoLoaded();
        if (!repo.isInitialized()){
            throw new RepositoryUninitializedException("Repository isnt Initialized, must make fist commit");
        }
    }
    private IFolderComparer getComparer() throws Exception {
        validateRepoLoaded();
        return repo.getChanges();
    }
    private static Set<Path> relativize(Path base, Collection<Path> paths){
        return paths.stream().map(base.toAbsolutePath()::resolve).collect(Collectors.toSet());
    }

    @Override
    public String getRepoName() {
        if (repo == null){
            return null;
        }
        return repo.getName();
    }
    @Override
    public Set<Path> getNewUnstagedFiles() throws Exception {
        validateRepoLoaded();
        return relativize(repo.getRepoDir(), getComparer().getNewFiles());
    }

    @Override
    public Set<Path> getModifiedUnstagedFiles() throws Exception {
        validateRepoLoaded();
        return relativize(repo.getRepoDir(), getComparer().getModified());
    }

    @Override
    public Set<Path> getRemovedUnstagedFiles() throws Exception{
        validateRepoLoaded();
        return relativize(repo.getRepoDir(), getComparer().getRemoved());
    }

    @Override
    public String getCurrentRepositoryLocation(){
        if (repo == null){
            return null;
        }
        return repo.getRepoDir().toAbsolutePath().toString();
    }

    @Override
    public Path getCurrentRepositoryPath(){
        if (repo == null){
            return null;
        }
        return repo.getRepoDir();
    }

    @Override
    public String getCurrentUsername() {
        return CurrentUser.getCurrentUser();
    }

    @Override
    public Map<String, MagiFileInfo> getCommitItemDetails() throws Exception {
        validateRepoLoaded();
        CommitItemDetailsGenerator generator = new CommitItemDetailsGenerator(repo);
        return generator.generateFilesDetails(repo.getCurrentBranch().getPointerCommit());
    }

    @Override
    public Map<String, MagiFileInfo> getCommitItemDetails(Commit commit) throws Exception {
        validateRepoLoaded();
        CommitItemDetailsGenerator generator = new CommitItemDetailsGenerator(repo);
        return generator.generateFilesDetails(commit);
    }

    @Override
    public List<Pair<Branch, String>> getBranchsInformation() throws Exception{
        validateRepoInitilized();
        ArrayList<Pair<Branch, String>> branchesList = new ArrayList<>();
        MagitFS fs = repo.getFileSystem();
        for (Branch branch : fs.listLocalBranches()){
            Commit commit = fs.readCommitObject(branch.getCommitPointed());
            branchesList.add(new Pair<>(branch, commit.getMessage()));
        }
        return branchesList;
    }

    @Override
    public String getHead() {
        try{
            validateRepoLoaded();
            return repo.getActiveBranch();
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public Branch getHeadBranch() throws Exception {
        validateRepoLoaded();
        return repo.getFileSystem().readLocalBranchObject(repo.getActiveBranch());
    }

    @Override
    public boolean doesBranchExist(String name) throws Exception{
        validateRepoLoaded();
        return repo.getFileSystem().isBranchExist(name);
    }

    @Override
    public boolean isChangesPending() throws Exception{
        validateRepoLoaded();
        return getComparer().hasChanges();
    }

    @Override
    public boolean isInitilized() throws Exception {
        validateRepoLoaded();
        return repo.isInitialized();
    }

    public boolean isRepositoryLoaded(){
        return repo != null;
    }

    @Override
    public boolean isRemoteRepo() {
        if (repo == null){
            return false;
        }
        return repo.getFileSystem().isRemote();
    }

    @Override
    public List<Commit> getCommitHistory(String commitSha1) throws Exception{
        validateRepoLoaded();
        ArrayList<Commit> commitHistory = new ArrayList<>();
        MagitFS fileSystem = repo.getFileSystem();
        ArrayDeque<Commit> cmtQueue = new ArrayDeque<>();
        cmtQueue.add(fileSystem.readCommitObject(commitSha1));
        while(!cmtQueue.isEmpty()){
            Commit currentCommit = cmtQueue.remove();
            commitHistory.add(currentCommit);
            currentCommit.applyOnCommitParents(cmtQueue::add);
        }
        return commitHistory;
    }

    @Override
    public MagitFS getRepoFS() throws Exception {
        validateRepoLoaded();
        return repo.getFileSystem();
    }

    @Override
    public Commit getCommitBySha1(String sha1) throws Exception {
        validateRepoLoaded();
        return repo.getFileSystem().readCommitObject(sha1);
    }

    @Override
    public Set<Commit> getLeafsCommits() throws Exception {
        validateRepoLoaded();
        Set<String> commitIDList = repo.getFileSystem().readCommitList();
        Set<Commit> commitList = new HashSet<>();
        for (String commitId: commitIDList){
            commitList.add(repo.getFileSystem().readCommitObject(commitId));
        }
        return commitList;
    }

    @Override
    public Map<String, List<Branch>> getCommitBranchMap() throws Exception {
        validateRepoInitilized();
        Map<String, List<Branch>> map = new HashMap<>();
        for (Branch branch: repo.getFileSystem().listAllBranches()){
            String commitId = branch.getCommitPointed();
            if (!map.containsKey(commitId)){
                map.put(commitId, new ArrayList<>());
            }
            map.get(commitId).add(branch);
        }
        return map;
    }

    @Override
    public void onCommit(String message) throws Exception {
        validateRepoLoaded();
        repo.commitStagedChanges(message, getCurrentUsername());
    }

    @Override
    public void onCommit(String message, String otherParent) throws Exception {
        validateRepoLoaded();
        repo.commitStagedChanges(message, getCurrentUsername(), otherParent);
    }

    @Override
    public void onRepoOpened(String path) throws Exception {
        repo = Repository.openRepository(FileSystemFactory.toPath(path));
    }

    @Override
    public void setUserName(String username) {
        CurrentUser.setCurrentUser(username);
    }

    @Override
    public MergeExecutor onMerge(String otherBranch) throws Exception {
        validateRepoInitilized();
        return this.repo.mergeHeadToBranch(otherBranch);
    }

    @Override
    public Path onRepoXmlLoaded(String path) throws Exception {
        RepoXmlLoader xmlLoader = new RepoXmlLoader();
        xmlLoader.loadRepoFromFile(FileSystemFactory.getFs().getPath(path));
        loader = xmlLoader;
        return loader.getLocation();
    }

    @Override
    public void onXmlDump() throws Exception{
        if (loader == null){
            throw new Exception("loader isnt initialized");
        }
        repo = loader.dumpRepo();
        loader = null;
    }

    @Override
    public void onClone(String repoName, Path destination, Path remoteRepository) throws Exception {
        repo = LocalRepository.clone(repoName, destination, remoteRepository);
    }

    private LocalRepository validateRemoteAndGetLocalRepository() throws Exception{
        if (!repo.getFileSystem().isRemote()){
            throw new Exception("Repository isn't connected to remote repo");
        }
        LocalRepository localRepository;
        if (repo instanceof LocalRepository){
            localRepository = (LocalRepository)repo;
        }else{
            localRepository = new LocalRepository(repo);
        }
        repo = localRepository;
        return localRepository;
    }

    @Override
    public void onFetch() throws Exception {
        LocalRepository localRepository = validateRemoteAndGetLocalRepository();
        localRepository.fetch();
    }

    @Override
    public void onPull() throws Exception {
        LocalRepository localRepository = validateRemoteAndGetLocalRepository();
        localRepository.pull();
    }

    @Override
    public void onPush() throws Exception {
        LocalRepository localRepository = validateRemoteAndGetLocalRepository();
        localRepository.push();
    }

    @Override
    public void onPushLocalBranch(Branch localBranch) throws Exception {
        LocalRepository localRepository = validateRemoteAndGetLocalRepository();
        localRepository.pushLocalBranch(localBranch);
    }

    @Override
    public void onNewLocalBranchCreated(String name, String commitID) throws Exception{
        validateRepoInitilized();
        MagitFS fs = repo.getFileSystem();
        if (fs.isBranchExist(name)){
            throw new Exception("Branch Already Exist");
        }
        Branch branch = new Branch(name, commitID);
        fs.writeObject(branch);
    }

    @Override
    public void onNewRTBranchCreated(Branch remote) throws Exception {
        validateRepoInitilized();
        MagitFS fs = repo.getFileSystem();
        if (fs.isBranchExist(remote.getName())){
            throw new Exception("Branch Already Exist");
        }
        Branch rtb = new Branch(remote.getName(), remote.getCommitPointed());
        rtb.setTrackingAfter(remote.getFullName());
        fs.writeObject(rtb);
    }

    @Override
    public void onBranchDeleted(String branchName) throws Exception{
        validateRepoInitilized();
        if (branchName.equals(repo.getActiveBranch())){
            throw new Exception("Cannot delete head branch");
        }
        repo.getFileSystem().removeLocalBranch(branchName);
    }

    @Override
    public void onSetHeadBranchCheckout(String branchName) throws Exception{
        validateRepoInitilized();
        MagitFS fs = repo.getFileSystem();
        if (!fs.isBranchExist(branchName)){
            throw new Exception("Branch name " + branchName + " does not exist");
        }
        repo.setActiveBranch(branchName);
        repo.checkout(branchName);
    }

    @Override
    public void overrideBranchCommit(String head, String sha1) throws Exception {
        validateRepoInitilized();
        try{
            Commit commit = repo.getFileSystem().readCommitObject(sha1);
        } catch (AssertionError | FileNotFoundException e){
            throw new Exception("invalid commit hash " + sha1);
        }
        Branch headBranch = repo.getFileSystem().readLocalBranchObject(head);
        headBranch.setCommitPointed(sha1);
        repo.getFileSystem().writeObject(headBranch);
    }
}
