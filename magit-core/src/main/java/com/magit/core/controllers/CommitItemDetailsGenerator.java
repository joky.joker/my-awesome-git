package com.magit.core.controllers;

import com.magit.core.objects.*;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CommitItemDetailsGenerator {
    private Map<String, MagiFileInfo> map;
    private Set<MagitObject> set;
    private Repository repo;
    public CommitItemDetailsGenerator(Repository repository){
        repo = repository;
    }

    public Map<String, MagiFileInfo> generateFilesDetails(Commit commit) throws Exception {
        if (!repo.isInitialized()){
            return new HashMap();
        }
        map = new HashMap<>();
        Folder rootFolder = commit.getRootFolder();
        generateMap(rootFolder, repo.getRepoDir().toAbsolutePath(), false);
        return map;
    }

    public Set<MagitObject> generateItemsDetails(Commit commit) throws Exception {
        if (!repo.isInitialized()){
            return new HashSet<>();
        }
        set = new HashSet<>();
        Folder rootFolder = commit.getRootFolder();
        set.add(rootFolder);
        generateSet(rootFolder);
        return set;
    }

    private void generateSet(Folder rootFolder) throws Exception{
        for (Map.Entry<String, MagiFileInfo> entry : rootFolder.getFiles().entrySet()){
            set.add(entry.getValue().loadMagiObj());
            if (entry.getValue().getType().equals(MagiObjectType.FOLDER)){
                generateSet((Folder) entry.getValue().loadMagiObj());
            }
        }
    }

    private void generateMap(Folder rootFolder, Path absolutePath, boolean includeFolders) throws Exception{
        for (Map.Entry<String, MagiFileInfo> entry : rootFolder.getFiles().entrySet()){
            Path itemPath = absolutePath.resolve(entry.getValue().getName());
            if (entry.getValue().getType().equals(MagiObjectType.BLOB)){
                map.put(repo.getRepoDir().relativize(itemPath).toString(), entry.getValue());
                continue;
            }
            if (includeFolders && entry.getValue().getType().equals(MagiObjectType.FOLDER)){
                map.put(repo.getRepoDir().relativize(itemPath).toString(), entry.getValue());
            }
            generateMap((Folder)entry.getValue().loadMagiObj(), itemPath, includeFolders);
        }
    }
}
