package com.magit.core.controllers;

import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.objects.Commit;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;

import java.util.*;
import java.util.logging.Logger;

public class CommitGraphGenerator {
    static Logger log = Logger.getLogger(CommitGraphGenerator.class.getTypeName());

    Set<Commit> leafCommits;
    IInformationProvider provider;

    public  CommitGraphGenerator(IInformationProvider provider, Set<Commit> leafCommits){
        this.leafCommits = leafCommits;
        this.provider = provider;
    }

    public Graph<Commit, DefaultEdge> produceTreeGraph() throws Exception {
        Graph<Commit, DefaultEdge> commitGraph = new SimpleDirectedGraph<>(DefaultEdge.class);
        Queue<Commit> commitQueue = new ArrayDeque<>();
        commitQueue.addAll(leafCommits);
        while (!commitQueue.isEmpty()){
            Commit currentCommit = commitQueue.remove();
            commitGraph.addVertex(currentCommit);
            currentCommit.applyOnParents(parent ->{
                    Commit parentCommit = provider.getCommitBySha1(parent);
                    commitQueue.add(parentCommit);

                    commitGraph.addVertex(parentCommit);
                    commitGraph.addEdge(parentCommit, currentCommit);
                    log.info(parentCommit.getSha1Identifier() + "->" + currentCommit.getSha1Identifier());
            });
        }
        log.info(String.valueOf(commitGraph));
        return commitGraph;
    }

    public static Set<Commit> getAncestors(Commit someCommit) throws Exception {
        Set<Commit> ancestors = new HashSet<>();
        Queue<Commit> commitQueue = new ArrayDeque<>();
        commitQueue.add(someCommit);
        while (!commitQueue.isEmpty()){
            Commit currentCommit = commitQueue.remove();
            ancestors.add(currentCommit);
            currentCommit.applyOnCommitParents(commitParent -> {
                commitQueue.add(commitParent);
            });
        }
        return ancestors;
    }
}
