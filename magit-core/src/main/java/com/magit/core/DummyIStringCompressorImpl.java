package com.magit.core;

import com.magit.core.interfaces.IStringCompressor;

import java.nio.charset.StandardCharsets;
import java.security.InvalidParameterException;

public class DummyIStringCompressorImpl implements IStringCompressor {
    @Override
    public byte[] compress(String text) {
        return text.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public String decompress(byte[] bytes) throws InvalidParameterException {
        return new String(bytes);
    }
}
