package com.magit.core.objects;

import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.controllers.CommitGraphGenerator;
import com.magit.core.controllers.CommitItemDetailsGenerator;
import com.magit.core.utils.MagitFS;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class LocalRepository extends Repository {
    private static final Logger log = Logger.getLogger(LocalRepository.class.getTypeName());
    private String remoteRepoName;
    private Path remoteRepoLocation;

    public LocalRepository(Path repoPath) {
        super(repoPath);
    }

    public LocalRepository(Repository repository){
        super(repository.getRepoDir());
    }

    public static LocalRepository createRepository(Path dest, String name) throws IOException {
        return new LocalRepository(Repository.createRepository(dest,name));
    }

    public static LocalRepository clone(String name, Path destination, Path remoteRepository) throws Exception {
        Files.createDirectories(destination);
        if (FileUtilsHelper.listFilesRelative(destination).size() != 0){
            throw new Exception("Destination path is not empty");
        }
        Repository remoteRepo = Repository.openRepository(remoteRepository);
        MagitFS remoteFs = remoteRepo.getFileSystem();

        //to prevent the createRepository from exceptioning
        Files.deleteIfExists(destination);
        LocalRepository localRepo = LocalRepository.createRepository(destination, name);
        MagitFS localFs = localRepo.getFileSystem();

        localRepo.setRemoteRepoLocation(remoteRepo.getRepoDir());
        localRepo.setRemoteRepoName(remoteRepo.getName());

        //copy the objects dir to our local repo
        FileUtilsHelper.copyDirectoryContent(remoteRepo.getObjectDir(),
                localRepo.getObjectDir());

        //copy the commit list
        localFs.writeCommitList(remoteFs.readCommitList());
        localFs.removeLocalBranch("master", true);
        //copy the branches list
        Set<Branch> branches = remoteRepo.getFileSystem().listLocalBranches();
        for (Branch branch : branches){
            Branch remoteBranch = new Branch(branch.getName(), branch.getCommitPointed());
            remoteBranch.setFs(localFs);
            remoteBranch.setRemote(true);
            remoteBranch.save();

            //create RTB for the head
            if (branch.getName().equals(remoteRepo.getActiveBranch())){
                Branch remoteTrackingBranch = new Branch(branch.getName(), branch.getCommitPointed());
                remoteTrackingBranch.setTrackingAfter(remoteBranch.getFullName());
                remoteTrackingBranch.setFs(localFs);
                remoteTrackingBranch.save();
            }
        }
        localRepo.setActiveBranch(remoteRepo.getActiveBranch());
        localRepo.checkout();
        return localRepo;
    }

    public void fetch() throws Exception {
        Repository remoteRepo = Repository.openRepository(getRemoteRepoLocation());
        //copy all the new objects from the objects dir
        FileUtilsHelper.copyDirectoryContent(remoteRepo.getObjectDir(), this.getObjectDir());

        //merge the commit lists
        MagitFS fs = getFileSystem();
        MagitFS remoteFs = remoteRepo.getFileSystem();
        Set merge = remoteRepo.getFileSystem().readCommitList();
        merge.addAll(fs.readCommitList());
        fs.writeCommitList(merge);

        //update all the RBs
        for (Branch remoteBranch : fs.listRemoteBranches()){
            Optional<Branch> remoteRepoBranch = remoteFs.listLocalBranches()
                    .stream()
                    .filter(branch -> branch.getName().equals(remoteBranch.getName())).findAny();
            if (!remoteRepoBranch.isPresent()){
                log.warning("possible detection of deleted branch " + remoteBranch.getFullName());
                continue;
            }
            String remoteRepoBranchCommit = remoteRepoBranch.get().getCommitPointed();
            if (!remoteRepoBranchCommit.equals(remoteBranch.getCommitPointed())){
                log.info("Updating remote branch " + remoteBranch.getFullName());
            }
            remoteBranch.setCommitPointed(remoteRepoBranchCommit);
            remoteBranch.save();
        }
    }

    public void pull() throws Exception {
        Branch head = getCurrentBranch();
        Repository remoteRepo = Repository.openRepository(getRemoteRepoLocation());
        if (!head.isTracking()){
            throw new Exception("head branch is not tracking after remote branch");
        }

        if (!isCleanState()){
            throw new Exception("cant pull, repository has pending changes");
        }
        Branch remoteBranch = getFileSystem().readBranchObject(head.getTrackingAfter());
        if (!remoteBranch.getCommitPointed().equals(head.getCommitPointed())){
            throw new Exception("RTB is not synced with RB, must pull first");
        }
        Branch remoteRepoBranch = remoteRepo.getFileSystem().readLocalBranchObject(remoteBranch.getName());
        Set<Commit> ancestors = CommitGraphGenerator.getAncestors(remoteRepoBranch.getPointerCommit());
        copyCommitListToRepo(ancestors, remoteRepo, this);
        //update the remoteBranch
        remoteBranch.setCommitPointed(remoteRepoBranch.getCommitPointed());
        remoteBranch.save();
        head.setCommitPointed(remoteRepoBranch.getCommitPointed());
        head.save();
        checkout();
    }

    public void push() throws Exception {
        Branch head = getCurrentBranch();
        Repository remoteRepo = Repository.openRepository(getRemoteRepoLocation());
        if (!head.isTracking()){
            throw new Exception("head branch is not tracking after remote branch");
        }
        Branch remoteBranch = getFileSystem().readBranchObject(head.getTrackingAfter());
        Branch remoteRepoBranch = remoteRepo.getFileSystem().readLocalBranchObject(remoteBranch.getName());
        if (!remoteBranch.getCommitPointed().equals(remoteRepoBranch.getCommitPointed())){
            throw new Exception("branch in remote repository is not synced in the local repository");
        }
        if (!remoteRepo.isCleanState()){
            throw new Exception("remote repository has unstaged changes pending, cant push");
        }
        Set<Commit> ancestors = CommitGraphGenerator.getAncestors(head.getPointerCommit());
        copyCommitListToRepo(ancestors, this, remoteRepo);
        //update the remoteBranch
        remoteRepoBranch.setCommitPointed(head.getCommitPointed());
        remoteRepoBranch.save();
        remoteBranch.setCommitPointed(head.getCommitPointed());
        remoteBranch.save();
        //if this is the head branch in the remote repo, checkout to it.
        if (remoteRepo.getActiveBranch().equals(remoteRepoBranch.getName())){
            remoteRepo.checkout(remoteRepoBranch.getName());
        }
    }

    private static void copyCommitListToRepo(Set<Commit> commitList, Repository srcRepository, Repository dstRepository) throws Exception {
        Set<String> lst = dstRepository.getFileSystem().readCommitList();
        CommitItemDetailsGenerator generator = new CommitItemDetailsGenerator(srcRepository);
        Set<Path> sha1sToCopy = new HashSet<>();
        for (Commit commit : commitList){
            sha1sToCopy.addAll(generator.generateItemsDetails(commit)
                    .stream()
                    .map(MagitObject::getSha1Identifier)
                    .map(FileSystemFactory::toPath)
                    .collect(Collectors.toSet()));
            sha1sToCopy.add(FileSystemFactory.toPath(commit.getSha1Identifier()));
            lst.add(commit.getSha1Identifier());
        }
        //update commit list
        dstRepository.getFileSystem().writeCommitList(lst);
        FileUtilsHelper.copyFiles(srcRepository.getObjectDir(), dstRepository.getObjectDir(), sha1sToCopy, false);
    }

    public String getRemoteRepoName() {
        if (remoteRepoName == null){
            try{
                remoteRepoName = getFileSystem().readRemoteRepoName();
            } catch (IOException e) {log.log(Level.SEVERE, "", e);}
        }
        return remoteRepoName;
    }

    public void setRemoteRepoName(String remoteRepoName) {
        this.remoteRepoName = remoteRepoName;
        try{
            getFileSystem().writeRemoteRepoName(remoteRepoName);
        }catch (IOException e) {log.log(Level.SEVERE, "", e);}
    }

    public Path getRemoteRepoLocation() {
        if (remoteRepoLocation == null){
            try{
                remoteRepoLocation = getFileSystem().readRemoteRepoPath();
            } catch (IOException e) {log.log(Level.SEVERE, "", e);}
        }
        return remoteRepoLocation;
    }

    public void setRemoteRepoLocation(Path remoteRepoLocation) {
        this.remoteRepoLocation = remoteRepoLocation;
        try{
            getFileSystem().writeRemoteRepoPath(remoteRepoLocation);
        } catch (IOException e) {log.log(Level.SEVERE, "", e);}
    }

    public void pushLocalBranch(Branch localBranch) throws Exception {
        Repository remoteRepo = Repository.openRepository(getRemoteRepoLocation());
        if (localBranch.isTracking() || localBranch.isRemote()){
            throw new Exception(localBranch.getName() + " isnt local, cant push!");
        }
        MagitFS remoteFs = remoteRepo.getFileSystem();
        if (remoteFs.isBranchExist(localBranch.getName())){
            throw new Exception("Branch with name " + localBranch.getName() + " already exists on remote repository");
        }
        //create branch in remote repo
        Branch remoteRepositoryBranch = new Branch(localBranch.getName(), localBranch.getCommitPointed());
        remoteRepositoryBranch.setFs(remoteFs);
        remoteRepositoryBranch.save();
        //create remote branch over here
        Branch remoteBranch = new Branch(localBranch.getName(), localBranch.getCommitPointed());
        remoteBranch.setFs(getFileSystem());
        remoteBranch.setRemote(true);
        remoteBranch.save();
        //convert branch to be tracking
        localBranch.setTrackingAfter(remoteBranch.getFullName());
        localBranch.save();

        Set<Commit> ancestors = CommitGraphGenerator.getAncestors(localBranch.getPointerCommit());
        copyCommitListToRepo(ancestors, this, remoteRepo);
    }


    private Branch validateRemoteExists(String name) throws Exception{
        Branch baseRemoteBranch = null;
        try {
            baseRemoteBranch = this.getFileSystem().readBranchObject(name);
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception("remote branch " + name + " does not exist");
        }
        return baseRemoteBranch;
    }
    public PullRequest createPullRequest(String initiator, String targetBranch, String baseBranch, String message) throws Exception{

//        Branch targetBranchLocal = this.getFileSystem().readLocalBranchObject(targetBranch);
//        //copy all relevant sha1's

        Repository remoteRepo = Repository.openRepository(getRemoteRepoLocation());
//        copyCommitListToRepo(ancestors, this, remoteRepo);
//        //create the target branch at the remote
//        remoteRepo.getFileSystem().writeObject(targetBranchLocal);
//        //should we turn target branch into rb?
//        //create pull request
        Branch baseRemoteBranch = validateRemoteExists(baseBranch);
        Branch targetRemoteBranch = validateRemoteExists(targetBranch);

        PullRequest pullRequest = new PullRequest();
        pullRequest.setInitiator(initiator);
        pullRequest.setBaseBranch(baseRemoteBranch.getName());
        pullRequest.setTargetBranch(targetRemoteBranch.getName());
        pullRequest.setLocalRepository(this);
        pullRequest.setRemoteRepository(remoteRepo);
        pullRequest.setMessage(message);

        remoteRepo.getFileSystem().addNewPullRequest(pullRequest);
        return pullRequest;
    }
}
