package com.magit.core.objects;

import com.magit.common.utils.HashUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;

//system supports text files only.
public class Blob extends MagitObject {
    private static final Logger log = Logger.getLogger(Blob.class.getSimpleName());

    public String getContent() {
        return m_plainContent;
    }

    public void setContent(String m_plainContent) {
        this.m_plainContent = m_plainContent;
    }

    private String m_plainContent;

    public Blob(String content){
        m_plainContent = content;
    }

    @Override
    public void checkout(Path path) {
        try{
            Files.write(path, getContent().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e){
            log.log(Level.SEVERE, "failed to create file at " + path.toString(), e);
        }
    }

    @Override
    public String getSha1Identifier() {
        MessageDigest sha1 = HashUtils.getSha1Digest();
        sha1.update(this.getContent().getBytes(StandardCharsets.UTF_8));
        return HashUtils.intoHex(sha1.digest());
    }

}
