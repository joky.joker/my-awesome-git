package com.magit.core.objects;

public enum MagiObjectType {
    FOLDER,
    BLOB;

    public static MagiObjectType fromString(String item){
        return MagiObjectType.valueOf(item.toUpperCase());
    }

    @Override
    public String toString(){
        return this.name().toLowerCase();
    }
}
