package com.magit.core.objects;

import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.HashUtils;
import com.magit.core.factories.StagingCommitFactory;
import com.magit.core.interfaces.ExceptionableConsumer;
import com.magit.core.interfaces.ExceptionableFunction;
import com.magit.core.utils.MagitFS;
import org.apache.commons.io.FileUtils;
import puk.team.course.magit.ancestor.finder.CommitRepresentative;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Set;

public class Commit extends MagitObject {
    private Folder rootFolder = null;
    private String rootFolderIdentifier;
    private String parent = null;
    private String secondParent = null;
    private String message;
    private String author;
    private Date dateOfCreation;

    public Folder getRootFolder() throws Exception{
        if (rootFolder == null){
            rootFolder = fs.readFolderObject(rootFolderIdentifier);
            if (fs != null){
                rootFolder.setFs(fs);
            }
        }
        return rootFolder;
    }

    public Commit setRootFolder(Folder rootFolder) {
        this.rootFolder = rootFolder;
        if (this.rootFolder != null){
            this.rootFolderIdentifier = this.rootFolder.getSha1Identifier();
            if (this.rootFolder.fs == null){
                this.rootFolder.fs = this.fs;
            }
        }
        return this;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public Commit setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public Commit setAuthor(String author) {
        this.author = author;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Commit setMessage(String message) {
        this.message = message;
        return this;
    }


    public String getSecondParent() {
        return secondParent;
    }

    public Commit setSecondParent(String secondParent) {
        this.secondParent = secondParent;
        return this;
    }

    public String getParent() {
        return parent;
    }

    public Commit getParentCommit() throws Exception {
        if (parent == null){
            return null;
        }
        return fs.readCommitObject(parent);
    }
    public Commit getSecondParentCommit() throws Exception{
        if (parent == null){
            return null;
        }
        return fs.readCommitObject(secondParent);
    }

    public Commit setParent(String parent) {
        this.parent = parent;
        return this;
    }

    public String getRootFolderIdentifier() {
        return rootFolderIdentifier;
    }

    public Commit setRootFolderIdentifier(String rootFolderIdentifier) {
        this.rootFolderIdentifier = rootFolderIdentifier;
        return this;
    }

    @Override
    public void checkout(Path path) throws Exception {
        Folder rootFolder = fs.readFolderObject(getRootFolderIdentifier());
        rootFolder.checkout(path);
    }

    @Override
    public String getSha1Identifier() {
        MessageDigest sha1 = HashUtils.getSha1Digest();
        sha1.update(getRootFolderIdentifier().getBytes(StandardCharsets.UTF_8));
        sha1.update(getMessage().getBytes(StandardCharsets.UTF_8));
        sha1.update(getAuthor().getBytes(StandardCharsets.UTF_8));
        sha1.update(getDateOfCreation().toString().getBytes(StandardCharsets.UTF_8));
        if (getParent() != null){
            sha1.update(getParent().getBytes(StandardCharsets.UTF_8));
        }
        if (getSecondParent() != null){
            sha1.update(getSecondParent().getBytes(StandardCharsets.UTF_8));
        }
        return HashUtils.intoHex(sha1.digest());
    }

    public void applyOnParents(ExceptionableConsumer<String> func) throws Exception {
        if (getParent() != null){
            func.apply(getParent());
        }
        if (getSecondParent() != null){
            func.apply(getSecondParent());
        }
    }

    public void applyOnCommitParents(ExceptionableConsumer<Commit> func) throws Exception {
        if (getParent() != null){
            func.apply(getParentCommit());
        }
        if (getSecondParent() != null){
            func.apply(getSecondParentCommit());
        }
    }

    public boolean allParents(ExceptionableFunction<String, Boolean> func) throws Exception {
        boolean result = true;
        if (getParent() != null){
            result = result && func.apply(getParent());
        }
        if (result && getSecondParent() != null){
            result = result && func.apply(getSecondParent());
        }
        return result;
    }

    public String getContentByPath(Path relativePath) throws Exception{
        Path tempDir = FileSystemFactory.createTempDirectory("magit-commitFindPath");
        this.checkout(tempDir);
        Path fullPath = tempDir.resolve(relativePath);
        if (!Files.isRegularFile(fullPath)){
            return "";
        }
        return new String(Files.readAllBytes(fullPath), StandardCharsets.UTF_8);
    }

    public boolean isMultiParent(){
        return parent != null && secondParent != null;
    }
    @Override
    public String toString() {
        return "Commit: " + getSha1Identifier();
    }

}
