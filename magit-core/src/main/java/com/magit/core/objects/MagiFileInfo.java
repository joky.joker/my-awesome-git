package com.magit.core.objects;

import com.magit.common.utils.HashUtils;
import com.magit.core.utils.MagitFS;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.Date;

public class MagiFileInfo extends MagitObject {
    private String name;
    private MagitObject magiObj;
    private String objIdentifier;
    private String lastChanger;
    private Date lastChanged;
    private MagiObjectType type;

    public String getObjIdentifier() {
        return objIdentifier;
    }

    public void setObjIdentifier(String objIdentifier) {
        this.objIdentifier = objIdentifier;
    }

    public MagiObjectType getType() {
        return type;
    }

    public void setType(MagiObjectType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MagitObject getMagiObj() {
        return magiObj;
    }

    @Override
    public MagitObject setFs(MagitFS fs){
        this.fs = fs;
        if (this.magiObj != null){
            this.magiObj.setFs(fs);
        }
        return this;
    }

    @Override
    public void save() throws Exception{
        loadMagiObj().save();
    }

    public MagitObject loadMagiObj() throws Exception{
        if (fs == null){
            throw new Exception("Filesystem is null, cant load");
        }
        if (magiObj == null){
            if (getType().equals(MagiObjectType.FOLDER)){
                magiObj = fs.readFolderObject(getObjIdentifier());
            } else if (getType().equals(MagiObjectType.BLOB)){
                magiObj = fs.readBlobObject(getObjIdentifier());
            }
        }
        return magiObj;
    }


    public void setMagiObj(MagitObject magiObj) {
        this.magiObj = magiObj;
        if (magiObj == null){
            objIdentifier = null;
            return;
        }
        if (magiObj.fs == null){
            magiObj.fs = this.fs;
        }
        objIdentifier = magiObj.getSha1Identifier();
    }

    public String getLastChanger() {
        return lastChanger;
    }

    public void setLastChanger(String lastChanger) {
        this.lastChanger = lastChanger;
    }

    public Date getLastChanged() {
        return lastChanged;
    }

    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    public MagiFileInfo(String name, MagiObjectType type, String lastChanger, Date lastChanged){
        this.name = name;
        this.magiObj = null;
        this.lastChanger = lastChanger;
        this.lastChanged = lastChanged;
        this.type = type;
    }

    @Override
    public void checkout(Path path) throws Exception{
        Path finalPath = path.toAbsolutePath().resolve(getName());
        if (getType().equals(MagiObjectType.BLOB)){
            loadMagiObj().checkout(finalPath);
        } else if (getType().equals(MagiObjectType.FOLDER)){
            Files.createDirectories(finalPath);
            loadMagiObj().checkout(finalPath);
        }
    }

    @Override
    public String getSha1Identifier() {
        MessageDigest sha1 = HashUtils.getSha1Digest();
        if (this.getName() != null){
            sha1.update(this.getName().getBytes(StandardCharsets.UTF_8));
        }
        if (this.getObjIdentifier() != null){
            sha1.update(this.getObjIdentifier().getBytes(StandardCharsets.UTF_8));
        }
        if (this.getLastChanger() != null){
            sha1.update(this.getLastChanger().getBytes(StandardCharsets.UTF_8));
        }
        if (this.getLastChanged() != null){
            sha1.update(this.getLastChanged().toString().getBytes(StandardCharsets.UTF_8));
        }
        if (this.getLastChanged() != null){
            sha1.update(this.getType().toString().getBytes(StandardCharsets.UTF_8));
        }
        return HashUtils.intoHex(sha1.digest());
    }


}
