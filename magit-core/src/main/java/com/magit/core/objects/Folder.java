package com.magit.core.objects;

import com.magit.common.utils.HashUtils;
import com.magit.core.utils.MagitFS;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Folder extends MagitObject implements Iterable<String> {
    private static Logger log = Logger.getLogger(Folder.class.getTypeName());

    private TreeMap<String, MagiFileInfo> files;
    private TreeMap<String, MagiFileInfo> folders;

    public Folder(){
        this.files = new TreeMap();
        this.folders = new TreeMap();
    }

    public void addFileOrFolder(String id, MagiFileInfo file) throws IllegalArgumentException {
        if (file == null){
            return;
        }
        if (file.getType().equals(MagiObjectType.BLOB)){
            addToMap(files, id, file);
        } else {
            addToMap(folders, id, file);
        }
    }
    private void addToMap(Map someMap, String id, MagiFileInfo fileInfo){
        if (fileInfo == null){
            return;
        }
        if (someMap.containsKey(id)){
            throw new IllegalArgumentException("Object already in Folder");
        }
        someMap.put(id, fileInfo);
    }

    public void addFileOrFolder(MagiFileInfo file) throws IllegalArgumentException {
        if (file == null || file.getObjIdentifier() == null){
            return;
        }
        addFileOrFolder(file.getObjIdentifier(), file);
    }

    @Override
    public MagitObject setFs(MagitFS fs){
        this.fs = fs;
        files.values().forEach(fileInfo -> fileInfo.setFs(fs));
        folders.values().forEach(fileInfo -> fileInfo.setFs(fs));
        return this;
    }

    public Set<String> getFolderNames(){
        return this.folders.values().stream().map(info -> info.getName()).collect(Collectors.toSet());
    }

    public TreeMap getUpdatedIdsToSha1(){
        TreeMap newFiles = new TreeMap();
        for (MagiFileInfo file : files.values()){
            newFiles.put(file.getObjIdentifier(), file);
        }
        for (MagiFileInfo file : folders.values()){
            newFiles.put(file.getObjIdentifier(), file);
        }
        return newFiles;
    }
    @Override
    public void save() throws Exception{
        fs.writeObject(this);
        for (MagiFileInfo info : files.values()){
            info.save();
        }
        for (MagiFileInfo info : folders.values()){
            info.save();
        }
    }
    public MagiFileInfo getFileOrFolder(String id) throws Exception{
        if (files.containsKey(id) && folders.containsKey(id)){
            log.log(Level.SEVERE, "id exists in both folder and file!!");
            throw new Exception("Id is indecisive file or folder (both)");
        }
        // get the key from the right map or null
        return files.getOrDefault(id, folders.getOrDefault(id, null));
    }

    public MagiFileInfo getFile(String id){
        return files.getOrDefault(id, null);
    }

    public MagiFileInfo getFolder(String id){
        return folders.getOrDefault(id, null);
    }
    public Set<String> getFileIds(){
        Set<String> keys = new HashSet<>(files.keySet());
        keys.addAll(folders.keySet());
        return keys;
    }
    public Set<String> getBlobIds(){
        return files.keySet();
    }
    public Set<String> getFoldeIds(){
        return folders.keySet();
    }

    public void replaceContent(String id, MagiFileInfo file) throws Exception{
        if (file == null){
            return;
        }
        if (file.getType().equals(MagiObjectType.BLOB)){
            files.replace(id, file);
        } else {
            folders.replace(id, file);
        }
    }

    public boolean removeFile(String name) throws Exception {
        if (getFileOrFolder(name) == null){
            return isEmpty();
        }
        files.remove(name);
        folders.remove(name);
        return isEmpty();
    }

    public boolean isEmpty(){ return files.size() == 0 && folders.size() == 0;}

    public TreeMap<String, MagiFileInfo> getFiles(){
        return getUpdatedIdsToSha1();
    }

    @Override
    public Iterator<String> iterator() {
        return getFileIds().iterator();
    }


    @Override
    public void checkout(Path path) throws Exception {
        for (Map.Entry<String, MagiFileInfo> entry: files.entrySet()){
            entry.getValue().checkout(path);
        }
        for (Map.Entry<String, MagiFileInfo> entry: folders.entrySet()){
            entry.getValue().checkout(path);
        }
    }

    @Override
    public String getSha1Identifier() {
        MessageDigest sha1 = HashUtils.getSha1Digest();
        TreeMap<String, MagiFileInfo> files = getUpdatedIdsToSha1();
        for (Map.Entry<String, MagiFileInfo> file : files.entrySet()){
            sha1.update(file.getValue().getSha1Identifier().getBytes(StandardCharsets.UTF_8));
        }
        return HashUtils.intoHex(sha1.digest());
    }

}
