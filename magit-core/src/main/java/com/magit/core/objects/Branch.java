package com.magit.core.objects;

import com.magit.common.utils.HashUtils;
import com.magit.core.utils.MagitFS;

import javax.management.RuntimeErrorException;
import javax.management.RuntimeOperationsException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.MessageDigest;

public class Branch extends MagitObject{
    private String name;
    private String commitPointed;
    private boolean isRemote;
    private String trackingAfter = null;

    public String getName() {
        return name;
    }

    public String getFullName(){
        if (!isRemote()){
            return getName();
        }
        try{
            return fs.readRemoteRepoName() + "\\" + getName();
        } catch (IOException e){throw new RuntimeException("failed to read remote repo's name");}
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getCommitPointed() {
        return commitPointed;
    }

    public void setCommitPointed(String commitPointed) {
        this.commitPointed = commitPointed;
    }

    public Commit getPointerCommit() throws Exception {
        if (commitPointed == null){
            return null;
        }
        Commit commit =  fs.readCommitObject(commitPointed);
        commit.setFs(fs);
        return commit;
    }
    public boolean isRemote() {
        return isRemote;
    }

    public void setRemote(boolean remote) {
        isRemote = remote;
    }

    public String getTrackingAfter() {
        return trackingAfter;
    }

    public boolean isTracking(){
        return trackingAfter != null;
    }

    public void setTrackingAfter(String trackingAfter) {
        this.trackingAfter = trackingAfter;
    }

    public Branch(String name, String commitPointed) {
        this.name = name;
        this.commitPointed = commitPointed;
    }

    @Override
    public void checkout(Path path) throws Exception {
        if (getCommitPointed() == null || getCommitPointed().isEmpty()){
            return;
        }
        Commit commit = fs.readCommitObject(getCommitPointed());
        commit.checkout(path);
    }

    @Override
    public String getSha1Identifier() {
        MessageDigest sha1 = HashUtils.getSha1Digest();
        sha1.update(name.getBytes(StandardCharsets.UTF_8));
        if (commitPointed != null){
            sha1.update(commitPointed.getBytes(StandardCharsets.UTF_8));
        }
        sha1.update(String.valueOf(isRemote()).getBytes(StandardCharsets.UTF_8));
        sha1.update(String.valueOf(getTrackingAfter()).getBytes(StandardCharsets.UTF_8));
        return HashUtils.intoHex(sha1.digest());
    }

    @Override
    public String toString(){
        return "Branch: " + getName() + " pointing to commit " + getCommitPointed();
    }

}
