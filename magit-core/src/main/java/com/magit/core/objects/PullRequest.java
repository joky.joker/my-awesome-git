package com.magit.core.objects;


import com.magit.common.utils.FileSystemFactory;

import java.nio.file.Path;
import java.util.Date;
import java.util.Objects;

public class PullRequest {
    public enum Status {OPEN, ACCEPTED, DENIED};
    private String initiator;
    private String targetUser;
    private String localRepositoryPath;
    private String remoteRepositoryPath;

    //respective to the remote repository P.O.V
    private String baseBranch;
    private String targetBranch;

    private Status status = Status.OPEN;
    private Date createionDate = new Date();
    private String message;
    private int id = 0;
    //on reject
    private String rejectionMessage;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Branch getBaseBranch() throws Exception {
        return getRemoteRepository().getFileSystem().readBranchObject(baseBranch);
    }

    public void setBaseBranch(String baseBranch) {
        this.baseBranch = baseBranch;
    }

    public void setBaseBranch(Branch baseBranch) {
        this.baseBranch = baseBranch.getName();
    }

    public Branch getTargetBranch() throws Exception {
        return getRemoteRepository().getFileSystem().readBranchObject(targetBranch);
    }

    public void setTargetBranch(String targetBranch) {
        this.targetBranch = targetBranch;
    }

    public void setTargetBranch(Branch targetBranch) {
        this.targetBranch = targetBranch.getName();
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(String targetUser) {
        this.targetUser = targetUser;
    }

    public LocalRepository getLocalRepository() throws Exception {
        return new LocalRepository(FileSystemFactory.toPath(localRepositoryPath));
    }

    public void setLocalRepository(LocalRepository localRepository) {
        this.localRepositoryPath = localRepository.getRepoDir().toString();
    }

    public void setLocalRepository(Path localRepository) {
        this.localRepositoryPath = localRepository.toString();
    }

    public Repository getRemoteRepository() throws Exception{
        return Repository.openRepository(FileSystemFactory.toPath(remoteRepositoryPath));
    }

    public void setRemoteRepository(Repository remoteRepository) throws Exception{
        this.remoteRepositoryPath = remoteRepository.getRepoDir().toString();
    }
    public void setRemoteRepository(Path remoteRepository){
        this.remoteRepositoryPath = remoteRepository.toString();
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getRejectionMessage() {
        return rejectionMessage;
    }

    public void setRejectionMessage(String rejectionMessage) {
        this.rejectionMessage = rejectionMessage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(initiator, targetUser, localRepositoryPath, remoteRepositoryPath, baseBranch, targetBranch, status, createionDate, message, id, rejectionMessage);
    }

    public Date getCreateionDate() {
        return createionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PullRequest that = (PullRequest) o;
        return id == that.id &&
                initiator.equals(that.initiator) &&
                Objects.equals(targetUser, that.targetUser) &&
                localRepositoryPath.equals(that.localRepositoryPath) &&
                remoteRepositoryPath.equals(that.remoteRepositoryPath) &&
                baseBranch.equals(that.baseBranch) &&
                targetBranch.equals(that.targetBranch) &&
                status == that.status &&
                createionDate.equals(that.createionDate) &&
                message.equals(that.message) &&
                Objects.equals(rejectionMessage, that.rejectionMessage);
    }
}
