package com.magit.core.objects;

import com.magit.core.interfaces.ISha1Hashable;
import com.magit.core.utils.MagitFS;

import java.nio.file.Path;

public abstract class MagitObject implements ISha1Hashable, Comparable {
    private static final String DIGEST_AlG = "SHA1";
    protected MagitFS fs;

    public enum MagiObjType {COMMIT, BLOB, FOLDER}
    public boolean deepEquals(MagitObject obj){
        return this.getSha1Identifier() == obj.getSha1Identifier();
    }

    public MagitObject setFs(MagitFS fs) {
        this.fs = fs;
        return this;
    }

    public Repository getParentRepository(){
        if (this.fs == null){
            return null;
        }
        return this.fs.getRepository();
    }

    public void save() throws Exception {
        fs.writeObject(this);
    }

    @Override
    public int compareTo(Object o) {
        if (this == o){ return 0;}
        if (o instanceof MagitObject){
            MagitObject other = (MagitObject) o;
            return this.getSha1Identifier().compareTo(other.getSha1Identifier());
        }
        return 1;
    }

    @Override
    public boolean equals(Object O){
        if (O instanceof  MagitObject){
            return ((MagitObject)O).getSha1Identifier().equals(getSha1Identifier());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getSha1Identifier().hashCode();
    }

    abstract public void checkout(Path path) throws Exception;

    public void checkout(MagitFS fs, Path path) throws Exception{
        if (fs != null){
            setFs(fs);
        }
        this.checkout(path);
    }
    abstract public String getSha1Identifier();
}
