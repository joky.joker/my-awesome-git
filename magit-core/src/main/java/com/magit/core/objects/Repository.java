package com.magit.core.objects;

import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.exceptions.RepositoryUninitializedException;
import com.magit.core.factories.StagingCommitFactory;
import com.magit.core.interfaces.ICommitter;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.interfaces.IMerger;
import com.magit.core.merge.MergeExecutor;
import com.magit.core.utils.MagitFS;
import com.magit.core.utils.PathUtils;
import de.jkeylockmanager.manager.KeyLockManager;
import de.jkeylockmanager.manager.KeyLockManagers;
import puk.team.course.magit.ancestor.finder.AncestorFinder;
import puk.team.course.magit.ancestor.finder.CommitRepresentative;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Repository {
    private static final KeyLockManager repoInternalsLockManager = KeyLockManagers.newLock();
    private static final Logger log = Logger.getLogger(Repository.class.getSimpleName());
    public static final String MAGIT_DIR_NAME = ".magit";
    public static final String MAGIT_OBJECTS_DIR_NAME = "objects";
    public static final String MAGIT_BRANCHES_DIR_NAME = "branches";
    public static final String MAGIT_MASTER_BRANCH_NAME = "master";
    protected MagitFS fsInstance;
    protected String name;
    protected String activeBranch;
    protected Path repoPath;

    public Repository(Path repoPath){
        this.repoPath = repoPath;
    }

    private void validateInitilized() throws RepositoryUninitializedException {
        try{
            if (!isInitialized()){
                throw new RepositoryUninitializedException("Cant perform that action when repository is not initizlied");
            }
        } catch (IOException e){
            log.log(Level.SEVERE, "Error: failed to write data", e);
        }
    }
    public static Repository openRepository(Path repoPath) throws Exception {
        return repoInternalsLockManager.executeLocked(repoPath, () -> {
            try{
                Repository currentRepo = new Repository(repoPath);
                MagitFS fileSystem = currentRepo.getFileSystem();
                currentRepo.activeBranch = fileSystem.readHead();
                currentRepo.name = fileSystem.readRepoName();
                return currentRepo;
            } catch (Exception e){
                throw new RuntimeException(e);
            }
        });
    }

    public void checkout(String branch, Path checkoutPath) throws Exception {
        repoInternalsLockManager.executeLocked(this.getRepoDir(), () -> {
            try{
                PathUtils.cleanFolder(checkoutPath, MAGIT_DIR_NAME);
                MagitFS fileSystem = this.getFileSystem();
                Branch chosenBranch = fileSystem.readLocalBranchObject(branch);
                chosenBranch.checkout(this.getFileSystem(), checkoutPath);
            } catch (Exception e){
                throw new RuntimeException(e);
            }
        });
    }

    public boolean isInitialized() throws IOException {
        return getFileSystem().isInitizlied();
    }

    public void checkout(String branch) throws Exception {
        checkout(branch, getRepoDir());
    }
    public void checkout() throws Exception {checkout(getActiveBranch());}

    public String getActiveBranch(){
        return repoInternalsLockManager.executeLocked(this.getRepoDir(), () -> {
            try{
                if (activeBranch == null){
                    try {
                        activeBranch = getFileSystem().readHead();
                    }catch (IOException e){
                        log.log(Level.SEVERE, "cant read head branch", e);
                    }
                }
                return activeBranch;
            } catch (Exception e){
                throw new RuntimeException(e);
            }
        });
    }

    public Branch getCurrentBranch() throws Exception {
        return repoInternalsLockManager.executeLocked(this.getRepoDir(), () -> {
            try{
                return getFileSystem().readLocalBranchObject(getActiveBranch());
            } catch (Exception e){
                throw new RuntimeException(e);
            }
        });
    }

    public void setActiveBranch(String activeBranch) throws Exception{
        repoInternalsLockManager.executeLocked(this.getRepoDir(), () -> {
            try{
                if (!getFileSystem().isBranchExist(activeBranch)){
                    throw new Exception("Branch name " + activeBranch + " does not exist");
                }
                this.activeBranch = activeBranch;
                getFileSystem().writeHead(activeBranch);
            } catch (Exception e){
                throw new RuntimeException(e);
            }
        });
    }

    public Path getRepoDir(){
        return repoPath.toAbsolutePath();
    }

    public Path getRepoGitDir(){
        return getRepoDir().resolve(MAGIT_DIR_NAME);
    }

    public String getName() {
        if (name == null) {
            name = getFileSystem().readRepoName();
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
        try{
            getFileSystem().writeRepoName(name);
        } catch (IOException e){
            log.log(Level.WARNING, "failed to write Repos name", e);
        }
    }

    public Path getObjectDir(){
        return getRepoGitDir().resolve(MAGIT_OBJECTS_DIR_NAME);
    }
    public Path getBranchDir(){
        return getRepoGitDir().resolve(MAGIT_BRANCHES_DIR_NAME);
    }

    public MagitFS getFileSystem(){
        if (fsInstance == null){
            fsInstance = new MagitFS(this);
        }
        return fsInstance;
    }


    public static Repository createRepository(Path repoPath, String repoName) throws IOException{

        //create .magit folder
        Path magitFolder = repoPath.resolve(MAGIT_DIR_NAME);
        Files.createDirectories(magitFolder);

        //create objects
        Files.createDirectory(magitFolder.resolve(MAGIT_OBJECTS_DIR_NAME));
        //branches
        Files.createDirectory(magitFolder.resolve(MAGIT_BRANCHES_DIR_NAME));
        Repository repo = new Repository(repoPath);

        Branch master = new Branch(MAGIT_MASTER_BRANCH_NAME, "");
        MagitFS fileSystem = repo.getFileSystem();
        fileSystem.writeObject(master);
        fileSystem.writeHead(MAGIT_MASTER_BRANCH_NAME);
        fileSystem.writeRepoName(repoName);
        return repo;
    }

    public IFolderComparer getChanges() throws Exception {
        Path tempDir = FileSystemFactory.createTempDirectory("magit");
        this.checkout(this.getActiveBranch(), tempDir);
        IFolderComparer comp = StagingCommitFactory.createFolderComparer();
        comp.compare(this.getRepoDir(), tempDir);
        FileUtilsHelper.deleteFileRec(tempDir);
        return comp;
    }

    public boolean isCleanState() throws Exception{
        return !getChanges().hasChanges();
    }

    public void commitStagedChanges(String message, String user) throws Exception{
        IFolderComparer comparer = getChanges();
        ICommitter stagger = StagingCommitFactory.createStagingCommitter();
        stagger.commitChanges(comparer, null,this, message, user);
    }

    public void commitStagedChanges(String message, String user, String secondParent, boolean force) throws Exception{
        IFolderComparer comparer = getChanges();
        ICommitter stagger = StagingCommitFactory.createStagingCommitter();
        stagger.commitChanges(comparer, secondParent,this, message, user, force);
    }

    public void commitStagedChanges(String message, String user, String secondParent) throws Exception{
        commitStagedChanges(message, user, secondParent, false);
    }

    public void forceCommitStagedChanges(String message, String user, String secondParent) throws Exception{
        commitStagedChanges(message, user, secondParent, true);
    }

    public Commit getCommonAncestor(Commit firstCommit, Commit secondCommit){
        MagitFS fs = this.getFileSystem();
        //made anonymous class so other packages importing magit-core wont need AncestorFinder as well.
        AncestorFinder finder = new AncestorFinder(string -> new CommitRepresentative() {
            @Override
            public String getSha1() {return string;}

            private String toEmptyIfNull(String str){
                if (str == null){
                    return "";
                }
                return str;
            }
            @Override
            public String getFirstPrecedingSha1() {
                return toEmptyIfNull(fs.readCommitBySha1Silent(string).getParent());
            }
            @Override
            public String getSecondPrecedingSha1() {
                return toEmptyIfNull(fs.readCommitBySha1Silent(string).getSecondParent());
            }
        });
        String commitSha1 = finder.traceAncestor(firstCommit.getSha1Identifier(), secondCommit.getSha1Identifier());
        try{
            return fs.readCommitObject(commitSha1);
        } catch (Exception e){
            log.severe("Ancestor commit not found! should never happen");
            return null;
        }
    }

    public MergeExecutor mergeHeadToBranch(String otherBranch) throws Exception {
        IMerger merger = StagingCommitFactory.createMerger();
        Commit headCommit = getCurrentBranch().getPointerCommit();
        Commit otherCommit = getFileSystem().readBranchObject(otherBranch).getPointerCommit();
        Commit baseCommit = getCommonAncestor(headCommit, otherCommit);
        merger.fromCommits(this, headCommit,otherCommit, baseCommit);
        return merger.merge();
    }

    public String getRepoUser(){
        return getRepoDir().getParent().getFileName().toString();
    }

}
