package com.magit.core.staging;

import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.objects.*;
import org.apache.commons.io.FileUtils;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.logging.Logger;

public class RawHierarchyCreator {
    private static Logger logger = Logger.getLogger(RawHierarchyCreator.class.getTypeName());
    String author;
    Date changeDate;
    Repository repo;

    public RawHierarchyCreator(Repository repo, String author, Date changeDate){
        this.author = author;
        this.changeDate = changeDate;
        this.repo = repo;
    }

    private MagiObjectType getTypeFromPath(Path path){
        if (Files.isRegularFile(path)){
            return MagiObjectType.BLOB;
        }
        if (Files.isDirectory(path)){
            return MagiObjectType.FOLDER;
        }
        return null;
    }

    public MagiFileInfo createMagiFromPath(Path item) throws IOException {
        if (!Files.exists(item)){
            logger.severe("file name does not exist " + item.toString());
            return null;
        }
        MagiObjectType type = getTypeFromPath(item);
        MagiFileInfo fileInfo = new MagiFileInfo(item.getFileName().toString(), type, author, changeDate);
        if (type.equals(MagiObjectType.BLOB)){
            fileInfo.setMagiObj(createBlobFromRaw(item));
        } else if (type.equals(MagiObjectType.FOLDER)){
            fileInfo.setMagiObj(createFolderFromRaw(item));
        } else {
            throw new IOException("file/folder does not exist" + item.toString());
        }
        fileInfo.setFs(repo.getFileSystem());
        return fileInfo;
    }

    public Folder createFolderFromRaw(Path folderPath) throws IOException {
        if (!Files.isDirectory(folderPath)){
            logger.severe("directory does not exist " + folderPath.toString());
            return null;
        }
        Folder folder = new Folder();
        for (Path file : FileUtilsHelper.listFilesRelative(folderPath)){
            if (file.getFileName().toString().equals(Repository.MAGIT_DIR_NAME)){
                continue;
            }
            folder.addFileOrFolder(createMagiFromPath(folderPath.resolve(file)));
        }
        if (folder.isEmpty()){
            return null;
        }
        folder.setFs(repo.getFileSystem());
        return folder;
    }

    public Blob createBlobFromRaw(Path file) throws IOException {
        if (!Files.isRegularFile(file)){
            logger.severe("file name does not exist " + file.toString());
            return null;
        }
        String content = new String(Files.readAllBytes(file), StandardCharsets.UTF_8);
        return (Blob) new Blob(content).setFs(repo.getFileSystem());
    }
}
