package com.magit.core.staging;

import com.magit.common.utils.HashUtils;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.objects.Repository;
import org.apache.commons.collections.CollectionUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class SimpleFolderComparer implements IFolderComparer {
    Set<Path> added;
    Set<Path> modified;
    Set<Path> removed;

    public static Set<Path> listFiles(Path path) throws Exception {
        int nameCount = path.getNameCount();
        return Files.walk(path)
                .filter(Files::isRegularFile)
                .filter(file -> !file.getName(nameCount).toString().equals(Repository.MAGIT_DIR_NAME))
                .map(path::relativize)
                .collect(Collectors.toSet());
    }

    @Override
    public void compare(Path master, Path slave) throws Exception {
        Set<Path> masterFiles = listFiles(master);
        Set<Path> slaveFiles =  listFiles(slave);
        added = new HashSet<>();
        modified = new HashSet<>();
        removed = new HashSet<>();

        added.addAll(CollectionUtils.subtract(masterFiles, slaveFiles));
        removed.addAll(CollectionUtils.subtract(slaveFiles, masterFiles));

        CollectionUtils.intersection(masterFiles, slaveFiles).forEach(commonFile ->{
            Path file = (Path)commonFile;
            Path masterFile = master.resolve(file);
            Path slaveFile = slave.resolve(file);
            try{
                if (HashUtils.getFileDigest(masterFile).equals(HashUtils.getFileDigest(slaveFile))){
                    return;
                }
                modified.add(file);
            } catch (IOException e){}
        });
    }

    @Override
    public Set<Path> getNewFiles() {
        return added;
    }

    @Override
    public Set<Path> getRemoved() {
        return removed;
    }

    @Override
    public Set<Path> getModified() {
        return modified;
    }

    @Override
    public boolean isNew(Path path) {
        return added.contains(path.normalize());
    }

    @Override
    public boolean isDeleted(Path path) {
        return removed.contains(path.normalize());
    }

    @Override
    public boolean isModified(Path path) {
        return modified.contains(path.normalize());
    }

    @Override
    public boolean hasChanges() {
        return !added.isEmpty() || !modified.isEmpty() || !removed.isEmpty();
    }
}
