package com.magit.core.staging;

import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.interfaces.ICommitter;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.objects.*;
import com.magit.core.utils.PathUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidParameterException;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommitStagerImpl implements ICommitter {
    private static Logger log = Logger.getLogger(CommitStagerImpl.class.getName());
    private IFolderComparer comparer;
    private Repository repo;
    private String username;
    private Date changeDate;
    private RawHierarchyCreator creator;

    public Commit commitChanges(IFolderComparer comparer, String secondParent, Repository repo, String message, String user) throws Exception{
        return commitChanges(comparer, secondParent, repo, message, user, false);
    }

    @Override
    public Commit commitChanges(IFolderComparer comparer, String secondParent, Repository repo, String message, String user, boolean forceCommit) throws Exception{
        this.comparer = comparer;
        this.repo = repo;
        this.username = user;
        this.changeDate = new Date();
        this.creator = new RawHierarchyCreator(repo, username, changeDate);

        if (!forceCommit && !comparer.hasChanges()){
            throw new InvalidParameterException("No changes detected, skipping commit...");
        }
        Commit currentCommit = getCurrentCommit();

        Commit newCommit = commitChanges(currentCommit, message);
        newCommit.setSecondParent(secondParent);
        newCommit.save();
        Branch branch = repo.getCurrentBranch();
        branch.setCommitPointed(newCommit.getSha1Identifier());
        branch.save();
        return newCommit;
    }

    private Commit getCurrentCommit() throws Exception{
        if (!repo.isInitialized()){
            return null;
        }
        return repo.getCurrentBranch().getPointerCommit();
    }

    private Folder getCommitEffectiveRootFolder(Commit currentCommit) throws Exception{
        Folder rootFolder = currentCommit.getRootFolder();
        return (Folder)rootFolder.getFiles().firstEntry().getValue().loadMagiObj();
    }

    private Commit commitChanges(Commit currentCommit, String message) throws Exception {
        Commit newCommit = new Commit();
        newCommit.setFs(repo.getFileSystem());
        Folder realRootFolder;

        if (currentCommit == null) {
            realRootFolder = new Folder();
            log.log(Level.INFO, "making first commit");
        } else {
            realRootFolder = getCommitEffectiveRootFolder(currentCommit);
        }

        MagiFileInfo newRootFolderInfo = generateFolderCommit(realRootFolder, FileSystemFactory.getFs().getPath("."));
        newRootFolderInfo.save();
        Folder fakeRootFolder = new Folder();
        fakeRootFolder.setFs(repo.getFileSystem());
        fakeRootFolder.addFileOrFolder(newRootFolderInfo);
        fakeRootFolder.save();
        if (currentCommit != null){
            newCommit.setParent(currentCommit.getSha1Identifier());
        }
        newCommit.setRootFolder(fakeRootFolder);
        newCommit.setAuthor(username);
        newCommit.setDateOfCreation(changeDate);
        newCommit.setMessage(message);
        return newCommit;
    }

    private MagiFileInfo handleBlobFile(MagiFileInfo blob, Path self) throws Exception{
        if (comparer.isDeleted(self)){
            log.info("commiting deleted file at " + self.toString());
            return null;
        }
        if (comparer.isModified(self)){
            log.info("commiting modified file at " + self.toString());
            return creator.createMagiFromPath(getFullPath(self));
        }
        return blob;
    }

    private Folder handleModifiedAndRemovedItems(Folder folder, Path selfPath) throws Exception{
        Folder newFolder = new Folder();
        newFolder.setFs(repo.getFileSystem());
        if (folder == null){
            return newFolder;
        }
        //handle all the modified/removed/neither files
        for(Map.Entry<String, MagiFileInfo> file: folder.getFiles().entrySet()){
            MagiFileInfo currentItem = file.getValue();
            Path itemPath = selfPath.resolve(currentItem.getName());

            if (currentItem.getType().equals(MagiObjectType.BLOB)){
                MagiFileInfo info = handleBlobFile(currentItem, itemPath);
                newFolder.addFileOrFolder(info);
                continue;
            }

            //if its a folder, we make a recursive call
            if (currentItem.getType().equals(MagiObjectType.FOLDER)){
                Folder currentFolder = (Folder) currentItem.loadMagiObj();
                MagiFileInfo currentFolderInfo = generateFolderCommit(currentFolder, itemPath);
                newFolder.addFileOrFolder(currentFolderInfo);
                continue;
            }
        }

        return newFolder;
    }
    private Path getFullPath(Path path){
        return repo.getRepoDir().toAbsolutePath().resolve(path);
    }

    private void handleNewItemsInFolder(Folder newFolder, Path selfPath, Set<String> handledFolders) throws Exception{
        //handle new files and dirs
        Path absFolderPath = repo.getRepoDir().toAbsolutePath().resolve(selfPath);
        if (!Files.isDirectory(absFolderPath)){
            return;
        }

        for (Path file : FileUtilsHelper.listFilesRelative(absFolderPath)){
            String filename = file.toString();
            Path itemPath = selfPath.resolve(filename);
            Path fullPath = getFullPath(itemPath);

            //if this is a directory, and we haven't handled it already - it might contain new file
            // therefore we need to recursively check it
            if (Files.isDirectory(fullPath) && !handledFolders.contains(filename)){
                log.info("Checking unknown directory " + itemPath.toString());
                newFolder.addFileOrFolder(generateFolderCommit(null, itemPath));
                continue;
            }

            if (!comparer.isNew(itemPath)){
                log.info("skipping non new file " + itemPath.toString());
                continue;
            }
            MagiFileInfo fileInfo = creator.createMagiFromPath(getFullPath(itemPath));
            if (fileInfo != null){
                log.info("commiting new item at " + itemPath.toString());
            }
            newFolder.addFileOrFolder(fileInfo);
        }
    }

    private MagiFileInfo generateFolderCommit(Folder folder, Path selfPath) throws Exception {
        Folder newFolder = handleModifiedAndRemovedItems(folder, selfPath);
        Set<String> handledFolderNames = new HashSet<>();
        if (folder != null){
            handledFolderNames = folder.getFolderNames();
        }
        handleNewItemsInFolder(newFolder, selfPath, handledFolderNames);
        if (newFolder.isEmpty()){
            return null;
        }

        log.info("commiting folder at " + selfPath.toString());
        MagiFileInfo folderInfo = new MagiFileInfo(selfPath.getFileName().toString(), MagiObjectType.FOLDER, username, changeDate);
        folderInfo.setMagiObj(newFolder);
        folderInfo.setFs(this.repo.getFileSystem());
        newFolder.save();
        return folderInfo;
    }

}
