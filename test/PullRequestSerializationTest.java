import com.magit.common.utils.FileSystemFactory;
import com.magit.core.objects.PullRequest;
import com.magit.core.objects.Repository;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class PullRequestSerializationTest {

    @Test
    public void writeReadPR() throws Exception{
        Repository repo = Repository.createRepository(FileSystemFactory.createTempDirectory(""), "some name");
        ArrayList<PullRequest> someList = new ArrayList<>();
        PullRequest pullRequest = new PullRequest();
        pullRequest.setInitiator("initiator");
        pullRequest.setBaseBranch("baseRemoteBranch");
        pullRequest.setTargetBranch("targetRemoteBranch");
        pullRequest.setLocalRepository(repo.getRepoDir());
        pullRequest.setRemoteRepository(FileSystemFactory.toPath("remoteRepo"));
        pullRequest.setMessage("message");
        someList.add(pullRequest);
        repo.getFileSystem().writePullRequests(someList);

        List<PullRequest> readPrs = repo.getFileSystem().readPullRequests();
        Assert.assertEquals(readPrs.get(0), someList.get(0));
        Assert.assertArrayEquals(someList.toArray(), readPrs.toArray());
    }
}
