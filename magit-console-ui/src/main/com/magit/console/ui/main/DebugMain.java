package com.magit.console.ui.main;

import com.magit.console.ui.ConsoleUI;
import com.magit.console.ui.factories.ConsoleUIFactory;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DebugMain {
    public static void main(String[] args){
        Logger.getGlobal().setLevel(Level.ALL);
        Logger.getGlobal().addHandler(new ConsoleHandler());
        ConsoleUI ui = ConsoleUIFactory.createConsoleUI();
        try{
            ui.startApplication();
        } catch (Throwable e){
            Logger.getGlobal().log(Level.SEVERE, "dayum", e);
        }
        System.out.println("Thank you for using Magit :)!");
        System.out.println("Bye..");
    }
}
