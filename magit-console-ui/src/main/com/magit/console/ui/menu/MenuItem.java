package com.magit.console.ui.menu;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;

public class MenuItem {
    private String title;
    private MenuItemRunner consumer;
    public MenuItem(String title, MenuItemRunner consumer){
        this.title = title;
        this.consumer = consumer;
    }

    public String getTitle(){
        return title;
    }

    public boolean onChosen(IUserInterfaceListener userInterfaceListener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception{
        return this.consumer.run(userInterfaceListener, provider, scanner);
    }
}
