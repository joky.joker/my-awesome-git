package com.magit.console.ui.menu;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;

public abstract class MenuItemRunner {
    public abstract boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception;
}
