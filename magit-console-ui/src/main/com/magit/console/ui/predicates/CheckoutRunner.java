package com.magit.console.ui.predicates;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;

public class CheckoutRunner extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        if (provider.isChangesPending()){
            System.out.println("There are pending changes, do you wish to commit them first? [Y\\n]");
            if (scanner.getYesNoInput()){
                CommitRunner runner = new CommitRunner();
                runner.run(listener, provider, scanner);
            }
        }
        System.out.println("Please provide a branch name to checkout to: ");
        String branchName = scanner.readLine().trim();
        listener.onSetHeadBranchCheckout(branchName);
        return true;
    }
}
