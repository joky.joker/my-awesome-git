package com.magit.console.ui.predicates;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;

public class CommitRunner extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        System.out.println("Please provide with a commit message: ");
        String message = scanner.readLine().trim();
        listener.onCommit(message);
        return true;
    }
}
