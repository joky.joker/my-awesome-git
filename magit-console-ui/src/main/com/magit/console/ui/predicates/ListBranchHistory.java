package com.magit.console.ui.predicates;

import com.magit.common.utils.DefaultDateFormatterFactory;
import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Commit;

import java.util.List;

public class ListBranchHistory extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        List<Commit> commitHistory = provider.getCommitHistory(provider.getHeadBranch().getCommitPointed());
        for (Commit commit: commitHistory){
            System.out.println("[" + commit.getSha1Identifier() + "] ");
            System.out.println("\tMessage: " + commit.getMessage());
            System.out.println("\tCreation Date: " + DefaultDateFormatterFactory.createDateFormatter().format(commit.getDateOfCreation()));
            System.out.println("\tCreated By: " + commit.getAuthor());
        }
        return true;
    }
}
