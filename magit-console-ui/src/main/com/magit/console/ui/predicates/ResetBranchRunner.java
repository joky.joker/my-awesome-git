package com.magit.console.ui.predicates;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;

public class ResetBranchRunner extends MenuItemRunner {

    public static boolean validateSha1(String sha1){
        return sha1.matches("[a-fA-F0-9]{40}");
    }

    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        if (provider.isChangesPending()){
            System.out.println("There are pending changes in the system, do you wish to continue? [Y\\n]");
            if (!scanner.getYesNoInput()){
                return true;
            }
        }
        System.out.println("Please enter the sha1 of the new commit to point to: ");
        String sha1 = scanner.readLine().trim();
        if (!validateSha1(sha1)){
            System.err.println("invalid sha1 received");
            return true;
        }
        String head = provider.getHead();
        listener.overrideBranchCommit(head, sha1);
        listener.onSetHeadBranchCheckout(head);
        return true;
    }
}
