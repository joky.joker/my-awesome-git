package com.magit.console.ui.predicates;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Branch;
import javafx.util.Pair;

import java.util.Comparator;
import java.util.List;

public class ListBranchesRunner extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        List<Pair<Branch, String>> branchInfo = provider.getBranchsInformation();
        branchInfo.sort(Comparator.comparing(p -> p.getKey().getName()));
        String head = provider.getHead();
        for (Pair<Branch, String> pair : branchInfo){
            Branch branch = pair.getKey();
            String message = pair.getValue();
            String name = branch.getName();
            if (head.equals(name)){
                name = "* " + name;
            }
            System.out.println(name + " [" + branch.getCommitPointed() + "] " + message);
        }
        return true;
    }
}
