package com.magit.console.ui.predicates;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;

import java.nio.file.Path;
import java.util.Set;

public class ShowStatusRunner extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        Set<Path> newFiles = provider.getNewUnstagedFiles();
        System.out.println("New Files: ");
        printSet(newFiles);
        System.out.println("Deleted Files: ");
        printSet(provider.getRemovedUnstagedFiles());
        System.out.println("Modified Files: ");
        printSet(provider.getModifiedUnstagedFiles());
        return true;
    }

    private void printSet(Set<Path> files){
        files.forEach(filename ->{
            System.out.println("\t" + filename.toAbsolutePath().toString());
        });
    }
}
