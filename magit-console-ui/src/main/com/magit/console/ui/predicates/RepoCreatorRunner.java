package com.magit.console.ui.predicates;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Repository;

import java.io.File;

public class RepoCreatorRunner extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        System.out.println("Please provide a full path to the new repository location");
        String path = scanner.readLine().trim();
        if (new File(path).exists()){
            System.out.println("'" + path + "' already exists, cant init repo");
            return true;
        }
        System.out.println("Please provide with a repository name: ");
        String repoName = scanner.readLine();
        Repository repo = Repository.createRepository(new File(path).toPath(), repoName);
        listener.onRepoOpened(path);
        return true;
    }
}
