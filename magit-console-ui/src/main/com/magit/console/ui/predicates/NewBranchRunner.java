package com.magit.console.ui.predicates;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;

public class NewBranchRunner extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        System.out.println("Please provide a new Branch name: ");
        String name = scanner.readLine().trim();
        if (provider.doesBranchExist(name)){
            System.err.println("Failed to create branch, Branch name already exists");
            return true;
        }
        listener.onNewLocalBranchCreated(name, provider.getHeadBranch().getCommitPointed());
        if (provider.isChangesPending()){
            System.out.println("Cannot checkout because there are pending changes");
            return true;
        }
        System.out.println("Do you wish to checkout the newly created Branch? [Y\\n]");
        if (!scanner.getYesNoInput()){
            return true;
        }
        listener.onSetHeadBranchCheckout(name);
        return true;
    }
}
