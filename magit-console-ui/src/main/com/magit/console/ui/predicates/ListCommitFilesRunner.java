package com.magit.console.ui.predicates;

import com.magit.common.utils.DefaultDateFormatterFactory;
import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.MagiFileInfo;
import javafx.util.Pair;

import java.util.List;
import java.util.Map;

public class ListCommitFilesRunner extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        Map<String, MagiFileInfo> commitItems = provider.getCommitItemDetails();
        if (commitItems.isEmpty()){
            System.out.println("Repository has no commits");
            return true;
        }
        for (Map.Entry<String, MagiFileInfo> pair : commitItems.entrySet()){
            MagiFileInfo fileInfo = pair.getValue();
            System.out.println("Item " + fileInfo.getObjIdentifier());
            System.out.println("\tType: " + fileInfo.getType().toString());
            System.out.println("\tName: " + fileInfo.getName());
            System.out.println("\tPath: " + pair.getKey());
            System.out.println("\tLast changer: " + fileInfo.getLastChanger());
            System.out.println("\tLast changed: " + DefaultDateFormatterFactory.createDateFormatter().format(fileInfo.getLastChanged()));
        }
        return true;
    }
}
