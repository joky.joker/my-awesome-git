package com.magit.console.ui.predicates;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.exceptions.RepositoryUninitializedException;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;

import java.nio.file.Path;

public class RepoXmlLoaderRunner extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception{
        System.out.println("Please provide with full path to the xml file: ");
        String path = scanner.readLine().trim();
        Path repoPath = listener.onRepoXmlLoaded(path);
        if (repoPath.toFile().isDirectory()){
            System.out.println("Path given in the xml already exists, do you wish to delete directory? [Y\\n]");
            if (!scanner.getYesNoInput()){
                listener.onRepoOpened(repoPath.toString());
                return true;
            }
        }
        listener.onXmlDump();
        String head = provider.getHead();
        System.out.println("trying to checkout head branch [" + head + "]");
        try {
            listener.onSetHeadBranchCheckout(head);
        } catch (RepositoryUninitializedException e){
            System.err.println("failed to checkout, repository is empty");
        }
        return true;
    }
}
