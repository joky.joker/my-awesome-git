package com.magit.console.ui.predicates;

import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItemRunner;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;
import com.magit.core.objects.Repository;

import java.io.File;

public class SwitchRepoMenuRunner extends MenuItemRunner {
    @Override
    public boolean run(IUserInterfaceListener listener, IInformationProvider provider, ConsoleInputInterface scanner) throws Exception {
        System.out.println("Please provide with full path to the new Repository");
        String fullPath = scanner.readLine().trim();
        if (!new File(fullPath, Repository.MAGIT_DIR_NAME).isDirectory()){
            System.err.println("Failed to find " + Repository.MAGIT_DIR_NAME + " directory in repo path");
            return true;
        }
        listener.onRepoOpened(fullPath);
        return true;
    }
}
