package com.magit.console.ui.factories;

import com.magit.console.ui.ConsoleUI;
import com.magit.console.ui.menu.MenuItem;
import com.magit.console.ui.predicates.*;
import com.magit.core.controllers.Controller;

public class ConsoleUIFactory {
    public static ConsoleUI createConsoleUI(){
        Controller controller = new Controller();
        ConsoleUI userInterface = new ConsoleUI(controller);
        userInterface.setListener(controller);
        userInterface.addItem(new MenuItem("Update username", new UpdateUsernameRunner()));
        userInterface.addItem(new MenuItem("Load Repository from XML", new RepoXmlLoaderRunner()));
        userInterface.addItem(new MenuItem("Open Repo by location", new SwitchRepoMenuRunner()));
        userInterface.addItem(new MenuItem("List commit files", new ListCommitFilesRunner()));
        userInterface.addItem(new MenuItem("Show Status", new ShowStatusRunner()));
        userInterface.addItem(new MenuItem("Commit changes", new CommitRunner()));
        userInterface.addItem(new MenuItem("List Branches", new ListBranchesRunner()));
        userInterface.addItem(new MenuItem("[BONUS +5] Create new Branch", new NewBranchRunner()));
        userInterface.addItem(new MenuItem("Delete Branch", new DeleteBranchRunner()));
        userInterface.addItem(new MenuItem("Checkout Branch", new CheckoutRunner()));
        userInterface.addItem(new MenuItem("List Branch History", new ListBranchHistory()));
        userInterface.addItem(new MenuItem("[BONUS +5] Create new Repository", new RepoCreatorRunner()));
        userInterface.addItem(new MenuItem("[BONUS +10] Set Branch to specific commit", new ResetBranchRunner()));

        userInterface.addItem(new MenuItem("Exit", new ExitRunner()));
        return userInterface;
    }
}
