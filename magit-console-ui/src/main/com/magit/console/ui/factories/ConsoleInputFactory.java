package com.magit.console.ui.factories;

import com.magit.console.ui.input.ConsoleInputImpl;
import com.magit.console.ui.interfaces.ConsoleInputInterface;

public class ConsoleInputFactory {
    public static ConsoleInputInterface createConsoleInput(){
        return new ConsoleInputImpl();
    }
}
