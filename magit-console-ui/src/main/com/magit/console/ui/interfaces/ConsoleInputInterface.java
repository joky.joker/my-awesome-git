package com.magit.console.ui.interfaces;

public interface ConsoleInputInterface extends AutoCloseable {
    boolean getYesNoInput();
    String readLine();
    int readInt();
}
