package com.magit.console.ui.input;

import com.magit.console.ui.interfaces.ConsoleInputInterface;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ConsoleInputImpl implements ConsoleInputInterface, AutoCloseable {
    Scanner scanner;
    public ConsoleInputImpl(){
        scanner = new Scanner(System.in);
    }

    @Override
    public boolean getYesNoInput() {
        String input = readLine().trim();
        if (input.length() == 0){
            return true;
        }
        return input.charAt(0) != 'n';
    }

    @Override
    public String readLine() {
        while (true){
            try{
                return scanner.nextLine();
            } catch(NoSuchElementException e){
                System.err.println("Invalid input, please enter again: ");
            }
        }
    }

    @Override
    public int readInt() {
        while (true){
            try{
                int result = scanner.nextInt();
                scanner.nextLine();
                return result;
            } catch(InputMismatchException e){
                System.err.println("Invalid number, integer is required: ");
                scanner.nextLine();
            }
        }
    }

    @Override
    public void close() throws Exception {
        scanner.close();
    }
}
