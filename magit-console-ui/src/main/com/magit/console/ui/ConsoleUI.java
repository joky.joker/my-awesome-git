package com.magit.console.ui;

import com.magit.console.ui.factories.ConsoleInputFactory;
import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.menu.MenuItem;
import com.magit.core.interfaces.IInformationProvider;
import com.magit.core.interfaces.IUserInterfaceListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsoleUI {
    private static Logger logger = Logger.getLogger(ConsoleUI.class.getTypeName());
    private IInformationProvider provider;
    private IUserInterfaceListener listener;
    private ArrayList<MenuItem> menuItems;

    public ConsoleUI(IInformationProvider prov){
        provider = prov;
        menuItems = new ArrayList<MenuItem>();
    }

    public IUserInterfaceListener getListener() {
        return listener;
    }

    public void setListener(IUserInterfaceListener listener) {
        this.listener = listener;
    }

    public void addItem(MenuItem item){
        menuItems.add(item);
    }

    private static String getStringOrDefault(String str, String def){
        if (str == null){
            return def;
        }
        return str;
    }
    public void showTitle(){
        System.out.print("Magit Menu\t\t");
        System.out.print("Current User: " + provider.getCurrentUsername() + "\t");
        String location = getStringOrDefault(provider.getCurrentRepositoryLocation(), "N/A");
        String name = getStringOrDefault(provider.getRepoName(), "N/A");
        System.out.print("Repository name: " + name + "\t");
        System.out.println("Repository location: " + location);
        System.out.println();
    }
    public void showMenu(){
        for (int i = 0; i < menuItems.size(); i++){
            System.out.println( i + " " + menuItems.get(i).getTitle());
        }
    }

    public void startApplication(){
        ConsoleInputInterface inputInterface = ConsoleInputFactory.createConsoleInput();
        boolean keepMenu = true;
        while (keepMenu){
            MenuItem menuItem = getUserInputForMenu(inputInterface);
            try{
                keepMenu = menuItem.onChosen(listener, provider, inputInterface);
            } catch (Exception e){
                logger.log(Level.SEVERE, "some error", e);
                System.err.println("Error: " + e.getMessage());
            }
            System.out.println();
        }
        try{
            inputInterface.close();
        } catch (Exception e){}
    }

    public MenuItem getUserInputForMenu(ConsoleInputInterface inputInterface){
        showTitle();
        showMenu();
        System.out.println("Please type a menu item number from above: ");
        int input = inputInterface.readInt();
        while (input < 0 || input >= menuItems.size()){
            System.out.println("Invalid menu item, please write a valid menu item number");
            input = inputInterface.readInt();
        }
        return menuItems.get(input);
    }
    private static void clearScreen(){
        try{
            final String os = System.getProperty("os.name");
            if (os.contains("Windows"))
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                Runtime.getRuntime().exec("clear");
        } catch (InterruptedException | IOException e){
            //ignore
        }
    }
}
