import com.magit.console.ui.interfaces.ConsoleInputInterface;
import com.magit.console.ui.predicates.RepoXmlLoaderRunner;
import com.magit.core.controllers.Controller;
import com.magit.core.factories.StagingCommitFactory;
import com.magit.core.interfaces.IFolderComparer;
import com.sun.xml.internal.ws.util.ByteArrayBuffer;
import org.junit.Assert;
import org.junit.Test;

import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LoadRepoXMLConsoleTest {

    public static String largeXmlPath = "E:\\Amit\\Dropbox\\מטלות\\Java\\תרגילי הקורס + חומרים רלבנטים תצוגת תיקיית קבצים\\תרגיל מתגלגל\\קבצי בדיקה\\EX 1\\large\\ex1-large.xml";
    public static String mediumXmlPath = "E:\\Amit\\Dropbox\\מטלות\\Java\\תרגילי הקורס + חומרים רלבנטים תצוגת תיקיית קבצים\\תרגיל מתגלגל\\קבצי בדיקה\\EX 1\\medium\\ex1-medium.xml";
    public static String smallXmlPath = "E:\\Amit\\Dropbox\\מטלות\\Java\\תרגילי הקורס + חומרים רלבנטים תצוגת תיקיית קבצים\\תרגיל מתגלגל\\קבצי בדיקה\\EX 1\\small\\ex1-small.xml";
    public Controller controller = null;

    public void loadXmlAndOverride(String xmlPath, boolean overrider, String expectedString) throws Exception {
        ConsoleInputInterface inputInterface = mock(ConsoleInputInterface.class);
        RepoXmlLoaderRunner repoXmlLoader = new RepoXmlLoaderRunner();
        when(inputInterface.readLine()).thenReturn(xmlPath);
        when(inputInterface.getYesNoInput()).thenReturn(overrider);
        OutputStream os = new ByteArrayBuffer();
        System.setOut(new PrintStream(os));
        repoXmlLoader.run(controller, controller, inputInterface);
        Assert.assertEquals(os.toString(), expectedString);
    }

    public void loadXmlAndVerify(Path repoPath, String xmlLocation, Path compareToPath, String head) throws Exception{
        controller = new Controller();
        loadXmlAndOverride(xmlLocation, true, "Please provide with full path to the xml file: \r\n" +
                "Path given in the xml already exists, do you wish to delete directory? [Y\\n]\r\n" +
                "trying to checkout head branch [$HEADNAME$]\r\n".replace("$HEADNAME$", head));
        IFolderComparer comparer = StagingCommitFactory.createFolderComparer();
        comparer.compare(repoPath, compareToPath);
        Assert.assertTrue(!comparer.hasChanges());

        controller.onNewLocalBranchCreated("testtest", controller.getHeadBranch().getCommitPointed());
        controller.onSetHeadBranchCheckout("testtest");

        controller = new Controller();
        loadXmlAndOverride(xmlLocation, false, "Please provide with full path to the xml file: \r\n" +
                "Path given in the xml already exists, do you wish to delete directory? [Y\\n]\r\n");

        comparer = StagingCommitFactory.createFolderComparer();
        comparer.compare(repoPath, compareToPath);
        Assert.assertTrue(!comparer.hasChanges());
        Assert.assertEquals(controller.getHead(), "testtest");
    }

    @Test
    public void loadXmlLargeOverride() throws Exception{
        loadXmlAndVerify(Paths.get("C:\\repo1"),
                largeXmlPath,
                Paths.get("E:\\Amit\\workspace\\Magit\\magit-core\\test-resources\\large\\large-deep-branch"),
                "deep");
    }

    @Test
    public void loadXmlMediumOverride() throws Exception{
        loadXmlAndVerify(Paths.get("C:\\repo1"),
                mediumXmlPath,
                Paths.get("E:\\Amit\\workspace\\Magit\\magit-core\\test-resources\\medium\\ex1-medium-master-branch"),
                "master");

    }

    @Test
    public void loadXmlSmallOverride() throws Exception {
        loadXmlAndVerify(Paths.get("C:\\repo1"),
                smallXmlPath,
                Paths.get("E:\\Amit\\workspace\\Magit\\magit-core\\test-resources\\small\\ex1-smal-master-branch"),
                "master");
    }
}
