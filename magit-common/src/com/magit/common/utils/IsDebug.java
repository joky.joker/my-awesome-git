package com.magit.common.utils;

import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class IsDebug {
    private static Logger log = Logger.getLogger("IsDebug");
    private static boolean isDebug = false;
    static {
        try {
            Socket sock = new Socket();
            sock.connect(new InetSocketAddress(InetAddress.getLoopbackAddress(),8181), 100);
            isDebug = true;
        } catch (IOException e) {
            //log.log(Level.SEVERE, "isDebug", e);
        }

        if (!isDebug){
            LogManager.getLogManager().reset();
            Logger.getGlobal().setLevel(Level.OFF);
        }
        log.info("is debug environment set to: " + isDebug);
    }
    public static boolean isDebugEnvironment(){
        return isDebug;
    }
}
