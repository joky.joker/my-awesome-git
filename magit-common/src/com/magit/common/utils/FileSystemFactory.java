package com.magit.common.utils;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileSystemFactory {
    public enum Type{VIRTUAL, REAL}
    private static Type currentType = Type.REAL;
    private static FileSystem fs;

    public static void setCurrentType(Type type) {
        currentType = type;
    }

    public static FileSystem getFs(){
        switch (currentType) {
            case REAL:
                return FileSystems.getDefault();
            case VIRTUAL:
                if (fs == null){
                    fs = Jimfs.newFileSystem(Configuration.windows());
                }
                return fs;
        }
        return null;
    }
    public static Path toPath(String name) {
        return getFs().getPath(name);
    }

    public static Path createTempDirectory(String prefix) throws IOException {
        switch (currentType) {
            case REAL:
                return Files.createTempDirectory(prefix);
            case VIRTUAL:
                if (fs == null){
                    fs = Jimfs.newFileSystem(Configuration.windows());
                }
                Path tempDir = fs.getPath("C:\\temp123");
                Files.createDirectories(tempDir);
                Path tempDirWithPrefix = tempDir.resolve(prefix + System.nanoTime());
                Files.createDirectories(tempDirWithPrefix);
                return tempDirWithPrefix;
        }
        return null;
    }
}
