package com.magit.common.utils;

import java.text.SimpleDateFormat;

public class DefaultDateFormatterFactory {
    public static SimpleDateFormat createDateFormatter(){
        return new SimpleDateFormat("dd.MM.yyyy-HH:mm:ss:SSS");
    }
}
