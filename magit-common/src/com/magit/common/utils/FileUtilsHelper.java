package com.magit.common.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtilsHelper {
    private static Logger log = Logger.getLogger(FileUtilsHelper.class.getName());

    public static void copyDirectoryContent(Path srcDirectory, Path destDirectory, boolean override) throws IOException {
        if (!Files.isDirectory(srcDirectory) || !Files.isDirectory(destDirectory)){
            throw new IOException("directory is non existing: " + srcDirectory.toString() + " : " + destDirectory.toString());
        }
        try(Stream<Path> files = Files.list(srcDirectory)){
            Set filesSet = files.map(srcDirectory::relativize).collect(Collectors.toSet());
            copyFiles(srcDirectory, destDirectory, filesSet ,override);
        }

    }

    public static void copyDirectoryContent(Path srcDirectory, Path destDirectory) throws IOException {
        copyDirectoryContent(srcDirectory, destDirectory, false);
    }

    public static Path copyDirRecursively(Path srcDir, Path destDir) throws Exception{
        /*doing some non generic, ugly things here to make the copy work over
            different providers, e.g JimFs and Windows
         */
        if (Files.isRegularFile(srcDir)){
            Path destPath = FileSystemFactory.toPath(destDir + "\\" +srcDir.getFileName().toString());
            Files.copy(srcDir, destPath);
            return destPath;
        }
        Files.walk(srcDir)
                .filter(Files::isRegularFile)
                .map(srcDir::relativize)
                .forEach(relativePath -> {
                    try{
                        Path resolvedDest = FileSystemFactory.toPath(destDir.toAbsolutePath().toString() + "\\" + relativePath.toString());
                        Files.createDirectories(resolvedDest.getParent());
                        Logger.getGlobal().info("copying from " + srcDir.resolve(relativePath).toString() + " -> " +
                                resolvedDest.toString());
                        Logger.getGlobal().info("relative path: " + relativePath.toString());
                        Files.copy(srcDir.resolve(relativePath), resolvedDest);
                    } catch (IOException e){
                        throw new RuntimeException(e);
                    }
                });
        return destDir;
    }

    public static Set<Path> listFilesRecNoHidden(Path srcDir) throws IOException {
        Set<Path> paths = new HashSet<>();
        Files.walk(srcDir)
                .filter(Files::isRegularFile)
                .map(srcDir::relativize)
                .forEach(relativePath -> {
                    if (relativePath.toString().contains(".magit")){
                        return;
                    }
                    paths.add(relativePath);
                });
        return paths;
    }

    public static void copyFiles(Path srcDir, Path destDir, Set<Path> names, boolean override) throws IOException{
        if (!Files.isDirectory(srcDir) || !Files.isDirectory(destDir)){
            throw new IOException("directory is non existing: " + srcDir.toString() + " : " + destDir.toString());
        }

        try(Stream<Path> srcFiles = Files.list(srcDir)){
            srcFiles.forEachOrdered(srcFile ->{
                Path destFile = destDir.resolve(srcFile.getFileName());
                if (!override &&Files.exists(destFile)){
                    return;
                }
                if (!names.contains(destFile.getFileName())){
                    return;
                }
                try{
                    Files.copy(srcFile, destFile);
                } catch (IOException e){
                    log.log(Level.SEVERE, "should never happen!", e);
                }
            });
        }
    }
    public static Set<Path> listFilesRelative(Path dir) throws IOException{
        try(Stream<Path> files = Files.list(dir)){
            return files.map(dir::relativize).collect(Collectors.toSet());
        }
    }

    public static void deleteFileRec(Path file) {
        try{
            if (Files.isDirectory(file)){
                Files.walk(file)
                        .sorted(Comparator.reverseOrder())
                        .forEachOrdered(internalFile -> {
                            try{
                                Files.delete(internalFile);
                            } catch (IOException e){
                                throw new RuntimeException(e);
                            }
                        });
            }
            Files.deleteIfExists(file);
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }


    public static boolean isDirEmpty(Path path) throws IOException {
        if (!Files.isDirectory(path)){
            return false;
        }
        return listFilesRelative(path).size() == 0;
    }
    public static<T> Set<T> mergeLists(List<T> list1, List<T> list2){
        HashSet<T> set = new HashSet<>(list1);
        set.addAll(list2);
        return set;
    }
}
