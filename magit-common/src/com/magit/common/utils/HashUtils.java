package com.magit.common.utils;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HashUtils {
    private final static Logger log = Logger.getLogger(HashUtils.class.getSimpleName());

    public static String intoHex(byte[] bytes){
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public static String sha1(byte[] data){
        MessageDigest sha1 = getSha1Digest();
        sha1.update(data);
        return intoHex(sha1.digest());
    }

    public static String sha1(String content){
        return sha1(content.getBytes(StandardCharsets.UTF_8));
    }

    public static MessageDigest getSha1Digest(){
        try{
            return MessageDigest.getInstance("SHA1");
        } catch (Exception e){
            log.severe("this system does not support sha1");
            return null;
        }
    }

    public static String getFileDigest(Path path) throws IOException {
        byte[] data = Files.readAllBytes(path);
        return sha1(data);
    }

    public static <T extends Serializable> byte[] serialize(T obj){
        try(ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            out.flush();
            return bos.toByteArray();
        } catch (IOException e){
            log.log(Level.FINE, "failed to serialize object of type " + obj.getClass().getSimpleName(), e);
        }
        return null;
    }
}
