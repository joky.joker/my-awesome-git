package web.synced;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.objects.LocalRepository;
import com.magit.core.objects.Repository;
import de.jkeylockmanager.manager.KeyLockManager;
import de.jkeylockmanager.manager.KeyLockManagers;
import web.general.MagitContextListener;
import web.messages.Message;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class User {
    @Expose
    @SerializedName("username")
    private String name;
    // used whenever reading or modifying user directory content
    private static final KeyLockManager userInternalsLockManager = KeyLockManagers.newLock();
    private static final KeyLockManager messagesLockManager = KeyLockManagers.newLock();
    private static ConcurrentHashMap<String, User> usersMap = new ConcurrentHashMap<>();

    static {
        synchronized (usersMap){
            try{
                for (Path user : FileUtilsHelper.listFilesRelative(MagitContextListener.getRootPath())){
                    usersMap.put(user.getFileName().toString(), new User(user.getFileName().toString()));
                }
            } catch (Exception e){

            }
        }
    }
    private User(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static User getUser(String name){
        synchronized (usersMap){
            if (!usersMap.containsKey(name)){
                usersMap.put(name, new User(name));
                try{
                    Files.createDirectories(getUserDirectory(name));
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
        return usersMap.get(name);
    }

    public List<Repository> listRepositories(){
        return userInternalsLockManager.executeLocked(this.name, () ->{
            try {
                ArrayList<Repository> repositories = new ArrayList<>();
                for (Path repoPath : FileUtilsHelper.listFilesRelative(getUserDirectory())){
                    Path fullPath = getUserDirectory().resolve(repoPath);
                    if (!Files.isDirectory(fullPath)){
                        continue;
                    }
                    repositories.add(Repository.openRepository(fullPath));
                }
                return repositories;
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });
    }

    public Repository getUserRepo(String name) throws Exception{
        Repository repo = Repository.openRepository(getUserDirectory().resolve(name));
        if (repo.getFileSystem().isRemote()){
            return new LocalRepository(repo);
        }
        return repo;
    }

    public LocalRepository getUserRepoAsLocal(String name) throws Exception {
        Repository repo = Repository.openRepository(getUserDirectory().resolve(name));
        if (repo.getFileSystem().isRemote()){
            return new LocalRepository(repo);
        }
        return null;
    }

    private static Path getUserDirectory(String name){
        return FileSystemFactory.toPath(MagitContextListener.REPO_ROOT_PATH).resolve(name);
    }

    public Path getUserDirectory(){
        return getUserDirectory(this.name);
    }

    public static List<String> listUsers(){
        return usersMap.values().stream().map(User::getName).collect(Collectors.toList());
    }

    public List<String> loadUserMessages() {
        return messagesLockManager.executeLocked(this.name, () ->{
            List<String> lines = new ArrayList<>();
            Path messagesFiles = getUserDirectory().resolve("messages.txt");
            if (!Files.isRegularFile(messagesFiles)){
                return lines;
            }
            try{
                lines = Files.readAllLines(messagesFiles);
                Files.delete(messagesFiles);
                return lines;
            } catch (Exception e){
                throw new RuntimeException(e);
            }
        });
    }

    public <T> void addMessage(Message<T> message) {
        messagesLockManager.executeLocked(this.name, () -> {
            Path messagesFiles = getUserDirectory().resolve("messages.txt");
            String msgStr = message.toString() + "\n";
            try{
                if (!Files.isRegularFile(messagesFiles)){
                    Files.createDirectories(messagesFiles.getParent());
                    Files.createFile(messagesFiles);
                }
                Files.write(messagesFiles, msgStr.getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
            } catch (Exception e){
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });
    }

}
