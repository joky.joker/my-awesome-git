package web.servlet.colab;

import com.google.gson.JsonObject;
import com.magit.core.objects.LocalRepository;
import com.magit.core.objects.Repository;
import web.general.MagitLoginManager;
import web.messages.Message;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet({"/fork"})
public class ForkServlet  extends JsonResponseServlet {
    private static final String USERNAME_PARAM = "username";
    private static final String REPO_PARAM = "repo";

    public ForkServlet(){
        this.init(USERNAME_PARAM, REPO_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User currentUser = (User) req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
        String remoteUserName = req.getParameter(USERNAME_PARAM);
        User remoteUser = User.getUser(remoteUserName);
        String repoName = req.getParameter(REPO_PARAM);
        Repository remoteRepo = remoteUser.getUserRepo(repoName);
        LocalRepository localRepository = LocalRepository.clone(remoteRepo.getName(),
                currentUser.getUserDirectory().resolve(remoteRepo.getName()),
                remoteRepo.getRepoDir());
        JsonObject result = new JsonObject();
        remoteUser.addMessage(new Message<String>(currentUser.getName() + " has forked " + repoName));
        result.addProperty("status", "success");
        return result;
    }
}
