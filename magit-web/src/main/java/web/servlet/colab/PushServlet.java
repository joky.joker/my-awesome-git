package web.servlet.colab;

import com.magit.core.objects.Branch;
import com.magit.core.objects.LocalRepository;
import web.servlet.JsonResponseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet({"/colab/push"})
public class PushServlet extends JsonResponseServlet {
    private static final String REPO_NAME = "repo";
    public PushServlet(){
        this.init(REPO_NAME);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        LocalRepository repo = getUserSession(req).getUserRepoAsLocal(req.getParameter(REPO_NAME));
        if (repo == null){
            throw new Exception("Repository is not forked, cant push");
        }
        Branch headBranch = repo.getCurrentBranch();
        if (headBranch.isTracking()){
            repo.push();
        } else {
            repo.pushLocalBranch(headBranch);
        }
        return null;
    }
}
