package web.servlet.branch;

import com.magit.core.objects.Branch;
import com.magit.core.objects.Repository;
import com.magit.core.utils.MagitFS;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet({"/branch/create"})
public class CreateLocalBranchServlet extends BranchActionServlet {
    public static final String SHA1_PARAM = "sha1";

    public CreateLocalBranchServlet(){
        this.init(SHA1_PARAM, REPO_NAME, BRANCH_NAME);
    }

    @Override
    public void handleOperation(HttpServletRequest req, Repository repo, String branchName) throws Exception {
        String branchSha1 = req.getParameter(SHA1_PARAM);
        MagitFS fs = repo.getFileSystem();
        try{
            fs.readCommitObject(branchSha1);
        } catch (Exception e) {
            throw new Exception("commit with sha1 '" + branchSha1 + "' does not exist");
        }
        Branch branch = new Branch(branchName, branchSha1);
        branch.setFs(fs);
        branch.save();
    }
}
