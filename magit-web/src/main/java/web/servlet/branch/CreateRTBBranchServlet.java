package web.servlet.branch;

import com.magit.core.objects.Branch;
import com.magit.core.objects.Repository;
import com.magit.core.utils.MagitFS;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet({"/branch/createRTB"})
public class CreateRTBBranchServlet extends BranchActionServlet {

    @Override
    public void handleOperation(HttpServletRequest req, Repository repo, String remoteBranchStr) throws Exception {
        MagitFS fs = repo.getFileSystem();
        Branch remoteBranch = fs.readBranchObject(remoteBranchStr);
        if (fs.isBranchExist(remoteBranch.getName())){
            throw new Exception("Branch Already Exist");
        }
        Branch rtb = new Branch(remoteBranch.getName(), remoteBranch.getCommitPointed());
        rtb.setFs(fs);
        rtb.setTrackingAfter(remoteBranch.getFullName());
        rtb.save();
    }
}
