package web.servlet.branch;

import com.magit.core.objects.Repository;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BranchActionServlet extends JsonResponseServlet {
    protected static final String REPO_NAME = "repo";
    protected static final String BRANCH_NAME = "name";

    public BranchActionServlet(){
        this.init(REPO_NAME, BRANCH_NAME);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User user = getUserSession(req);
        Repository repo = user.getUserRepo(req.getParameter(REPO_NAME));
        handleOperation(req, repo, req.getParameter(BRANCH_NAME));
        return null;
    }

    public abstract void handleOperation(HttpServletRequest req, Repository repo, String branchName) throws Exception;
}
