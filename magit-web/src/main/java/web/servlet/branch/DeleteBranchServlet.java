package web.servlet.branch;

import com.magit.core.objects.Branch;
import com.magit.core.objects.LocalRepository;
import com.magit.core.objects.Repository;
import web.general.MagitLoginManager;
import web.messages.Message;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet({"/branch/delete"})
public class DeleteBranchServlet extends BranchActionServlet {
    @Override
    public void handleOperation(HttpServletRequest req, Repository repo, String branchName) throws Exception{
        if (repo.getActiveBranch().equals(branchName)){
            throw new Exception("Cant delete head branch!");
        }
        Branch branch = repo.getFileSystem().readBranchObject(branchName);
        if (branch.isRemote()){
            throw new Exception("Cant delete remote branch, can delete its RTB if you will (Bonus)");
        }
        if (branch.isTracking()){
            User currentUser = (User)req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
            Branch remote = repo.getFileSystem().readBranchObject(branch.getTrackingAfter());
            LocalRepository local = new LocalRepository(repo);
            Repository remoteRepo  = Repository.openRepository(local.getRemoteRepoLocation());
            remoteRepo.getFileSystem().removeLocalBranch(remote.getName());
            local.getFileSystem().removeLocalBranch(remote.getFullName());
            User remoteUser = User.getUser(remoteRepo.getRepoUser());
            String message = String.format("user '%s' has deleted the branch '%s'", currentUser.getName(), branchName);
            remoteUser.addMessage(new Message<String>(message));
        }
        repo.getFileSystem().removeLocalBranch(branchName);
    }
}
