package web.servlet.branch;

import com.google.gson.JsonObject;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Repository;
import web.servlet.JsonResponseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet({"/checkout"})
public class CheckoutServlet extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";
    private static final String NEW_HEAD = "new_head";
    public CheckoutServlet(){
        this.init(REPO_PARAM, NEW_HEAD);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        Repository repo = this.getUserSession(req).getUserRepo(req.getParameter(REPO_PARAM));
        if (!repo.isCleanState()){
            JsonObject result = new JsonObject();
            result.addProperty("status", "cant checkout, pending changes in WC");
            return result;
        }
        Branch newHead = repo.getFileSystem().readBranchObject(req.getParameter(NEW_HEAD));
        if (newHead.isRemote()){
            JsonObject result = new JsonObject();
            result.addProperty("status", "cant checkout to remote branch");
            return result;
        }
        repo.setActiveBranch(req.getParameter(NEW_HEAD));
        repo.checkout(req.getParameter(NEW_HEAD));
        return null;
    }
}
