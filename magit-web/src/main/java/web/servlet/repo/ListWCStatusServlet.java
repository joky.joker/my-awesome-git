package web.servlet.repo;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.objects.Repository;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.Path;
import java.util.Set;

@WebServlet({"/repo/wc_status"})
public class ListWCStatusServlet extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";

    public ListWCStatusServlet() {
        this.init(REPO_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User currentUser = getUserSession(req);
        Repository repo = getUserSession(req).getUserRepo(req.getParameter(REPO_PARAM));
        IFolderComparer comparer = repo.getChanges();
        JsonObject response = new JsonObject();
        response.addProperty("status", "success");
        response.addProperty("isCleanState", repo.isCleanState());
        response.add("new", intoStringList(comparer.getNewFiles()));
        response.add("deleted", intoStringList(comparer.getRemoved()));
        response.add("modified", intoStringList(comparer.getModified()));
        return response;
    }

    private JsonArray intoStringList(Set<Path> pathlist){
        JsonArray result =  new JsonArray();
        for (Path path : pathlist){
            result.add(path.toString());
        }
        return result;
    }
}
