package web.servlet.repo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;
import com.magit.core.merge.FileStateProvider;
import com.magit.core.objects.Commit;
import com.magit.core.objects.Repository;
import web.general.MagitLoginManager;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.Path;

@WebServlet({"/list_wc"})
public class ReadCommitFileContentServlet extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";

    public ReadCommitFileContentServlet(){
        this.init(REPO_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        JsonObject response = new JsonObject();
        String repoName = req.getParameter(REPO_PARAM);
        User user = (User) req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
        Path repoDir = user.getUserDirectory().resolve(repoName);
        JsonArray fileNameArr = new JsonArray();
        for (Path path : FileUtilsHelper.listFilesRecNoHidden(repoDir)){
            fileNameArr.add(path.toString());
        }
        response.addProperty("status", "success");
        response.add("files", fileNameArr);
        return response;
    }
}
