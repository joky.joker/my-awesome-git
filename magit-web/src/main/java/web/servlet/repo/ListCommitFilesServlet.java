package web.servlet.repo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.magit.core.controllers.CommitItemDetailsGenerator;
import com.magit.core.objects.Commit;
import com.magit.core.objects.Repository;
import web.general.MagitLoginManager;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@WebServlet({"/list_commit_files"})
public class ListCommitFilesServlet extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";
    private static final String SHA1_PARAM = "sha1";

    public ListCommitFilesServlet(){
        this.init(REPO_PARAM, SHA1_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        JsonObject response = new JsonObject();
        String repoName = req.getParameter(REPO_PARAM);
        String sha1 = req.getParameter(SHA1_PARAM);
        User user = (User) req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
        Repository repo = user.getUserRepo(repoName);
        Commit commit = repo.getFileSystem().readCommitObject(sha1);
        CommitItemDetailsGenerator generator = new CommitItemDetailsGenerator(repo);
        Set<String> files = generator.generateFilesDetails(commit).keySet();
        JsonArray arr = new JsonArray();
        for (String file: files){
            arr.add(file);
        }
        response.add("files", arr);
        response.addProperty("status", "success");
        return response;
    }
}
