package web.servlet.repo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.magit.core.objects.Branch;
import com.magit.core.objects.LocalRepository;
import com.magit.core.objects.Repository;
import web.servlet.JsonResponseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//TODO: delete?
@WebServlet({"/repo/remote_branches"})
public class ListRemoteBranchesServlet extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";

    public ListRemoteBranchesServlet(){
        this.init(REPO_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        LocalRepository repo = getUserSession(req).getUserRepoAsLocal(req.getParameter(REPO_PARAM));
        JsonArray array = new JsonArray();
        for (Branch branch: repo.getFileSystem().listRemoteBranches()){
            JsonObject branchDesc = new JsonObject();
            branchDesc.addProperty("name", branch.getFullName());
            branchDesc.addProperty("sha1", branch.getSha1Identifier());
            array.add(branchDesc);
        }
        return array;
    }
}
