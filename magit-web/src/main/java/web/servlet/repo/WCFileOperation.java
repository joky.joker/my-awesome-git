package web.servlet.repo;


import com.google.gson.JsonObject;
import web.general.MagitLoginManager;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.Path;

@WebServlet({"/mod"})
public class WCFileOperation extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";
    private static final String PATH_PARAM = "filepath";
    private static final String OPERATION_PARAM = "operation";

    public WCFileOperation(){
        this.init(REPO_PARAM, PATH_PARAM, OPERATION_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String operation = req.getParameter(OPERATION_PARAM);
        User user = (User) req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
        String repoName = req.getParameter(REPO_PARAM);
        String filePath = req.getParameter(PATH_PARAM);
        Path repoPath = user.getUserDirectory().resolve(repoName);
        Path fullFilePath = repoPath.resolve(filePath);
        JsonObject response = new JsonObject();
        response.addProperty("filepath", filePath);
        if (operation.equals("read")){
            response.addProperty("content", new String(Files.readAllBytes(fullFilePath), StandardCharsets.UTF_8));
        } else if (operation.equals("write")){
            Files.createDirectories(fullFilePath.getParent());
            Files.write(fullFilePath, req.getParameter("content").getBytes(StandardCharsets.UTF_8));
        } else if (operation.equals("delete")){
            Path currentPath = fullFilePath;
            try {
                while (currentPath != null){
                    Files.delete(currentPath);
                    currentPath = currentPath.getParent();
                }
            } catch (DirectoryNotEmptyException e){}
        }
        response.addProperty("status", "success");
        return response;
    }

}
