package web.servlet.repo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.magit.core.controllers.CommitGraphGenerator;
import com.magit.core.objects.Branch;
import com.magit.core.objects.Commit;
import com.magit.core.objects.LocalRepository;
import com.magit.core.objects.Repository;
import org.apache.commons.collections.comparators.ComparatorChain;
import web.general.DateTimeFormatterFactory;
import web.general.JsonSerializerUtil;
import web.general.MagitLoginManager;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;

@WebServlet({"/repo_metadata"})
public class RepoMetaDataServlet extends JsonResponseServlet {
    private static final String REPO_PARAM_NAME = "repo";
    public RepoMetaDataServlet(){
        this.init(REPO_PARAM_NAME);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        JsonObject response = new JsonObject();
        String repoName = req.getParameter(REPO_PARAM_NAME);
        User user = (User) req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
        Repository repo = user.getUserRepo(repoName);
        response.add("branches", getBranches(repo));
        response.add("commit_history", getCommitHistory(repo));
        response.addProperty("head", repo.getActiveBranch());
        if (repo instanceof LocalRepository){
            response.addProperty("remote_repo_name", ((LocalRepository) repo).getRemoteRepoName());
            String username = extractUserNameFromRepoPath(((LocalRepository) repo).getRemoteRepoLocation());
            response.addProperty("remote_repo_user", username);
        }
        response.addProperty("is_clean_state", repo.isCleanState());
        response.addProperty("status", "success");
        return response;
    }

    public String extractUserNameFromRepoPath(Path path){
        return path.getParent().getFileName().toString();
    }
    public JsonArray getBranches(Repository repo) throws Exception{
        JsonArray branches = new JsonArray();
        for (Branch branch : repo.getFileSystem().listAllBranches()){
            JsonObject branchObj = new JsonObject();
            branchObj.addProperty("name", branch.getFullName());
            branchObj.addProperty("sha1", branch.getCommitPointed());
            branchObj.addProperty("is_rb", branch.isRemote());
            branchObj.addProperty("is_rtb", branch.isTracking());
            branches.add(branchObj);
        }
        return branches;
    }

    public JsonArray getCommitHistory(Repository repo) throws Exception{
        JsonArray commitHistory = new JsonArray();
        ArrayDeque<Commit> cmtQueue = new ArrayDeque<>();
        Set<Commit> parents = CommitGraphGenerator.getAncestors(repo.getCurrentBranch().getPointerCommit());
        Map<String, List<String>> commitMap = getCommitBranchMap(repo);
        List<Commit> sortedParents = new ArrayList<>(parents);
        sortedParents.sort(new ComparatorChain(Comparator.comparing(Commit::getDateOfCreation), true));
        for (Commit commit : sortedParents){
            commitHistory.add(generateFromCommit(commit, commitMap));
        }
        return commitHistory;
    }

    public Map<String, List<String>> getCommitBranchMap(Repository repo) throws Exception {
        Map<String, List<String>> map = new HashMap<>();
        for (Branch branch: repo.getFileSystem().listAllBranches()){
            String commitId = branch.getCommitPointed();
            if (!map.containsKey(commitId)){
                map.put(commitId, new ArrayList<>());
            }
            map.get(commitId).add(branch.getFullName());
        }
        return map;
    }

    private JsonObject generateFromCommit(Commit commit, Map<String, List<String>> commitMap){
        JsonObject commitObj = new JsonObject();
        commitObj.addProperty("sha1", commit.getSha1Identifier());
        commitObj.addProperty("message", commit.getMessage());
        commitObj.addProperty("creator", commit.getAuthor());
        SimpleDateFormat format = DateTimeFormatterFactory.create();
        commitObj.addProperty("date", format.format(commit.getDateOfCreation()));


        List<String> arr = commitMap.getOrDefault(commit.getSha1Identifier(), new ArrayList<>());
        JsonArray branches = new JsonArray();
        for (String branchName : arr){
            branches.add(branchName);
        }
        commitObj.add("branches", branches);
        return commitObj;
    }
}
