package web.servlet.repo;

import com.google.gson.JsonObject;
import com.magit.core.objects.Repository;
import web.general.MagitLoginManager;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet({"/commit"})
public class CommitServlet  extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";
    private static final String MESSAGE_PARAM = "message";
    public CommitServlet(){
        this.init(REPO_PARAM, MESSAGE_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        JsonObject response = new JsonObject();
        String repoPath = req.getParameter(REPO_PARAM);
        User user = (User) req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
        Repository repo = user.getUserRepo(repoPath);
        repo.commitStagedChanges(req.getParameter(MESSAGE_PARAM), user.getName());
        response.addProperty("status", "success");
        return response;
    }
}
