package web.servlet.repo;

import com.google.gson.JsonObject;
import com.magit.common.utils.FileSystemFactory;
import com.magit.core.merge.FileStateProvider;
import com.magit.core.objects.Commit;
import com.magit.core.objects.Repository;
import web.general.MagitLoginManager;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet({"/read_commit_file_content"})
public class ListWcFilesServlet extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";
    private static final String SHA1_PARAM = "sha1";
    private static final String PATH_PARAM = "filepath";

    public ListWcFilesServlet(){
        this.init(REPO_PARAM, SHA1_PARAM, PATH_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        JsonObject response = new JsonObject();
        String repoName = req.getParameter(REPO_PARAM);
        String sha1 = req.getParameter(SHA1_PARAM);
        User user = (User) req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
        Repository repo = user.getUserRepo(repoName);
        Commit commit = repo.getFileSystem().readCommitObject(sha1);
        FileStateProvider provider = new FileStateProvider(commit);
        String content = provider.getContent(FileSystemFactory.toPath(req.getParameter(PATH_PARAM)));
        response.addProperty("status", "success");
        response.addProperty("content", content);
        return response;
    }
}

