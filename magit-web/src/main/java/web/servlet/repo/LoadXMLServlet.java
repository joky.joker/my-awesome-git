package web.servlet.repo;

import com.google.gson.JsonObject;
import com.magit.common.utils.StreamHelper;
import com.magit.core.objects.Repository;
import com.magit.core.serializers.RepoXmlLoader;
import web.general.MagitContextListener;
import web.general.MagitLoginManager;
import web.synced.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;

@WebServlet("/load_xml")
@MultipartConfig
public class LoadXMLServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParts().size() != 1){
            returnError(resp, "Invalid number of files uploaded");
        }
        RepoXmlLoader loader = new RepoXmlLoader();
        try {
            User currentUser = (User)req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
            for (Part part: req.getParts()){
                String xml = StreamHelper.intoString(part.getInputStream());
                loader.loadRepoFromString(xml);
                boolean override = Boolean.parseBoolean(req.getParameter("override"));
                if (Files.isDirectory(currentUser.getUserDirectory().resolve(loader.getRepoName())) && !override){
                    JsonObject obj = new JsonObject();
                    obj.addProperty("status", "success");
                    obj.addProperty("already_exists", true);
                    writePlainString(resp, obj.toString());
                    return;
                }
                Repository repo = loader.dumpRepoByName(currentUser.getUserDirectory());
                repo.checkout();
            }
            returnString(resp, "success");
        } catch (Exception e){
            returnError(resp, e.getMessage());
        }
    }
    private void returnError(HttpServletResponse resp, String error) throws IOException{
        returnString(resp, String.format("Error: %s", error));
        throw new RuntimeException(error);
    }

    private void writePlainString(HttpServletResponse response, String str) throws IOException{
        try(Writer writer = response.getWriter()){
            writer.write(str);
        }
    }
    private void returnString(HttpServletResponse resp, String string) throws IOException{
        try(Writer writer = resp.getWriter()){
            writer.write(String.format("{\"status\" : \"%s\"}", string));
        }
    }
}
