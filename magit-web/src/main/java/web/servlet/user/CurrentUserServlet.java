package web.servlet.user;

import com.google.gson.Gson;
import web.general.JsonSerializerUtil;
import web.general.MagitLoginManager;
import web.synced.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet({"/current_user"})
public class CurrentUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        User user = (User) req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
        PrintWriter writer = resp.getWriter();
        writer.write(JsonSerializerUtil.getGsonBuilder().toJson(user));
        writer.close();
    }
}
