package web.servlet.user;

import com.google.gson.JsonObject;
import com.magit.core.objects.Commit;
import com.magit.core.objects.Repository;
import org.apache.commons.collections.comparators.ComparatorChain;
import web.general.DateTimeFormatterFactory;
import web.general.JsonSerializerUtil;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;

@WebServlet({"/user_repos"})
public class ListUserReposServlet extends JsonResponseServlet {
    private static String USERNAME_PARAM = "username";
    public ListUserReposServlet(){
        this.init(USERNAME_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String username = req.getParameter(USERNAME_PARAM);
        ArrayList<JsonObject> repositories = new ArrayList<>();
        for (Repository repo : User.getUser(username).listRepositories()){
            JsonObject repoDesc = new JsonObject();
            //TODO: change the commit info to be the last commit and not the head commit
            Commit headCommit = repo.getCurrentBranch().getPointerCommit();
            SimpleDateFormat format = DateTimeFormatterFactory.create();
            repoDesc.addProperty("owner", username);
            repoDesc.addProperty("is_local", !repo.getFileSystem().isRemote());
            repoDesc.addProperty("name", repo.getName());
            repoDesc.addProperty("branch", repo.getActiveBranch());
            repoDesc.addProperty("branch_num", repo.getFileSystem().listAllBranches().size());
            Commit commit = getLastCommit(repo);
            repoDesc.addProperty("commit_msg", commit.getMessage());
            repoDesc.addProperty("commit_time", format.format(commit.getDateOfCreation()));

            repositories.add(repoDesc);
        }
        return repositories;
    }

    private Commit getLastCommit(Repository repo){
        Set<String> commits = repo.getFileSystem().readCommitList();
        return (Commit)commits.stream().map(commitID -> {
            try {
                return repo.getFileSystem().readCommitObject(commitID);
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }).sorted(new ComparatorChain(Comparator.comparing(Commit::getDateOfCreation), true)).findFirst().get();
    }
}
