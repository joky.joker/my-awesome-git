package web.servlet.user;

import web.general.MagitContextListener;
import web.general.MagitLoginManager;
import web.synced.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet({"/login"})
public class LoginServlet extends HttpServlet {
    private static String USER_PARAM = "username";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String param = req.getParameter(USER_PARAM);
        if (param == null || param.isEmpty()){
            resp.sendRedirect(MagitLoginManager.LOGIN_PAGE);
            return;
        }
        session.setAttribute(MagitLoginManager.USER_ATTR_NAME, User.getUser(param));
        resp.sendRedirect("user.html");
    }
}
