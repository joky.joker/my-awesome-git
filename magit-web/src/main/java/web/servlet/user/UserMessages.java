package web.servlet.user;

import com.google.gson.JsonArray;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet({"/user_msges"})
public class UserMessages extends JsonResponseServlet {

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User user = getUserSession(req);
        JsonArray messages = new JsonArray();
        if (user == null){
            return messages;
        }
        List<String> messagesList = user.loadUserMessages();
        if (messagesList.size() != 0){
            System.out.println(messagesList.get(0));
        }
        for (String message: messagesList){
            messages.add(message);
        }
        return messages;
    }
}
