package web.servlet.user;

import web.general.JsonSerializerUtil;
import web.synced.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

@WebServlet({"/users"})
public class UserListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        try (Writer writer = resp.getWriter()){
            writer.write(JsonSerializerUtil.getGsonBuilder().toJson(User.listUsers()));
        }
    }
}
