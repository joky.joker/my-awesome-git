package web.servlet.pullrequest;

import com.magit.core.objects.Branch;
import com.magit.core.objects.LocalRepository;
import com.magit.core.objects.PullRequest;
import com.magit.core.objects.Repository;
import web.messages.Message;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet({"/repo/pr/create"})
public class CreatePullRequest extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";
    private static final String TARGET_PARAM = "target";
    private static final String BASE_PARAM = "base";
    private static final String MESSAGE_PARAM = "message";

    public CreatePullRequest() {
        this.init(REPO_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User currentUser = getUserSession(req);
        LocalRepository repo = getUserSession(req).getUserRepoAsLocal(req.getParameter(REPO_PARAM));
        Branch baseBranch = repo.getFileSystem().readBranchObject(req.getParameter(BASE_PARAM));
        Branch targetBranch = repo.getFileSystem().readBranchObject(req.getParameter(TARGET_PARAM));

        Repository remoteRepo = Repository.openRepository(repo.getRemoteRepoLocation());

        String prMessage = String.format("'%s' has made a pull request from base branch '%s' to target branch '%s' with message saying '%s'",
                currentUser.getName(), baseBranch.getName(), targetBranch.getName(), req.getParameter(MESSAGE_PARAM));

        User.getUser(remoteRepo.getRepoUser()).addMessage(new Message<String>(prMessage));
        repo.createPullRequest(currentUser.getName(),
                req.getParameter(TARGET_PARAM),
                req.getParameter(BASE_PARAM),
                req.getParameter(MESSAGE_PARAM));
        return null;
    }
}
