package web.servlet.pullrequest;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.magit.core.objects.PullRequest;
import com.magit.core.objects.Repository;
import web.general.DateTimeFormatterFactory;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet({"/repo/prs"})
public class ListRepoPullRequests extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";

    public ListRepoPullRequests() {
        this.init(REPO_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User currentUser = getUserSession(req);
        Repository repo = currentUser.getUserRepo(req.getParameter(REPO_PARAM));
        List<PullRequest> prs = repo.getFileSystem().readPullRequests();
        return convert(prs);
    }

    private JsonObject convert(List<PullRequest> prs) throws Exception{
        JsonObject result = new JsonObject();
        result.addProperty("status", "success");
        JsonArray prList = new JsonArray();
        for (PullRequest pr : prs){
            JsonObject singlePr = new JsonObject();
            singlePr.addProperty("id", pr.getId());
            singlePr.addProperty("initiator", pr.getInitiator());
            singlePr.addProperty("base_branch", pr.getBaseBranch().getName());
            singlePr.addProperty("target_branch", pr.getTargetBranch().getName());
            singlePr.addProperty("message", pr.getMessage());
            singlePr.addProperty("creation_date", DateTimeFormatterFactory.create().format(pr.getCreateionDate()));
            singlePr.addProperty("status", pr.getStatus().toString().toLowerCase());
            prList.add(singlePr);
        }
        result.add("prs", prList);
        return result;
    }
}
