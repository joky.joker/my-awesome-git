package web.servlet.pullrequest;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.magit.core.controllers.CommitGraphGenerator;
import com.magit.core.controllers.CommitItemDetailsGenerator;
import com.magit.core.interfaces.IFolderComparer;
import com.magit.core.merge.FileStateProvider;
import com.magit.core.objects.Branch;
import com.magit.core.objects.LocalRepository;
import com.magit.core.objects.PullRequest;
import com.magit.core.objects.Repository;
import web.messages.Message;
import web.servlet.JsonResponseServlet;
import web.synced.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.Path;

@WebServlet({"/repo/pr"})
public class PullRequestActionServlet extends JsonResponseServlet {
    private static final String REPO_PARAM = "repo";
    private static final String ACTION_PARAM = "action";
    private static final String PR_PARAM = "pr_id";
    private static final String REJECTION_PARAM = "message";

    public PullRequestActionServlet() {
        this.init(REPO_PARAM, ACTION_PARAM, PR_PARAM);
    }

    @Override
    public Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User currentUser = getUserSession(req);
        Repository repo = getUserSession(req).getUserRepo(req.getParameter(REPO_PARAM));
        String action = req.getParameter(ACTION_PARAM);
        JsonObject response = new JsonObject();
        response.addProperty("status", "success");
        PullRequest pr = repo.getFileSystem().getPullRequest(Integer.parseInt(req.getParameter(PR_PARAM)));
        if (action.equals("view")){
            response = handleView(response, pr);
        }

        if (action.equals("accept")){
            handleAccept(pr);
            String prMessage = String.format("PR #%s made by you for repo '%s' at '%s' with message '%s' was accepted by the owner '%s'",
                    pr.getId(), pr.getRemoteRepository().getName(), pr.getCreateionDate(), pr.getMessage(), currentUser.getName());
            User.getUser(pr.getInitiator()).addMessage(new Message<String>(prMessage));
        }

        if (action.equals("reject")){
            if (req.getParameter(REJECTION_PARAM) == null){
                throw new Exception("rejection message cant be empty");
            }

            pr.setStatus(PullRequest.Status.DENIED);
            pr.getRemoteRepository().getFileSystem().updatePullRequest(pr);
            String prMessage = String.format("PR #%s made by you for repo '%s' at '%s' with " +
                            "message '%s' was rejected by the owner '%s' with the following message '%s'",
                    pr.getId(), pr.getRemoteRepository().getName(), pr.getCreateionDate(),
                    pr.getMessage(), currentUser.getName(), req.getParameter(REJECTION_PARAM));
            User.getUser(pr.getInitiator()).addMessage(new Message<String>(prMessage));
        }

        return response;
    }

    public JsonObject handleView(JsonObject response, PullRequest pr) throws Exception{
        FileStateProvider targetProvider = new FileStateProvider(pr.getTargetBranch().getPointerCommit());
        FileStateProvider baseProvider = new FileStateProvider(pr.getBaseBranch().getPointerCommit());
        IFolderComparer comparer = targetProvider.compareTo(baseProvider);
        JsonArray files = new JsonArray();
        for (Path added : comparer.getNewFiles()){
            JsonObject obj = new JsonObject();
            obj.addProperty("filepath", added.toString());
            obj.addProperty("status", "created");
            obj.addProperty("content", targetProvider.getContent(added));
            files.add(obj);
        }

        for (Path added : comparer.getModified()){
            JsonObject obj = new JsonObject();
            obj.addProperty("filepath", added.toString());
            obj.addProperty("status", "modified");
            obj.addProperty("content", targetProvider.getContent(added));
            files.add(obj);
        }

        for (Path added : comparer.getRemoved()){
            JsonObject obj = new JsonObject();
            obj.addProperty("filepath", added.toString());
            obj.addProperty("status", "deleted");
            obj.addProperty("content", "this file was removed");
            files.add(obj);
        }
        response.add("files", files);
        return response;
    }

    public void handleAccept(PullRequest pr) throws Exception{
        if (!pr.getRemoteRepository().isCleanState()){
            throw new Exception("Repository has unstaged changes, please commit first");
        }
        //update the base in the remote
        Branch baseBranch = pr.getBaseBranch();
        Branch targetBranch = pr.getTargetBranch();
        baseBranch.setCommitPointed(targetBranch.getCommitPointed());
        baseBranch.save();

        //update the RB in the local
        LocalRepository local = pr.getLocalRepository();
        Branch localTargetBranch = local.getFileSystem().readBranchObject(targetBranch.getName());
        Branch remote = local.getFileSystem().readBranchObject(localTargetBranch.getTrackingAfter());
        remote.setCommitPointed(localTargetBranch.getCommitPointed());
        remote.save();

        //update the pr
        pr.setStatus(PullRequest.Status.ACCEPTED);
        pr.getRemoteRepository().getFileSystem().updatePullRequest(pr);
    }
}
