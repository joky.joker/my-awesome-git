package web.servlet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import web.general.JsonSerializerUtil;
import web.general.MagitLoginManager;
import web.synced.User;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;


public abstract class JsonResponseServlet extends HttpServlet {
    private String[] required;

    public void init(String ... args){
        required = args;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        Gson builder = JsonSerializerUtil.getGsonBuilder();
        validateRequired(req, resp);

        try{

            Object responseObj = processRequest(req, resp);
            if (responseObj == null){
                JsonObject response = new JsonObject();
                response.addProperty("status", "success");
                responseObj = response;
            }
            String response = builder.toJson(responseObj);
            try(Writer writer = resp.getWriter()){
                writer.write(response);
            }
        } catch (Exception e){
            writeError(resp, e.getMessage(), e);
        }
    }
    private void validateRequired(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
        if (required == null){
            return;
        }
        for (String arg : this.required){
            if (req.getParameter(arg) != null){
                continue;
            }
            writeError(resp, "missing get param '" + arg + "'", new Exception());
        }
    }
    private void writeError(HttpServletResponse resp, String errorString, Exception e) throws IOException, ServletException{
        try(Writer writer = resp.getWriter()){
            JsonObject err = new JsonObject();
            err.addProperty("status", "ERROR: " + errorString);
            writer.write(err.toString());
        }
        e.printStackTrace();
        throw new RuntimeException(e);
    }

    public abstract Object processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception;
    protected User getUserSession(HttpServletRequest req){
        return (User) req.getSession().getAttribute(MagitLoginManager.USER_ATTR_NAME);
    }

}
