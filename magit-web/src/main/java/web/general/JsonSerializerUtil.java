package web.general;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonSerializerUtil {

    public static Gson getGsonBuilder(){
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }
    public static Gson getDefaultGson(){
        return new Gson();
    }
}
