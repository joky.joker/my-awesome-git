package web.general;

import com.magit.common.utils.FileSystemFactory;
import com.magit.common.utils.FileUtilsHelper;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MagitContextListener implements ServletContextListener {
    public static String REPO_ROOT_PATH = "C:\\magit-ex3";
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        Path file = FileSystemFactory.toPath(REPO_ROOT_PATH);
        FileUtilsHelper.deleteFileRec(file);
        try {
            Files.createDirectories(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        Path file = FileSystemFactory.toPath(REPO_ROOT_PATH);
        FileUtilsHelper.deleteFileRec(file);
    }

    public static Path getRootPath(){
        return FileSystemFactory.toPath(REPO_ROOT_PATH);
    }
}
