package web.general;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class MagitLoginManager implements Filter {
    public static String USER_ATTR_NAME = "username";
    public static String LOGIN_PAGE = "signup.html";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        resp.setHeader("Cache-Control", "no-cache, must-revalidate");
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Expires", "Sat, 26 Jul 1997 05:00:00 GMT");

        if (null != req.getSession().getAttribute(USER_ATTR_NAME)){
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        String path = req.getServletPath();
        if (path.endsWith(LOGIN_PAGE)){
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        if (!path.endsWith(".html") && !path.equals("/")){
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        HttpServletResponse res = (HttpServletResponse)servletResponse;
        res.sendRedirect(req.getContextPath() + "/" + LOGIN_PAGE);
    }

    @Override
    public void destroy() {

    }
}
