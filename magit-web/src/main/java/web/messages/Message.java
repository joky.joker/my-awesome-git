package web.messages;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Message<T extends Object> {
    private Date messageDate;
    private T messageContent;

    public Message(Date messageDate, T messageContent) {
        this.messageDate = messageDate;
        this.messageContent = messageContent;
    }

    public Message(T messageContent) {
        this.messageContent = messageContent;
        this.messageDate = new Date();
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat();
        return String.format("[%s] %s", formatter.format(messageDate), messageContent.toString());
    }

    public Date getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(Date messageDate) {
        this.messageDate = messageDate;
    }

    public T getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(T messageContent) {
        this.messageContent = messageContent;
    }

}
