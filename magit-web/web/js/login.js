

function onLogin(username){
    if (username.trim() === ""){
        return;
    }
    console.log(username);
    window.location.href = resolveURL("login?username=" + encodeURIComponent(username));
}

$(function(){
    console.log("window loaded");
    $("#submitBtn").click(function(){
        var value = $("#userName").val();
        onLogin(value);
    })
    $(document).keypress(function (e) {
        if (e.which === 13 || e.key === 13) {
            onLogin($("#userName").val());
        }
    });
    $("#userName").focus();
});
