var chosenfile = null;

var commitList = null;
var wcList = null;
var prList = null;

var chosenCommit = null;
var currentBranch = null;
var repoMetaData = null;
var currentPr = null;

var newFilesList = null;
var deletedFilesList = null;
var modifiedFilesList = null;

//pr functionality
var currentTargetBranch = null;
var currentBaseBranch = null;
//TODO: add message for the pr in the table.
$(function(){
    commitList = new List('commitFiles', {
        valueNames: [ 'filepath' ],
        item: 'fileItemTemplateCommit'
    });
    wcList = new List('wcFiles', {
        valueNames: [ 'filepath' ],
        item: 'fileItemTemplate'
    });
    prList = new List('prFilesList', {
        valueNames: [ 'filepath' , 'status'],
        item: 'fileItemTemplatePR'
    });
    //wc status functionality

    newFilesList = new List('newFiles', {
        valueNames: [ 'filepath'],
        item: 'wcFileItemTemplate'
    });
    deletedFilesList = new List('deletedFiles', {
        valueNames: [ 'filepath'],
        item: 'wcFileItemTemplate'
    });
    modifiedFilesList = new List('modifiedFiles', {
        valueNames: [ 'filepath'],
        item: 'wcFileItemTemplate'
    });
});

function onCommitFileChosen(e) {
    var filePath = $(e).children(":first").text();
    $(".fileitem.chosen").removeClass("chosen");
    $(e).toggleClass("chosen");
    console.log("commit: " + chosenCommit + " chosen file is :" + filePath);
    $.getJSON(resolveURL("/read_commit_file_content"), {
        "repo" : currentRepo,
        "sha1" : chosenCommit,
        "filepath" : filePath
    }, function (json) {
        if (!validateMsg(json)){
            return;
        }
        $("#commitFileContent").val(json["content"]);
    });
}

function commit() {
    var message = prompt("Please enter commit message: ");
    $.getJSON(resolveURL("/commit"), {
        "repo" : currentRepo,
        "message" : message
    }, function (json) {
        if (validateMsg(json)){
            loadRepoMetaData();
            loadWCChanges();
            $.modal.close();
        }
    })
}
function dropbtnToggle(dropdown) {
    // var dropdown = $("#myDropdown");
    dropdown.toggle(function(){
        $(this).animate({height : 0}, 1000);
    }, function(){
        dropdown.animate({height: "fit-content"}, 1000);
    });
}
function loadWC(){
    wcList.clear();
    $.getJSON(resolveURL("/list_wc"), {"repo" : currentRepo}, function (json) {
        if (!validateMsg(json)){
            return;
        }
        var files = $.map(json["files"], function(file){return {"filepath" : file}});
        wcList.add(files);
        $("#wcFileContent").val('');
        $("#wcModal").modal({
            fadeDuration: 200
        });
    });
}

function loadWCChanges(){
    $.getJSON(resolveURL("/repo/wc_status"), {
        "repo" : currentRepo
    }, function (json) {
        if (validateMsg(json)){
            if (json["isCleanState"]){
                $(".noEmptyChanges").addClass("unseen");
                $(".emptyChanges").removeClass("unseen");
                return;
            }
            $(".noEmptyChanges").removeClass("unseen");
            $(".emptyChanges").addClass("unseen");

            var newFiles = $.map(json["new"], function(file){return {"filepath" : file}});
            var deletedFiles = $.map(json["deleted"], function(file){return {"filepath" : file}});
            var modifiedFiles = $.map(json["modified"], function(file){return {"filepath" : file}});
            newFilesList.clear();
            newFilesList.add(newFiles);

            modifiedFilesList.clear();
            modifiedFilesList.add(modifiedFiles);

            deletedFilesList.clear();
            deletedFilesList.add(deletedFiles);
        }
    })
}
function editWc(){
    loadWC();
    loadWCChanges();
}

function onSaveNewFile(){
    console.log("file path: " + $("#newFilePath").val());
    console.log("file content: " + $("#newFileContent").val());
    wcList.add({filepath: $("#newFilePath").val()})
    $.modal.close();
    $.getJSON(resolveURL("/mod"), {
        "operation" : "write",
        "repo" : currentRepo,
        "filepath" : $("#newFilePath").val(),
        "content" : $("#newFileContent").val(),
    }, function (json) {
        validateMsg(json);
        $("#newFilePath").val('');
        $("#newFileContent").val('');
        loadWCChanges();
    });
}

function saveFile(){
    console.log("saving to '" + chosenfile+ "' content: " + $("#wcFileContent").val());
    $.getJSON(resolveURL("/mod"), {
        "operation" : "write",
        "repo" : currentRepo,
        "filepath" : chosenfile,
        "content" : $("#wcFileContent").val(),
    }, function (json) {
        validateMsg(json);
        loadWCChanges();
    });
}

function deleteFile(){
    console.log("deleting " + chosenfile);
    //TODO: delete file request
    //GET /mod?operation='delete'&repo=currentRepo&filepath=filepath-> {status: "", filepath : "", content : ""}

    $.getJSON(resolveURL("/mod"), {
        "operation" : "delete",
        "repo" : currentRepo,
        "filepath" : chosenfile,
    }, function (json) {
        if (validateMsg(json)){
            wcList.remove("filepath", chosenfile);
            $("#wcFileContent").val('');
            loadWCChanges();
        }
    });
}

function onWcFileChosen(e){
    var filePath = $(e).children(":first").text();
    $(".fileitem.chosen").removeClass("chosen");
    $(e).toggleClass("chosen");
    console.log("chosen file is :" + filePath);
    chosenfile = filePath;
    $.getJSON(resolveURL("/mod"), {
        "operation" : "read",
        "repo" : currentRepo,
        "filepath" : chosenfile,
    }, function (json) {
        if (validateMsg(json)){
            $("#wcFileContent").val(json["content"]);
            loadWCChanges();
        }
    });
}

function newFile(){
    chosenfile = null;
    $("#wcNewFileModal").modal({
        fadeDuration : 200,
        closeExisting: false,
    })
}

function onCommitChosen(commit_id){
    console.log("Commit chosen: " + commit_id);
      chosenCommit = commit_id;
      commitList.clear();
    $.getJSON(resolveURL("/list_commit_files"), {"repo" : currentRepo, "sha1" : commit_id}, function (json) {
        if (!validateMsg(json)){
            return;
        }
        var files = $.map(json["files"], function(file){return {"filepath" : file}});
        commitList.add(files);
        $("#commitFileContent").val('');
        $("#commitFilesListModal").modal({
            fadeDuration: 200
        });
    });
    // GET /list_commit_files?repo=''&sha1=''
    //     {
    //         status : "success",
    //         files : [],
    //     }
}

// "sha1" : "",
// "message" : "",
// "creator" : "",
// "date" : "",
// "branches" : [""],
function updateCommitList(commit_history){
    $("#repositoriesContainer>:not(.table-header)").remove();
    $.each(commit_history, function(id, value){

        var divstr = "<div></div>";
        $("#repositoriesContainer").append(
            $(divstr).addClass("table-item").text(id + 1),
            $(divstr).addClass("table-item").text(value["sha1"]),
            $(divstr).addClass("table-item").text(value["message"]),
            $(divstr).addClass("table-item").text(value["creator"]),
            $(divstr).addClass("table-item").text(value["date"]),
            $(divstr).addClass("table-item").text(value["branches"].join(", ")),
            $(divstr).addClass("table-item").click(function(){
                onCommitChosen(value["sha1"]);
            }).append("<img src='" + resolveURL("/icons/list.png") + "'>")
        );
    });
}

function onBranchSelected(branch, isHead){
    var prefix = isHead ? "(Head)" : (branch["is_rtb"] == true ? "(RTB)": "");
    $("#branchesDropDownBtn").text(prefix + " " + branch["name"] + " (" + branch["sha1"] + ")");
    currentBranch = branch;
    $("#delete").unblock();
    $("#checkout").unblock();
    if (branch["is_rb"] || isHead){
        $("#checkout").block({message : null, bindEvents: false, overlayCSS : { cursor: "not-allowed"}});
        $("#delete").block({message : null, bindEvents: false, overlayCSS : { cursor: "not-allowed"}});
    }
}

function updateBranches(repo_metadata){
    $("#branchesDropDownData").children().remove();
    $.each(repo_metadata["branches"], function(_, branch){
        var isHead = branch["name"] === repo_metadata["head"];
        var prefix = isHead ? "(Head)" : (branch["is_rtb"] == true ? "(RTB)": "");
        var html = $("<a href=\"javascript:void(0);\">" + prefix + " " +  branch["name"] + " (" + branch["sha1"] + ")" + "</a>");
        html.click(function(){
            onBranchSelected(branch, isHead);
            dropbtnToggle($("#branchesDropDownData"));
        });
        $("#branchesDropDownData").append(html);
        if (isHead) {
            onBranchSelected(branch, true);
        }
    });
}

function isNotHavingRemoteRepo() {
    return repoMetaData["remote_repo_name"] === undefined || repoMetaData["remote_repo_user"] === undefined;
}

function updateTitle(remote_repo, remote_user){
    if (isNotHavingRemoteRepo()){
        $("#pageHeader").text(currentRepo);
        //$("#prControlCube").block({message : null, bindEvents: false, overlayCSS : { cursor: "not-allowed"}});
        $("#prControlCube").hide();
        $(".remote-colab").block({message : null, bindEvents: false, overlayCSS : { cursor: "not-allowed"}});
        return;
    } else {
        $("#prUpperContainer").hide();
    }
    $("#pageHeader").text(currentRepo + " (Forked from " + remote_user + "'s repo '" + remote_repo + "')");
}

function onPull(){
    console.log("pulling...");
    //TODO make pull http request
    // GET /colab/pull?repo='' -> {status: ""}
    $.getJSON(resolveURL("/colab/pull"), {"repo" : currentRepo}, function (json) {
        if (validateMsg(json)){
            alert("pulled successfull");
        }
    });
}

function onPush(){
    console.log("pushing....");
    //TODO: make push request
    //GET /colab/push?repo='' -> {status: ""}
    $.getJSON(resolveURL("/colab/push"), {"repo" : currentRepo}, function (json) {
        if (validateMsg(json)){
            alert("pushed successfull");
            loadRepoMetaData();
        }
    });
}

function onBranchDelete(){
    if (repoMetaData["head"] === currentBranch["name"]){
        onError("Cant delete head branch");
        return;
    }
    //TODO: delete branch and refresh
    //GET /branch/delete?repo=''&branch=''
    $.getJSON(resolveURL("/branch/delete"), {
        "repo" : currentRepo,
        "name" : currentBranch["name"]
    }, function (json) {
        if (validateMsg(json)){
            loadRepoMetaData();
        }
    });
}

function onBranchCreate(){
    var branchName = prompt("Please enter branch name: ");
    if (branchName == null){
        return;
    }
    var branchSha1 = prompt("Please enter commit sha1: ");
    if (branchSha1 == null){
        return;
    }
    //TODO: create branch and refresh (local)
    //GET /branch/createLocal?repo=''&name=''&sha1=''
    $.getJSON(resolveURL("/branch/create"), {
        "repo" : currentRepo,
        "name" : branchName,
        "sha1" : branchSha1
    }, function (json) {
        if (validateMsg(json)){
            loadRepoMetaData();
        }
    });
}

function onCreateRTB(){
    //TODO: check if current branch is Remote branch
    //TODO: create RTB after RB:
    //GET /branch/createRTB?repo=''&name=''&rb=''
    if (currentBranch["is_rb"] !== true){
        onError("Please choose a remote branch from the dropdown");
        return;
    }
    $.getJSON(resolveURL("/branch/createRTB"), {
        "repo" : currentRepo,
        "name" : currentBranch["name"],
    }, function (json) {
        if (validateMsg(json)){
            loadRepoMetaData();
        }
    });
}

function onBranchCheckout(){
    if (repoMetaData["head"] === currentBranch["name"]){
        onError("Cant checkout to head branch");
        return;
    }
    if (currentBranch["is_rb"] === true){
        onError("Cannot checkout remote branch!");
        return;
    }
    //TODO: check out request
    $.getJSON(resolveURL("/checkout"), {
        "repo" : currentRepo,
        "new_head" : currentBranch["name"],
    }, function (json) {
        if (validateMsg(json)){
            loadRepoMetaData();
        }
    });
    //GET /checkout?&repo=''&new_head=''
}

function onPrAccepted(pr_id){
    //TODO: 
    //GET /repo/pr?action='accept'&repo=''
    $.getJSON(resolveURL("/repo/pr"), {
        action : "accept",
        repo : currentRepo,
        pr_id : pr_id,
    }, function (json) {
        if (validateMsg(json)){
            loadRepoMetaData();
        }
    });
}

function onPrRejected(pr_id){
    //TODO:
    //GET /repo/pr?action='reject'&repo=''
    var message = prompt("Please provide with a rejection message: ");
    if (message == null){
        return;
    }
    $.getJSON(resolveURL("/repo/pr"), {
        action : "reject",
        repo : currentRepo,
        message: message,
        pr_id : pr_id,
    }, function (json) {
        if (validateMsg(json)){
            loadRepoMetaData();
        }
    });
}
function onPrListChanges(pr_id){
    //TODO:
    //GET /repo/pr?action='view'&repo=''
    $.getJSON(resolveURL("/repo/pr"), {action : "view", repo : currentRepo, pr_id : pr_id}, function(json){
        currentPr = json;
        prList.clear();
        $("#prFileContent").val('');
        prList.add($.map(json["files"], function(value, key){
            return value;
        }));
        $("#prFileListModal").modal({
            fadeDuration: 200
        });
    });
}

function getPrFileByPath(filepath) {
    for (var i = 0; i < currentPr["files"].length; i++){
        if (currentPr["files"][i]["filepath"] === filepath){
            return currentPr["files"][i];
        }
    }
    return null;
}
function onPrFileChosen(prFile){
    var filename = $(prFile).find(".filepath").text();
    var fileInfo = getPrFileByPath(filename);
    if (fileInfo["status"] === "deleted"){
        $("#prFileContent").val("file was deleted");
    } else {
        $("#prFileContent").val(fileInfo["content"]);
    }
}

function loadPrs(){
    //TODO: populate the prs
    //GET /repo/prs?repo=''
    $.getJSON(resolveURL("/repo/prs"), { repo : currentRepo}, function(json){
        $("#prContainer>:not(.table-header)").remove();
        $.each(json["prs"], function(_, pr){
            var divstr = "<div></div>";
            var spanstr = "<span></span>";
            $("#prContainer").append(
                $(divstr).addClass("table-item").addClass("first-item").text(pr["id"]),
                $(divstr).addClass("table-item").text(pr["initiator"]),
                $(divstr).addClass("table-item").text(pr["target_branch"]),
                $(divstr).addClass("table-item").text(pr["base_branch"]),
                $(divstr).addClass("table-item").text(pr["creation_date"]),
                $(divstr).addClass("table-item").text(pr["message"]),
                $(divstr).addClass("table-item").append($(spanstr).addClass("status-" + pr["status"]))
            );
            var accept = $("<img src='" + resolveURL("/icons/checked.png") + "'>").addClass("dont-revert").click(function(){
                onPrAccepted(pr["id"]);
            });
            var decline = $("<img src='" + resolveURL("/icons/declined.png") + "'>").addClass("dont-revert").click(function(){
                onPrRejected(pr["id"]);
            });
            var list = $("<img src='" + resolveURL("/icons/list.png") + "'>").click(function(){
                onPrListChanges(pr["id"]);
            });
            if (pr["status"] === "open"){
                $("#prContainer").append(
                    $(divstr).addClass("table-item")
                    .addClass("last-item").append(accept, decline, list)
                )
            } else {
                $("#prContainer").append(
                    $(divstr).addClass("table-item")
                    .addClass("last-item")
                )
            }
        });
    });
}

function populateBranches(branches, dropdownElement, elemOnClick) {
    $(dropdownElement).children().remove();
    $.each(branches, function(_, branch){
        var html = $("<a href=\"javascript:void(0);\">" +  branch["name"] + " (" + branch["sha1"] + ")" + "</a>");
        html.click(function(){
            dropbtnToggle($(dropdownElement));
            elemOnClick(branch);
        });
        $(dropdownElement).append(html);
    });
}

function prControlPanelBranches(repo_metadata) {
    if (isNotHavingRemoteRepo()){
        return;
    }
    var remotes = $(repo_metadata["branches"]).filter(function (id, branch) {
        return branch["is_rb"] === true;
    });

    $("#prTargetButton").text( remotes[0]["name"] + " (" + remotes[0]["sha1"] + ")");
    currentTargetBranch = remotes[0];
    $("#prBaseButton").text( remotes[0]["name"] + " (" + remotes[0]["sha1"] + ")");
    currentBaseBranch = remotes[0];

    populateBranches(remotes, $("#prTargetDropdownToggle"), function (branch) {
        $("#prTargetButton").text( branch["name"] + " (" + branch["sha1"] + ")");
        currentTargetBranch = branch;
    });

    populateBranches(remotes, $("#prBaseDropdownToggle"), function (branch) {
        $("#prBaseButton").text( branch["name"] + " (" + branch["sha1"] + ")");
        currentBaseBranch = branch;
    });
}
function onPrCreated() {
    var prMesage = $("#prMessage").val();
    if (currentTargetBranch == currentBaseBranch){
        alert("Cant PR on same branch!");
        return;
    }

    $.getJSON(resolveURL("/repo/pr/create"), {"repo" : currentRepo,
        "target" : currentTargetBranch["name"],
        "base" : currentBaseBranch["name"],
        "message" : prMesage,
    }, function (json) {
        if (validateMsg(json)){
            alert("PR sent successfully!");
            $("#prMessage").val('');
        }
    });
}
function loadRepoMetaData(){
    $.getJSON(resolveURL("/repo_metadata"), {"repo" : currentRepo}, function(json){
        if (!validateMsg(json)){
            return;
        }
        repoMetaData = json;
        updateCommitList(json["commit_history"]);
        updateTitle(json["remote_repo_name"], json["remote_repo_user"]);
        updateBranches(json);
        prControlPanelBranches(json);
    });
    loadPrs();
}

$(function(){
    // GET /repo_metadata?repo=''
    // {
    //     status : "success",
    //     "branches" : [{name :"", is_rb, is_rtb}],
    //     "head" : "",
    //     "commit_history" : [{
    //         "sha1" : "",
    //         "message" : "",
    //         "creator" : "",
    //         "date" : "",
    //         "branches" : [""],
    //     }]
    //     "remote_repo_name": "",
    //     "remote_repo_user": "",
    //     "is_clean_state" : false/true,
    // }
    window.msgMgr = new message_manager($("#msgContainer"));
    loadRepoMetaData();
})