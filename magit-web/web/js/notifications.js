function message_manager(container){
    if (window.msgMgr !== undefined){
        return window.msgMgr;
    }
    window.msgMgr = this;
    this.container = container;
    this.messages = []
    this.poll = function(){
        $.getJSON(resolveURL("/user_msges"), {
            "id" : msgMgr.messages.length,
        }, function(json){
            console.log("messages: " + JSON.stringify(json));
            msgMgr.messages = msgMgr.messages.concat(json);
            msgMgr.update_view();
        })
    }
    this.update_view = function(){
        $(this.container).html("");
        Object.values(this.messages).forEach(function(msg){
            if (msg == null){
                return;
            }
            $(msgMgr.container).append(
                $("<div></div>").html(msg).addClass("hoverable item")
            )
        })
        $(this.container).animate({
            scrollTop: $(this.container).prop("scrollHeight")
        }, 700);
    }
    window.msgMgr.poll();
    setInterval(window.msgMgr.poll, 2000);
}