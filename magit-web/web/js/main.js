
function fetchJson(url, params, onError){
    var resolved = resolveURL(url);
    return fetch(resolved, params).then(function(response){
        return response.json();
    }).catch(onError);
}

function validateMsg(msg){
    if (msg["status"] != "success" && msg["status"] !== undefined){
        onError(msg["status"]);
        return false;
    }
    return true;
}

function onError(err){
    console.log("Error : ", err);
    alert(err);
}

function hide(htmlElem){
    htmlElem.setAttribute("style", "display: none");
}

function show(htmlElem){
    htmlElem.removeAttribute("style");
}

function getContextPath() {
    var base = document.getElementsByTagName('base')[0];
    if (base && base.href && (base.href.length > 0)) {
        base = base.href;
    } else {
        base = document.URL;
    }
    var base = window.location.pathname;

    return base.substr(0, base.indexOf("/", 1));

}

function resolveURL(url) {
    return (getContextPath() + "/" + url).replace("//", "/");
}