import socket


try:
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.bind(("localhost", 8181))
		s.listen(1)
		print("listening...")
		s.settimeout(2)
		while True:
			try:
				conn, addr = s.accept()
				print(f"handling connection {addr}")
				conn.close()
			except socket.timeout:
				pass
except KeyboardInterrupt:
	print("exiting....")