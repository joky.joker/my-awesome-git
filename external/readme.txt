Submitters:
    Amit Maimon (311454334) - amit.maimon2@gmail.com
    Guy Meiri (201392453) - guymeiri312@gmail.com

General Explanation:
    The Magit project is made mostly of 3 modules:
    1. Core - contains all the logic and functionality.
    2. Console-ui - contains only UI functionality.
    3. Common - for classes needed in both core & ui (mostly unused)

    We tried to implement the functionality based on the S.O.L.I.D principles,
    thus using lots of interfaces and Factories.

    In addition we included some unit testings we wrote using Junit(4).

Main classes and their functionality:
    Core:
        We have implemented several classes, each representing a Magi-Object such as
        Blobs, Folders, Commits, Branches etc.
        All those Magi-object like classes are responsible to represent the objects and provide
        with sufficient functionality to the user who uses them.
        1. Blob: represents a blob object
        2. MagiFileInfo: represents additional details about a Blob or a Folder.
        3. Folder: represents a folder, holding files, folders and their details in the form of MagiFileInfo.
        4. Branch: represents a branch.
        5. Repository: represents a repository, wrapping most of the functionality around it.
        6. MagitFS: a utility class that is responsible to store and load the MagitObject classes,
            uses a IStringCompressor interface to compress the data it writes, and
            IMagiXMLSerilizer to deserialize and serialize the objects to XMLs.
        7. RepoXMLLoader: class that uses the IMagiXMLSerializer to serialize and load the xml files,
            into repository and later dump it to the disk.
        8. ComparerImpl: class that implements the IFolderComparerInterface and is providing with the
            functionality to compare 2 given folder, one named master, the other slave,
            and produce 3 lists of files: added, removed and modified.
            this functionality is being used to determine the unstaged changes in the git dir.
        9. CommitStagerImpl: implements the ICommitter and able to produce new commit based on the lists
            of files produced by the ComparerImpl.
        10. Controller: implements IInformationProvider, IUserInterfaceListener and provides the
            core functionality to the UI, such as that ui will be easily replaceable.
    UI:
        1. ConsoleInputImpl: implements the ConsoleInputInterface, providing functions such as
            readInt(), and readLine() from the console.
        2. MenuItemRunner: represents a function that runs receiving
            (IInformationProvider, IUserInterfaceListener , ConsoleInputInterface)
            and producing certain Console Menu item functionality.
        3. MenuItem: represents a menu item, with a MenuItemRunner, and a title.
        4. ConsoleUI: a class that has a list of MenuItems dynamically changeable, and thus
            its functionality can be easily extended, in a way that the ConsoleUI is not aware
            of its menu items or their functionality.

Assumptions we made:
    1. when creating an empty new repository, we add a master branch and initial commit containing
        nothing but the root folder, such as all the next commits will have this commit as a parent.
        (did that to prevent situations of multiple roots to the commit tree)
    2. we used Deflate compression to write the objects to disk, instead of Regular Zip file
        with entries - Aviad approved.

Implemented Bonuses:
    1. Implemented Create a new Repository on desired location,
        will fail if the location exists already.
    2. Set branch to specific commit sha1, input is validated to be a real commit sha1.
    3. Checkout branch on creation, after creating a branch the user will be asked if he wants
        to checkout to that branch, and if said yes, and there are no unstaged changes
        the system will check that branch out.
    4. Support for renaming files, move files, and copying folders and files around,
        in case of a renamed files the system will recognize it as a deleted and added files,
        when in fact the Blob object will not be changed, only the folder object containing it.
        The system does not assume anything about the file content/its name, and such as is able
        to fully work with renamed files.
