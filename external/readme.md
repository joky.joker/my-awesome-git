# Submitters
 1. **Amit Maimon** (311454334) - amit.maimon2@gmail.com  
 2. **Guy Meiri** (201392453) - guymeiri312@gmail.com  
  
# General Explanation  
   The Magit project is made mostly of 3 modules:  
   1. **Core** - contains all the logic and functionality.  
   2. **Wep** - contains only Web functionality.  
   3. **Common** - for classes needed in both core & ui (mostly unused). 
 
   We tried to implement the functionality based on the S.O.L.I.D principles,  
   thus using lots of interfaces and Factories.  
 
   In addition we included some unit testings we wrote using Junit(4).  
 
# Main classes and their functionality:  
  
  We have implemented several classes, each representing a Magi-Object such as  
       Blobs, Folders, Commits, Branches etc.  
       All those Magi-object like classes are responsible to represent the objects and provide  
       with sufficient functionality to the user who uses them.  
       
## Core
   1. `Blob`: represents a blob object  
   2. `MagiFileInfo`: represents additional details about a `Blob` or a `Folder`.  
   3. `Folder`: represents a folder, holding files, folders and their details in the form of `MagiFileInfo`.  
   4. `Branch`: represents a branch.  
   5. `Repository`: represents a repository, wrapping most of the functionality around it.  
   6. `MagitFS`: a utility class that is responsible to store and load the `MagitObject` classes, uses a `IStringCompressor` interface to compress the data it writes, and  
       `IMagiXMLSerilizer` to deserialize and serialize the objects to XMLs.  
   7. `RepoXMLLoader`: class that uses the `IMagiXMLSerializer` to serialize and load the xml files, into repository and later dump it to the disk.  
   8. `ComparerImpl`: class that implements the `IFolderComparerInterface` and is providing with the functionality to compare 2 given folder, one named master, the other slave, and produce 3 lists of files: added, removed and modified. this functionality is being used to determine the unstaged changes in the git dir.  
   9. `CommitStagerImpl`: implements the `ICommitter` and able to produce new commit based on the lists of files produced by the `ComparerImpl`.  
   10. `Controller`: implements `IInformationProvider`, `IUserInterfaceListener` and provides the core functionality to the UI, such as that ui will be easily replaceable. 
   11. `CommitGraphGenerator`: a class that can take a Set of leaf Commits and generate a Graph (using Jgrapht) of the whole commit tree)
   12. `IMerger` a merger class interface that is getting used in merge operations
   13. `ConflictDetails`: class that is responsible for describing a conflict, and solving it.
   14. `MagitMerger`: The class that is actually responsible for the Merge operations, returns a MergeExecutor that executes the merge itself (implements `IMerger`
   15.  `LocalRepository`: extends `Repository` with extra functionality of collaboration functions.
## Web Application
1. `MagitContextListener`: a servlet context listener, responsible of cleaning the `C:\magit-exe3` directory on startup and end.
2. `MagitLoginManager`: a Servlet filter, responsible of redirecting ".html" requests in case the user isnt logged in.
3. `Message`: class representing a message to be sent.
4. `User`: class wrapping up the user functionality, along with creating new user, retreiving user list, sending message to a user and opening user repo in a concurrent safe manner.
5. `JsonResponseServlet`: an abstract Servlet which make sure the returned data is always json, in case of an exception, the servlet will wrap up the error in json and send to the client, in case the returned data is an object it will "Jsonify" it.
6. `BranchActionServlet`: an abstract class, inheriting from JsonResponseServlet, parsing out the relevant data for various actions such as creating new branch, deleting, and reseting and checking out of a branch.
7. `web.general.servlet` package, contains all the servlets in the WebApp
8. `web.general.servlet.branch` package contains all branch related servlets and the `BranchActionServlet` as well.
9. `web.general.servlet.colab` package contains all the collaboration servlets, including push/pull/fork functionality.
10. `web.general.servlet.pullrequest` package contains all the PR functionality, everything from creation of PR, to viewing its status and accepting/rejecting it.
11. `web.general.servlet.repo` package containes various functionality implementation regarding the repository itself, such as commit, list commits, list/manipulation of wc contnet, loading xml, viewing commit files and getting repo metadata.
12. `web.general.servlet.user` package contains all user related functions, such as getting the current user, list its repos, login/logout, list all users, and view user messages.

## Things you should know
### General
   1. Nothing really, just happy to be over this course xD, hope you do too!
  
## Implemented Bonuses:  
  1. **Bonus #3 (7 points up to 100)**, in page number #3 (repo details), you can choose a branch from the dropdown list, and click on delete, in that case if thats a local branch it will be deleted and if its RTB its RB and the remote repo's branch will be deleted as well.
  2. **Bonus #5 (10 points above 100)**, when clicking on viewing a PR's details the regarding the files are aggregated, meaning the diff is computed from the base branch to the target.
