

function repository_manager(container, uploadIcon, forkIcon) {
    this.ctr = 0;
    this.repoContainer = container;
    this.repositories = {};
    this.uploadIcon = uploadIcon;
    this.forkIcon = forkIcon;
    this.addRepository = function(repo){
        var currentNumber = this.ctr;
        this.ctr++;
        this.repositories[currentNumber] = repo;
        var action = $("<a href='#'></a>").append(
            $("<img>").attr("src", "/static/icons/fork.svg")
        ).append("fork").click(function(){
            console.log("forking " + repo["name"]);
        });
        if (userMgr.current_user == repo["owner"]){
            action = $("<a href='#'></a>").append(
                $("<img>").attr("src", "/static/icons/open.svg")
            ).append("open").click(function(){
                console.log("opening " + repo["name"]);
            });
        }
        $(this.repoContainer).append(
            $("<div></div>").text(this.ctr),
            $("<div></div>").text(repo["name"]),
            $("<div></div>").text(repo["branch"]),
            $("<div></div>").text(repo["branch_num"]),
            $("<div></div>").text(repo["commit_msg"]),
            $("<div></div>").text(repo["commit_time"]),
            $("<div></div>").append(action)
        );

        this.updateWindowStatus();
    };

    this.updateWindowStatus = function() {
        var repoPlaceholder = $("#repoPlaceholder");
        if (Object.keys(this.repositories).length == 0){
            repoPlaceholder.show();
        } else {
            repoPlaceholder.hide();
        }
    };

    this.onLoadRepo = function(e){
        console.log(e);
        var files = $("#xmlUploadInput").files;
        console.log(files);
        var formData = new FormData();
        formData.append("xml",files[0])
        fetchJson("/load_xml", {"method": "post", "body" : formData}, onError)
        .then(json=>{
            if (validateMsg(json)){
                repoMgr.onUserChosen(userMgr.current_user);
            }
        }).catch(onError);
    }
    this.clearRepos = function() {
        $(this.repoContainer).children().each(function(){
            var current = $(this);
            if (current.hasClass("table-header")){
                return;
            }
            current.remove();
        })
        this.ctr = 0;
    };
    
    this.onRepoChosen = function(chosen){
        console.log("on repo chosen: " + JSON.stringify(this.repositories[chosen]));
    };
    this.onUserChosen = function(username){
        console.log("user chosen: " + username);
        $("#currentUser").text(username + "'s");
        fetchJson("/user_repos?username=" + encodeURIComponent(username)).then(jsonData=>{
            repoMgr.clearRepos();
            Object.values(jsonData).forEach(repo=>{
                repoMgr.addRepository(repo);
            });
            this.updateWindowStatus();
            if (username == userMgr.current_user){
                uploadIcon.show();
            } else {
                uploadIcon.hide();
            }
        });
    };
    $("xmlUploadInput").change(this.onLoadRepo);
}

function users_manager(container){
    this.users = [];
    this.current_user = null;
    this.container = container;
    this.addUser = function(username){
        var userDiv = $("<div></div>").addClass("item hoverable");
        var extra = "not-chosen";
        if (username == this.current_user){
            extra = "";
        }
        userDiv.html('<img src="/static/icons/choose.svg" class="' + extra + '">' + username);
        $(this.container).append(
            $("<a href='#'></a>").append(
                userDiv
            ).attr("id", username).click(function(){
                window.repoMgr.onUserChosen(username);
            })
        );
        this.users.push(username);
    };

    this.loadUsers = function(){
        $.getJSON("/users",function(jsonData){
            console.log(jsonData);
            jsonData.forEach(username => {
                userMgr.addUser(username);
            });
        });
    };

    fetchJson("/current_user", {"method" : "GET"})
    .then(function(json){
        userMgr.current_user = json["username"];
        //populate the user list
        userMgr.loadUsers();
        repoMgr.onUserChosen(userMgr.current_user);
    });
}


function main(){
    window.repoMgr = new repository_manager($("#repositoriesContainer"), $("#xmlUpload"), $("#forkRepo"));
    window.userMgr = new users_manager($("#usersContainer"));
    window.msgMgr = new message_manager($("#msgContainer"));
    $("#xmlUpload").click(function() {
        $("#xmlUploadInput").value = '';
        console.log("clicked!");
        $("#xmlUploadInput").trigger("click");
        
    });
}


function test(){
    repoMgr.addRepository({
        "name": "Hookah!",
        "branch" : "branch",
        "branch_num" : "7",
        "commit_time" : "some time",
        "commit_msg" : "AHAHAHAHAHAHHAA!"
    });
}

window.addEventListener("load", main);