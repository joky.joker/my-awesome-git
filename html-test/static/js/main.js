
function fetchJson(url, params, onError){
    return fetch(url, params).then(function(response){
        return response.json();
    }).catch(onError);
}

function validateMsg(msg){
    if (msg["status"] != "success"){
        onError(msg["status_msg"]);
        return false;
    }
    return true;
}

function onError(err){
    console.log("Error : ", err);
    alert(err);
}

function hide(htmlElem){
    htmlElem.setAttribute("style", "display: none");
}

function show(htmlElem){
    htmlElem.removeAttribute("style");
}