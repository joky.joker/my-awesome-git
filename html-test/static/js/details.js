var chosenfile = null;

var commitList = null;
var wcList = null;
var prList = null;

var chosenCommit = null;
var currentBranch = null;
var repoMetaData = null;
var currentPr = null;

$(function(){
    commitList = new List('commitFiles', {
        valueNames: [ 'filepath' ],
        item: 'fileItemTemplateCommit'
    });
    wcList = new List('wcFiles', {
        valueNames: [ 'filepath' ],
        item: 'fileItemTemplate'
    });
    prList = new List('prFilesList', {
        valueNames: [ 'filepath' , 'status'],
        item: 'fileItemTemplatePR'
    });
});

function onCommitFileChosen(e) {
    var filePath = $(e).children(":first").text();
    $(".fileitem.chosen").removeClass("chosen");
    $(e).toggleClass("chosen");
    console.log("commit: " + chosenCommit + " chosen file is :" + filePath);
    $("#commitFileContent").val(filePath);
    chosenfile = filePath;
    //TODO: get commit file content ? 
}

function dropbtnToggle(dropdown) {
    // var dropdown = $("#myDropdown");
    dropdown.toggle(function(){
        $(this).animate({height : 0}, 1000);
    }, function(){
        var curHeight = dropdown.height();
        dropdown.css('height', 'auto');
        var autoHeight = $('#first').height();
        dropdown.height(curHeight).animate({height: autoHeight}, 1000);
    });
}

function editWc(){
    wcList.clear();  
    var values = [
        {
            filepath: './fol1/foo.java',
        },
        {
            filepath: './fol2/foo2.java',
        },
        {
            filepath: './a.txt',
        }
      ];
      
      wcList.add({
        filepath: './b.txt',
      });
      $("#wcFileContent").val('');
      $("#wcModal").modal({
        fadeDuration: 200
    });
}

function onSaveNewFile(){
    console.log("file path: " + $("#newFilePath").val());
    console.log("file content: " + $("#newFileContent").val());
    wcList.add({filepath: $("#newFilePath").val()})
    $.modal.close();
    //TODO: save file request
    //GET /mod?operation='write'&repo=currentRepo&filepath=filepath&content=content -> {status: "", filepath : "", content : ""}
}

function saveFile(){
    console.log("saving to '" + chosenfile+ "' content: " + $("#wcFileContent").val());
    //TODO: save file request
    //GET /mod?operation='write'&repo=currentRepo&filepath=filepath&content=content -> {status: "", filepath : "", content : ""}
}

function deleteFile(){
    console.log("deleting " + chosenfile);
    //TODO: delete file request
    //GET /mod?operation='delete'&repo=currentRepo&filepath=filepath-> {status: "", filepath : "", content : ""}
    wcList.remove("filepath", chosenfile);
    $("#wcFileContent").val('');
}

function onWcFileChosen(e){
    var filePath = $(e).children(":first").text();
    $(".fileitem.chosen").removeClass("chosen");
    $(e).toggleClass("chosen");
    console.log("chosen file is :" + filePath);
    $("#wcFileContent").val(filePath);
    chosenfile = filePath;
    //TODO: read file request
    //GET /mod?operation='read'&repo=currentRepo&filepath=filepath&content=content -> {status: "", filepath : "", content : ""}
}

function newFile(){
    chosenfile = null;
    $("#wcNewFileModal").modal({
        fadeDuration : 200,
        closeExisting: false,
    })
}

function onCommitChosen(commit_id){
    console.log("Commit chosen: " + commit_id);
      chosenCommit = commit_id;
      commitList.clear();
      var values = [
        {
            filepath: './fol1/foo.java',
        },
        {
            filepath: './fol2/foo2.java',
        },
        {
            filepath: './a.txt',
        }
      ];
      
      commitList.add({
        filepath: './b.txt',
      });
      commitList.add(values);
      $("#commitFileContent").val('');
      $("#commitFilesListModal").modal({
        fadeDuration: 200
    });
}

// "sha1" : "",
// "message" : "",
// "creator" : "",
// "date" : "",
// "branches" : [""],
function updateCommitList(commit_history){
    $("#repositoriesContainer>:not(.table-header)").remove();
    $.each(commit_history, function(id, value){

        var divstr = "<div></div>";
        $("#repositoriesContainer").append(
            $(divstr).addClass("table-item").text(id + 1),
            $(divstr).addClass("table-item").text(value["sha1"]),
            $(divstr).addClass("table-item").text(value["message"]),
            $(divstr).addClass("table-item").text(value["creator"]),
            $(divstr).addClass("table-item").text(value["date"]),
            $(divstr).addClass("table-item").text(value["branches"].join(", ")),
            $(divstr).addClass("table-item").click(function(){
                onCommitChosen(value["sha1"]);
            }).append("<img src='/static/icons/list.png'>")
        );
    });
}

function onBranchSelected(branch, isHead){
    var prefix = isHead ? "(Head)" : (branch["is_rtb"] == true ? "(RTB)": "");
    $("#branchesDropDownBtn").text(prefix + " " + branch["name"]);
    currentBranch = branch;
    $("#delete").unblock();
    $("#checkout").unblock();
    if (isHead){
        $("#delete").block({message : null, overlayCSS : { cursor: "not-allowed"}});
    }
    if (branch["is_rb"]){
        $("#checkout").block({message : null, overlayCSS : { cursor: "not-allowed"}});
    }
}

function updateBranches(repo_metadata){
    $("#branchesDropDownData").children().remove();
    $.each(repo_metadata["branches"], function(_, branch){
        var isHead = branch["name"] === repo_metadata["head"]
        var prefix = isHead ? "(Head)" : (branch["is_rtb"] == true ? "(RTB)": "");
        var html = $("<a href='#'>" + prefix + " " +  branch["name"] + "</a>");
        html.click(function(){
            onBranchSelected(branch, isHead);
            dropbtnToggle($("#branchesDropDownData"));
        });
        $("#branchesDropDownData").append(html);
        if (isHead) {
            onBranchSelected(branch, true);
        }
    });
}

function updateTitle(remote_repo, remote_user){
    if (remote_repo === null || remote_user === null){
        $("#pageHeader").text(currentRepo);
        $("#prControlCube").block({message : null, overlayCSS : { cursor: "not-allowed"}});
        $(".remote-colab").block({message : null, overlayCSS : { cursor: "not-allowed"}});
        return;
    }
    $("#pageHeader").text(currentRepo + " (Forked from " + remote_user + "'s repo '" + remote_repo + "')");
}

function onPull(){
    console.log("pulling...");
    //TODO make pull http request
    // GET /colab/pull?repo='' -> {status: ""}
}

function onPush(){
    console.log("pushing....");
    //TODO: make push request
    //GET /colab/push?repo='' -> {status: ""}
}

function onBranchDelete(){
    if (repoMetaData["head"] === currentBranch){
        onError("Cant delete head branch");
        return;
    }
    //TODO: delete branch and refresh
    //GET /branch/delete?repo=''&branch=''
}

function onBranchCreate(){
    var branchName = prompt("Please enter branch name: ");
    if (branchName == null){
        return;
    }
    var branchSha1 = prompt("Please enter commit sha1: ");
    //TODO: create branch and refresh (local)
    //GET /branch/createLocal?repo=''&name=''&sha1=''
}

function onCreateRTB(){
    //TODO: check if current branch is Remote branch
    //TODO: create RTB after RB:
    //GET /branch/createRTB?repo=''&name=''&rb=''
}

function onBranchCheckout(){
    if (repoMetaData["head"] === currentBranch){
        onError("Cant checkout to head branch");
        return;
    }
    //TODO: check out request
    //GET /checkout?&repo=''&new_head=''
}

function onPrAccepted(pr_id){
    //TODO: 
    //GET /repo/pr?action='accept'&repo=''
}

function onPrRejected(pr_id){
    //TODO:
    //GET /repo/pr?action='reject'&repo=''
}
function onPrListChanges(pr_id){
    //TODO:
    //GET /repo/pr?action='view'&repo=''
    $.getJSON("/repo/pr", {action : "view", repo : currentRepo}, function(json){
        currentPr = json;
        prList.clear();
        $("#prFileContent").val('');
        prList.add($.map(json["files"], function(value, key){
            return {"filepath" : key, "status" : "(" + value["status"] + ")"};
        }));
        $("#prFileListModal").modal({
            fadeDuration: 200
        });
    });
}

function onPrFileChosen(prFile){
    var filename = $(prFile).find(".filepath").text();
    if (currentPr["files"][filename]["status"] === "deleted"){
        $("#prFileContent").val("file was deleted");
    } else {
        $("#prFileContent").val(currentPr["files"][filename]["content"]);
    }
}

function loadPrs(){
    //TODO: populate the prs
    //GET /repo/prs?repo=''
    $("#prContainer>:not(.table-header)").remove();
    $.getJSON("/repo/prs", { repo : currentRepo}, function(json){
        $.each(json["prs"], function(_, pr){
            var divstr = "<div></div>";
            var spanstr = "<span></span>";
            $("#prContainer").append(
                $(divstr).addClass("table-item").addClass("first-item").text(pr["id"]),
                $(divstr).addClass("table-item").text(pr["initiator"]),
                $(divstr).addClass("table-item").text(pr["target_branch"]),
                $(divstr).addClass("table-item").text(pr["base_branch"]),
                $(divstr).addClass("table-item").text(pr["creation_date"]),
                $(divstr).addClass("table-item").append($(spanstr).addClass("status-" + pr["status"])),
            );
            var accept = $("<img src='/static/icons/checked.png'>").addClass("dont-revert").click(function(){
                onPrAccepted(pr["id"]);
            });
            var decline = $("<img src='/static/icons/declined.png'>").addClass("dont-revert").click(function(){
                onPrRejected(pr["id"]);
            });
            var list = $("<img src='/static/icons/list.png'>").click(function(){
                onPrListChanges(pr["id"]);
            });
            if (pr["status"] === "open"){
                $("#prContainer").append(
                    $(divstr).addClass("table-item")
                    .addClass("last-item").append(accept, decline, list)
                )
            } else {
                $("#prContainer").append(
                    $(divstr).addClass("table-item")
                    .addClass("last-item")
                )
            }
        });
    });
}
$(function(){
    // GET /repo_metadata?repo=''
    // {
    //     status : "success",
    //     "branches" : [{name :"", is_rb, is_rtb}],
    //     "head" : "",
    //     "commit_history" : [{
    //         "sha1" : "",
    //         "message" : "",
    //         "creator" : "",
    //         "date" : "",
    //         "branches" : [""],
    //     }]
    //     "remote_repo_name": "",
    //     "remote_repo_user": "",
    //     "is_clean_state" : false/true,
    // }
    window.msgMgr = new message_manager($("#msgContainer"));
    $.getJSON("/repo_metadata", {"repo" : currentRepo}, function(json){
        if (!validateMsg(json)){
            return;
        }
        repoMetaData = json;
        updateCommitList(json["commit_history"]);
        updateTitle(json["remote_repo_name"], json["remote_repo_user"]);
        updateBranches(json);
    })
    loadPrs();
})