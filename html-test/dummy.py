import os
import time
basedir = os.path.realpath(os.curdir)
print(basedir)
from flask import Flask, request, send_from_directory, redirect, url_for, jsonify

# set the project root directory as the static folder, you can set others.
#static_url_path='/static', static_folder=basedir + "\\static\\"
app = Flask(__name__)

@app.route('/static/<path:path>')
@app.route('/', methods=["GET"])
def send_data(path="signup.html"):
    print(path)
    return app.send_static_file(path)

@app.route("/login")
def login():
    print("username : " + request.args["username"])
    return redirect(url_for("static", filename="user.html"))

@app.route("/users")
def users():
    return jsonify(["mokah", "hokah", "shokah"])

@app.route("/current_user")
def get_user():
    return jsonify({"username" : "hokah"})

@app.route("/load_xml", methods = ["POST"])
def loadXml():
    print(request.form)
    print(request.files)
    return jsonify({"status_msg" : "success"})

@app.route("/user_msges", methods = ["GET"])
def get_msges():
    last_id = int(request.args.get("id", 0))
    messages = ["the time now is : " + str(int(time.time()))] * (last_id  + (int(time.time()) % 3))
    return jsonify(messages[last_id+1:])

@app.route("/user_repos")
def user_repos():
    return jsonify([
        {
            "owner" : request.args["username"],
            "is_local" : True,
            "name" : "Repo1|" + request.args["username"],
            "branch" : "master",
            "branch_num" : 7,
            "commit_time" : "00-111.22",
            "commit_msg" : "this is the last commit message!",

    },    {
            "owner" : request.args["username"],
            "is_local" : True,
            "name" : "Repo2|" + request.args["username"],
            "branch" : "deep",
            "branch_num" : 5,
            "commit_time" : "00-111.22",
            "commit_msg" : "this is the last commit message!",

    }])

@app.route("/repo_metadata")
def repo_meta():
    # repoName = request.args.get("repo", "N/A")
    return jsonify({
            "status" : "success",
            "branches" : [
                {"name" : "master", "is_rb" : False, "is_rtb" : False},
                 {"name" : "deep", "is_rb" : True, "is_rtb" : False},
                 {"name" : "temp", "is_rb" : False, "is_rtb" : True},],
            "head" : "master",
            "commit_history" : [{
                "sha1" : "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
                "message" : "Some commit history message",
                "creator" : "Amit",
                "date" : "20/Sep/2019 17:18:01",
                "branches" : ["master", "deep"],
            },{
                "sha1" : "1" * 40,
                "message" : "Some other commit message",
                "creator" : "John Doe",
                "date" : "20/Sep/2019 17:18:01",
                "branches" : [],
            }],
            "remote_repo_name": "None",
            "remote_repo_user": "John Doe",
            "is_clean_state" : False,
    })

@app.route("/repo/prs")
def repo_prs():
    repoName = request.args.get("repo", "N/A")
    return jsonify({
        "status": "success",
        "prs" : [
            {
                "id" : 1,
                "initiator" : "moshe",
                "target_branch" : "target",
                "base_branch" : "base",
                "creation_date" : "today?",
                "status" : "accepted"
            },
            {
                "id" : 2,
                "initiator" : "simha",
                "target_branch" : "target",
                "base_branch" : "base",
                "creation_date" : "today?",
                "status" : "denied"
            },
            {
                "id" : 3,
                "initiator" : "shlomo",
                "target_branch" : "target",
                "base_branch" : "base",
                "creation_date" : "today?",
                "status" : "open"
            },
        ]
    })

@app.route("/repo/pr")
def repo_pr_actions():
    repoName = request.args.get("repo", "N/A")
    action = request.args.get("action", "view")
    if action == "view":
        return jsonify({
        "status": "success",
        "files" : {
            "./fol1/foo.java" : {"status" : "deleted", "content" : ""},
            "./fol2/foo.java" : {"status" : "created", "content" : "hola"},
            "./a.txt" : {"status" : "modified", "content" : "aha"}
        }
    })

if __name__ == "__main__":
    app.run(debug=True)